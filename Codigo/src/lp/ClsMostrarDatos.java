package lp;



import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


import comun.ItfProperty;
import comun.clsConstantes;
import ln.ClsGestorLN;

/**
 * Esta clase sirve para mostrar los datos.
 * 
 * @author Aitor Ubierna
 */
public class ClsMostrarDatos {

	/**
	 * Constructor de la clase MostrarDatos
	 */
	public ClsMostrarDatos() {
	}

	/**
	 * Metodo para sacar por consola las series.
	 * 
	 * @param ObjGestorVSe parametro para acceder al metodo DameSeries.
	 * @throws SQLException fallo bbdd
	 */
	public void VerSeries(ClsGestorLN ObjGestorVSe) throws SQLException {

		/**
		 * Insatnciamos un Array de tipo ITF
		 */
		ArrayList<ItfProperty> Series;

		/**
		 * Recogemos los objetos del Array de la clase y los pasamos al de ITF
		 */
		Series = ObjGestorVSe.ObtenerSeries();

		/**
		 * Mostramos mensaje a partir del cual va e salir la informacion obtenida
		 */
		System.out.println("SERIES RECUPERADAS");
		System.out.println("-------------------");

		/**
		 * obtenemos cada propiedad de cada uno de los objetos y los sacamos por
		 * pantalla
		 */
		for (ItfProperty a : Series) {

			System.out.println(a.getIntegerProperty(clsConstantes.PROPIEDAD_SERIES_NUMERO_DE_SERIE) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_SERIES_DESCRIPCION));

		}
		System.out.println("-------------------");
	}

	/**
	 * Metodo para sacar por consola las tapas
	 * 
	 * @param ObjGestorVS parametro para acceder a Tapas
	 * @throws SQLException fallo bbdd
	 */
	public void VerTapas(ClsGestorLN ObjGestorVS) throws SQLException {

		/**
		 * Insatnciamos un Array de tipo ITF
		 */
		ArrayList<ItfProperty> Tapas;

		/**
		 * Recogemos los objetos del Array de la clase y los pasamos al de ITF
		 */
		Tapas = ObjGestorVS.ObtenerTapas();

		/**
		 * Mostramos mensaje a partir del cual va e salir la informacion obtenida
		 */
		System.out.println("TAPAS RECUPERADAS");
		System.out.println("-------------------");

		/**
		 * obtenemos cada propiedad de cada uno de los objetos y los sacamos por
		 * pantalla
		 */
		for (ItfProperty a : Tapas) {

			System.out.println(a.getIntegerProperty(clsConstantes.PROPIEDAD_TAPAS_REFERENCIA) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_TAPAS_DESCRIPCION) + " "
					+ a.getDoubleProperty(clsConstantes.PROPIEDAD_TAPAS_PRECIO));

		}
		System.out.println("-------------------");
	}

	/**
	 * Metodo para sacar por consola Materiales
	 * 
	 * @param ObjGestorVM parametro para acceder a DameMateriales
	 * @throws SQLException fallo bbdd
	 */
	public void VerMateriales(ClsGestorLN ObjGestorVM) throws SQLException {

		/**
		 * Insatnciamos un Array de tipo ITF
		 */
		ArrayList<ItfProperty> Materiales;

		/**
		 * Recogemos los objetos del Array de la clase y los pasamos al de ITF
		 */
		Materiales = ObjGestorVM.ObtenerMateriales();

		/**
		 * Mostramos mensaje a partir del cual va e salir la informacion obtenida
		 */
		System.out.println("MATERIALES RECUPERADOS");
		System.out.println("----------------------");

		/**
		 * obtenemos cada propiedad de cada uno de los objetos y los sacamos por
		 * pantalla
		 */
		for (ItfProperty a : Materiales) {

			System.out.println(a.getIntegerProperty(clsConstantes.PROPIEDAD_MATERIALES_REFERENCIA) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_MATERIALES_DESCRIPCION) + " "
					+ a.getDoubleProperty(clsConstantes.PROPIEDAD_MATERIALES_PRECIO));

		}
		System.out.println("-------------------");

	}

	/**
	 * Metodo para sacar por consola los tipos de hoja
	 * 
	 * @param objGestorVTH parametro para acceder a dame tipos de hoja
	 * @throws SQLException fallo bbdd
	 */
	public void VerTipos(ClsGestorLN objGestorVTH) throws SQLException {

		/**
		 * Insatnciamos un Array de tipo ITF
		 */
		ArrayList<ItfProperty> Tipos;

		/**
		 * Recogemos los objetos del Array de la clase y los pasamos al de ITF
		 */
		Tipos = objGestorVTH.ObtenerTiposHoja();

		/**
		 * Mostramos mensaje a partir del cual va e salir la informacion obtenida
		 */
		System.out.println("TIPOS DE HOJA RECUPERADOS");
		System.out.println("--------------------");

		/**
		 * obtenemos cada propiedad de cada uno de los objetos y los sacamos por
		 * pantalla
		 */
		for (ItfProperty a : Tipos) {

			System.out.println(a.getIntegerProperty(clsConstantes.PROPIEDAD_TIPOHOJA_DESCRIPCION) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_TIPOHOJA_DESCRIPCION) + " "
					+ a.getDoubleProperty(clsConstantes.PROPIEDAD_TIPOHOJA_PRECIO));

		}
		System.out.println("-------------------");
	}

	/**
	 * Metodo para sacar por consola Clientes
	 * 
	 * @param objGestorVC parametro para acceder a dame clientes
	 * @throws SQLException fallo bbdd
	 */
	public void VerClientes(ClsGestorLN objGestorVC) throws SQLException {

		/**
		 * Insatnciamos un Array de tipo ITF
		 */
		ArrayList<ItfProperty> Clientes;

		/**
		 * Recogemos los objetos del Array de la clase y los pasamos al de ITF
		 */
		Clientes = objGestorVC.ObtenerClientes();

		/**
		 * Mostramos mensaje a partir del cual va e salir la informacion obtenida
		 */
		System.out.println("CLIENTES RECUPERADOS");
		System.out.println("--------------------");

		/**
		 * obtenemos cada propiedad de cada uno de los objetos y los sacamos por
		 * pantalla
		 */
		for (ItfProperty a : Clientes) {

			System.out.println(a.getIntegerProperty(clsConstantes.PROPIEDAD_CLIENTE_NUMERO) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_CLIENTE_NOMBRE_Y_APELLIDOS) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_CLIENTE_DNI_NIF) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_CLIENTE_DIRECCION) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_CLIENTE_PROVINCIA) + " "
					+ a.getIntegerProperty(clsConstantes.PROPIEDAD_CLIENTE_TELEFONO) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_CLIENTE_EMAIL));

		}
		System.out.println("-------------------");
	}

	/**
	 * Metodo sacar por consola Pedidos
	 * 
	 * @param objGestorVP objeto para acceder a DamePedidos
	 * @throws SQLException fallo bbdd
	 */
	public void VerPedidos(ClsGestorLN objGestorVP) throws SQLException {

		/**
		 * Variables para el cambio
		 */
		String Entregado;
		SimpleDateFormat MiFormato = new SimpleDateFormat("yyyy-MM-dd");
		/**
		 * Insatnciamos un Array de tipo ITF
		 */
		ArrayList<ItfProperty> Pedidos;

		/**
		 * Recogemos los objetos del Array de la clase y los pasamos al de ITF
		 */
		Pedidos = objGestorVP.ObtenerPedidos();

		/**
		 * Mostramos mensaje a partir del cual va e salir la informacion obtenida
		 */
		System.out.println("PEDIDOS RECUPERADOS");
		System.out.println("--------------------");

		/**
		 * obtenemos cada propiedad de cada uno de los objetos y los sacamos por
		 * pantalla
		 */
		for (ItfProperty a : Pedidos) {

			if (a.getBooleanProperty(clsConstantes.PROPIEDAD_PEDIDOS_ENTREGADO)) {
				Entregado = "Entregado";
			} else {
				Entregado = "No entregado";
			}

			System.out.println(a.getIntegerProperty(clsConstantes.PROPIEDAD_PEDIDOS_NUMERO_DE_PEDIDO) + " "
					+ MiFormato.format(a.getDateProperty(clsConstantes.PROPIEDAD_PEDIDOS_FECHA_DE_PEDIDO)) + " "
					+ MiFormato.format(a.getDateProperty(clsConstantes.PROPIEDAD_PEDIDOS_FECHA_DE_ENTREGA)) + " " + Entregado + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_PEDIDOS_NOMBRE_Y_APELLIDOS_DEL_CLIENTE) + " "
					+ a.getIntegerProperty(clsConstantes.PROPIEDAD_PEDIDOS_NUMERO_DE_CLIENTE_PEDIDO));

		}
		System.out.println("-------------------");
	}

	/**
	 * Metodo sacar por consola Envios
	 * 
	 * @param objGestorVE objeto para acceder a DameEnvios
	 * @throws SQLException fallo bbdd
	 */
	public void VerEnvios(ClsGestorLN objGestorVE) throws SQLException {

		/**
		 * Insatnciamos un Array de tipo ITF
		 */
		ArrayList<ItfProperty> Envios;

		/**
		 * Recogemos los objetos del Array de la clase y los pasamos al de ITF
		 */
		Envios = objGestorVE.ObtenerEnvios();

		/**
		 * Mostramos mensaje a partir del cual va e salir la informacion obtenida
		 */
		System.out.println("ENVIOS RECUPERADOS");
		System.out.println("--------------------");

		/**
		 * obtenemos cada propiedad de cada uno de los objetos y los sacamos por
		 * pantalla
		 */
		for (ItfProperty a : Envios) {

			System.out.println(a.getIntegerProperty(clsConstantes.PROPIEDAD_ENVIOS_NUMERO_DE_ENVIO) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_ENVIOS_NOMBRE_CLIENTE) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_ENVIOS_DIRECCION_DE_ENVIO) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_ENVIOS_POBLACION_DE_ENVIO) + " "
					+ a.getIntegerProperty(clsConstantes.PROPIEDAD_ENVIOS_CPD_DE_ENVIO) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_ENVIOS_PROVINCIA_DE_ENVIO) + " "
					+ a.getIntegerProperty(clsConstantes.PROPIEDAD_ENVIOS_TELEFONO_DE_ENVIO) + " "
					+ a.getIntegerProperty(clsConstantes.PROPIEDAD_ENVIOS_NUMERO_DE_CLIENTE_ENVIO));

		}
		System.out.println("-------------------");
	}

	/**
	 * Metodo sacar por consola Articulos
	 * 
	 * @param objGestorVA objeto para acceder a DameArticulos.
	 * @throws SQLException fallo bbdd
	 */
	public void VerArticulos(ClsGestorLN objGestorVA) throws SQLException {

		/**
		 * Insatnciamos un Array de tipo ITF
		 */
		ArrayList<ItfProperty> Articulos;

		/**
		 * Recogemos los objetos del Array de la clase y los pasamos al de ITF
		 */
		Articulos = objGestorVA.ObtenerArticulos();

		/**
		 * Mostramos mensaje a partir del cual va e salir la informacion obtenida
		 */
		System.out.println("ARTICULOS RECUPERADOS");
		System.out.println("--------------------");

		/**
		 * obtenemos cada propiedad de cada uno de los objetos y los sacamos por
		 * pantalla
		 */
		for (ItfProperty a : Articulos) {

			System.out.println(a.getIntegerProperty(clsConstantes.PROPIEDAD_ARTICULO_REFERENCIA) + " "
					+ a.getIntegerProperty(clsConstantes.PROPIEDAD_ARTICULO_SERIE) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_ARTICULO_DESCRIPCION) + " "
					+ a.getIntegerProperty(clsConstantes.PROPIEDAD_ARTICULO_CANTIDAD_DE_MATERIAL) + " "
					+ a.getDoubleProperty(clsConstantes.PROPIEDAD_ARTICULO_PRECIO));
		}
		System.out.println("-------------------");
	}

	public void VerDesgloses(ClsGestorLN objGestorVD) throws SQLException {

		/**
		 * Insatnciamos un Array de tipo ITF
		 */
		ArrayList<ItfProperty> Desgloses;

		/**
		 * Recogemos los objetos del Array de la clase y los pasamos al de ITF
		 */
		Desgloses = objGestorVD.ObtenerDesgloses();

		/**
		 * Mostramos mensaje a partir del cual va e salir la informacion obtenida
		 */
		System.out.println("DESGLOSES RECUPERADOS");
		System.out.println("---------------------");

		/**
		 * obtenemos cada propiedad de cada uno de los objetos y los sacamos por
		 * pantalla
		 */
		for (ItfProperty a : Desgloses) {

			System.out.println(a.getIntegerProperty(clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_DESGLOSE) + " "
					+ a.getIntegerProperty(clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_REFERENCIA_DEL_ARTICULO) + " "
					+ a.getIntegerProperty(clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_SERIE) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_TIPO) + " "
					+ a.getStringProperty(clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_TAMA�O) + " "
					+ a.getIntegerProperty(clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_DESGLOSE));
		}
		System.out.println("-------------------");
	}
}
