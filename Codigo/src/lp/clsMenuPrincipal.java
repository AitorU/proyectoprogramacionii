package lp;

import java.sql.SQLException;
import java.util.Scanner;

import lp.ClsMostrarDatos;
import lp.UtilidadesLP;
import ln.ClsGestorLN;

public class clsMenuPrincipal {

	/**
	 * Metodo que reune el menu principal
	 * 
	 * @throws SQLException fallo bbdd
	 */
	public static void MenuPrincipal() throws SQLException {
		Scanner sc = new Scanner(System.in);

		/**
		 * Creamos un objeto Gestor y un objeto Datos.
		 */
		ClsGestorLN objGestor = new ClsGestorLN();
		ClsMostrarDatos objMostrarDatos = new ClsMostrarDatos();

		/**
		 * Llamadas a metodos para cargar los datos en Arrays nada mas iniciar la
		 * aplicacion.
		 */

		try {
			objGestor.DameClientes();
			objGestor.DameEnvios();
			objGestor.DameMateriales();
			objGestor.DamePedidos();
			objGestor.DameSeries();
			objGestor.DameTapas();
			objGestor.DameTipos();
			objGestor.DameArticulos();

		} catch (SQLException e) {
			/**
			 * Lanza mensaje de excepcion en caso de que no pueda cargar la informacion
			 */
			System.out.println("Ejecute la aplicacion MySQLWorkBench");
			System.out.println(e);
		}
		/**
		 * Variable para elegir la opcion de menu.
		 */
		int opcion;

		/**
		 * 
		 * Mostramos las opciones del menu principal.
		 *
		 */
		System.out.println("BIENVENIDO A LA GESTI�N DE LA F�BRICA DE LIBROS");
		System.out.println("======================================");
		do {
			System.out.println("OPCIONES:");
			System.out.println("1.INTRODUCIR DATOS");
			System.out.println("2.CONSULTAR DATOS");
			System.out.println("3.BORRAR DATOS");
			System.out.println("4.ACTUALIZAR PEDIDOS");
			System.out.println("5.SALIR");
			System.out.println("ELIGE UNA OPCI�N:");
			/**
			 * Pedimos la opcion a escoger por el usuario.
			 */
			opcion = UtilidadesLP.leerEntero();
			/**
			 * Segun la opcion escogida realizamos las siguientes tareas.
			 */
			switch (opcion) {
			/**
			 * opcion=1 menu de introducir datos.
			 */
			case 1:
				ClsMenuIntroducirdatos.MenuIntroducirDatos(objGestor);
				break;
			/**
			 * 
			 * opcion=2 menu de consultar datos.
			 *
			 */
			case 2:
				ClsMenuConsultarDatos.MenuConsultarDatos(objGestor, objMostrarDatos);
				break;
			/**
			 * 
			 * opcion=3 menu de borrar datos.
			 *
			 */
			case 3:
				ClsMenuBorrarDatos.MenuBorrarDatos(objGestor);
				break;
			/**
			 * 
			 * opcion=4 actualizamos entregas
			 *
			 */
			case 4:
				ActualizarEntregas(objGestor, objMostrarDatos);
				break;
			/**
			 * 
			 * opcion=5 finalizamos la aplicacion
			 *
			 */
			case 5:
				System.out.println("FIN DE LA APLICACI�N");
				break;
			}
		} while (opcion != 5);
	}

	/**
	 * Metodo directo para actualizar Entregas de Pedidos
	 * 
	 * @param objGestor       objeto gestor para acceder al gestor
	 * @param objMostrarDatos objeto MostrarDatos para mostrar los pedidos
	 * @throws SQLException fallo bbdd
	 */
	public static void ActualizarEntregas(ClsGestorLN objGestor, ClsMostrarDatos objMostrarDatos) throws SQLException {

		/**
		 * me muestra los pedidos
		 */
		objMostrarDatos.VerPedidos(objGestor);
		/**
		 * pedidos numero de pedido a actualizar
		 */
		System.out.println("Dime el numero de Pedido del que desas actualizar su entrega:");
		int Pedido = UtilidadesLP.leerEntero();

		/**
		 * Lo mandamos al gestor
		 */
		try {
			if (objGestor.ActualizarEntregasDePedidos(Pedido) == true) {
				System.out.println("Pedido actualizado correctamente!");
			}
		} catch (SQLException e) {
			System.out.println("La actualizacion no se a podido hacer: " + e);
		}
	}

}
