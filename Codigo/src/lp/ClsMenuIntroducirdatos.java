package lp;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import comprobadores.ClsComprobarDNI_NIF;
import excepciones.clsDNI_Excepcion;
import ln.ClsGestorLN;
import lp.UtilidadesLP;

/**
 * Creamos un menu donde nos de las opciones de introducir los distintos tipos
 * de datos que tenemos en nuestra base de datos.
 * 
 * @author Aitor Ubierna
 */

public class ClsMenuIntroducirdatos {

	/**
	 * Metodo que reune el menu de introducir datos
	 * 
	 * @param objGestorMI recibe el objeto gestor
	 */
	public static void MenuIntroducirDatos(ClsGestorLN objGestorMI) {

		/**
		 * 
		 * Variable para guardar la opcion elegida por el usuario.
		 *
		 */
		int opcionIntroducir;

		do {
			/**
			 * 
			 * Mostramos al usuario las opciones que tiene para elegir a la hora de
			 * introducir datos.
			 *
			 */
			System.out.println("Elije la entidad donde desas introducir nuevos datos: ");
			System.out.println("1- Introducir datos de las Tapas: \n" + "2- Introducir datos de Series: \n"
					+ "3- Introducir datos de Pedidos: \n" + "4- Introducir datos de Materiales: \n"
					+ "5- Introducir datos de Tipos de Hoja: \n" + "6- Introducir datos de Envios: \n"
					+ "7- Introducir datos de Clientes: \n" + "8- Introducir datos de Articulos: \n"
					+ "9- Introducir datos de Desglose de Pedido: \n" + "10- Regresar al Menu Principal: ");
			/**
			 * 
			 * Pedimos los datos que deseen introducir.
			 *
			 */
			opcionIntroducir = UtilidadesLP.leerEntero();
			switch (opcionIntroducir) {
			case 1:
				IntroducirDatosTapas(objGestorMI);
				break;

			case 2:
				IntroducirDatosSeries(objGestorMI);
				break;

			case 3:
				IntroducirDatosPedidos(objGestorMI);
				break;

			case 4:
				IntroducirDatosMateriales(objGestorMI);
				break;

			case 5:
				IntroducirDatosTipoHoja(objGestorMI);
				break;

			case 6:

				IntroducirDatosEnvios(objGestorMI);
				break;

			case 7:
				IntroducirDatosClientes(objGestorMI);
				break;

			case 8:
				IntroducirDatosArticulos(objGestorMI);
				break;

			case 9:
				IntroducirDatosDesgloseDePedido(objGestorMI);
				break;

			default:
				break;
			}

			/**
			 * si es opcion 10 regresamos al menu principal
			 */
		} while (opcionIntroducir != 10);
	}

	/**
	 * Pedimos los datos a introducir en la entidad tapas.
	 * 
	 * @param objGTapas objeto tapas
	 */
	public static void IntroducirDatosTapas(ClsGestorLN objGTapas) {

		/**
		 * variables para recoger los datos de la entidad Suela.
		 */
		int Referencia_Tapas;
		String Descripcion_Tapas;
		double Precio_Tapas;

		/**
		 * pedimos los datos.
		 */
		System.out.print("Introduzca el n�mero de Referencia:");
		Referencia_Tapas = UtilidadesLP.leerEntero();
		System.out.print("Introduzca la Descripci�n:");
		Descripcion_Tapas = UtilidadesLP.leerCadena();
		System.out.print("Introduzca el Precio:");
		Precio_Tapas = UtilidadesLP.leerReal();

		/**
		 * Rodeamos con TRY CATCH para tratar excepcion.
		 */
		try {
			/**
			 * Pasamos los parametros para generer el objeto
			 */
			objGTapas.CrearTapa(Referencia_Tapas, Descripcion_Tapas, Precio_Tapas);
			/**
			 * Mensaje para confirmar la introduccion de datos
			 */
			System.out.println("Datos introducidos correctamente!");
		} catch (SQLException e) {
			/**
			 * Mensaje de excepcion
			 */
			System.out.println("No se ha podido realizar el insert: " + e);
		}

	}

	/**
	 * Pedimos los datos a introducir en la entidad series.
	 * 
	 * @param objGSeries objeto series
	 */
	public static void IntroducirDatosSeries(ClsGestorLN objGSeries) {

		/**
		 * variables para recoger los datos de la entidad Series.
		 */
		int Numero_Serie;
		String Descripcion_Serie;

		/**
		 * pedimos los datos.
		 */
		System.out.print("Introduzca el n�mero de la Serie:");
		Numero_Serie = UtilidadesLP.leerEntero();
		System.out.print("Introduzca la Descripci�n:");
		Descripcion_Serie = UtilidadesLP.leerCadena();
		/**
		 * Rodeamos con TRY CATCH para tratar excepcion.
		 */
		try {
			/**
			 * Pasamos los parametros para generer el objeto
			 */
			objGSeries.CrearSerie(Numero_Serie, Descripcion_Serie);
			/**
			 * Mensaje para confirmar la introduccion de datos
			 */
			System.out.println("Datos introducidos correctamente!");
		} catch (SQLException e) {
			/**
			 * Mensaje de excepcion
			 */
			System.out.println("No se ha podido realizar el insert: " + e);
		}

	}

	/**
	 * Pedimos los datos a introducir en la entidad pedidos.
	 * 
	 * @param objGPedidos objeto pedidos
	 */
	public static void IntroducirDatosPedidos(ClsGestorLN objGPedidos) {

		/**
		 * variables para recoger los datos de la entidad pedido.
		 */
		int numero_Pedido;
		String fecha_Pedido;
		String fecha_Entrega_Pedido;
		Boolean entregado_Pedido;
		String nombre_apellido_cliente_pedido;
		int numero_cliente_pedido;
		Date Fecha_Pedido;
		Date Fecha_Entrega_Pedido;
		int comprobar;
		/**
		 * Variables de chequeo
		 */
		boolean primera = false;
		boolean segunda = false;

		/**
		 * Para formatear las fechas
		 */
		SimpleDateFormat miFormato = new SimpleDateFormat("dd/MM/yyyy");

		/**
		 * pedimos los datos.
		 */
		System.out.print("Introduzca el n�mero de pedido:");
		numero_Pedido = UtilidadesLP.leerEntero();

		do {
			System.out.print("Introduzca la Fecha del Pedido (Formato: 00/00/0000):");
			fecha_Pedido = UtilidadesLP.leerCadena();
			/**
			 * Pasamos de String a date tratando la excepcion.
			 */
			Fecha_Pedido = null;
			/**
			 * Rodeamos con TRY CATCH para tratar excepcion.
			 */
			try {
				Fecha_Pedido = (Date) miFormato.parse(fecha_Pedido);
				primera = true;
			} catch (ParseException e1) {
				System.out.println("�Error en el formato!");
			}
		} while (!primera);

		do {
			System.out.print("Introduzca la Fecha de Entrega (Formato: 00/00/0000):");
			fecha_Entrega_Pedido = UtilidadesLP.leerCadena();
			/**
			 * Rodeamos con TRY CATCH para tratar excepcion.
			 */
			Fecha_Entrega_Pedido = null;
			try {
				Fecha_Entrega_Pedido = (Date) miFormato.parse(fecha_Entrega_Pedido);
				segunda = true;
			} catch (ParseException e) {
				System.out.println("�Error en el formato!");
			}
		} while (!segunda);

		System.out.print("Introduce 1 (Si) o 0 (No) para indicar si el pedido esta entregado o no:");
		comprobar = UtilidadesLP.leerEntero();
		if (comprobar == 1) {
			entregado_Pedido = true;

		} else {
			entregado_Pedido = false;
		}

		System.out.print("Introduzca el Numero de Cliente:");
		numero_cliente_pedido = UtilidadesLP.leerEntero();
		System.out.print("Introduzca el Nombre y los Apellidos del Cliente:");
		nombre_apellido_cliente_pedido = UtilidadesLP.leerCadena();

		/**
		 * Rodeamos con TRY CATCH para tratar excepcion.
		 */
		try {
			/**
			 * Pasamos los parametros para generer el objeto
			 */
			objGPedidos.CrearPedidos(numero_Pedido, Fecha_Pedido, Fecha_Entrega_Pedido, entregado_Pedido,
					numero_cliente_pedido, nombre_apellido_cliente_pedido);
			/**
			 * Mensaje para confirmar la introduccion de datos
			 */
			System.out.println("Datos introducidos correctamente!");
		} catch (SQLException e) {
			/**
			 * Mensaje de excepcion
			 */
			System.out.println("No se ha podido realizar el insert: " + e);
		}

	}

	/**
	 * Pedimos los datos a introducir en la entidad Materiales. *
	 * 
	 * @param objGMateriales objeto materiales
	 */
	public static void IntroducirDatosMateriales(ClsGestorLN objGMateriales) {

		/**
		 * variables para recoger los datos de la entidad Materiales.
		 */
		int Referencia_Materiales;
		String Descripcion_Materiales;
		double Precio_Materiales;

		/**
		 * pedimos los datos.
		 */
		System.out.print("Introduzca el n�mero de Referencia:");
		Referencia_Materiales = UtilidadesLP.leerEntero();
		System.out.print("Dame la Descripci�n:");
		Descripcion_Materiales = UtilidadesLP.leerCadena();
		System.out.print("Introduzca el Precio:");
		Precio_Materiales = UtilidadesLP.leerReal();

		/**
		 * Rodeamos con TRY CATCH para tratar excepcion.
		 */
		try {
			/**
			 * Pasamos los parametros para generer el objeto
			 */
			objGMateriales.CrearMateriales(Referencia_Materiales, Descripcion_Materiales, Precio_Materiales);
			/**
			 * Mensaje para confirmar la introduccion de datos
			 */
			System.out.println("Datos introducidos correctamente!");
		} catch (SQLException e) {
			/**
			 * Mensaje de excepcion
			 */
			System.out.println("No se ha podido realizar el insert: " + e);
		}

	}

	/**
	 * Pedimos los datos a introducir en la entidad Tipos de Hoja.
	 * 
	 * @param objGTipo objeto tipos de hoja
	 */
	public static void IntroducirDatosTipoHoja(ClsGestorLN objGTipo) {

		/**
		 * variables para recoger los datos de la entidad Herrajes.
		 */
		int Referencia_Tipo;
		String Descripcion_Tipo;
		double Precio_Tipo;

		/**
		 * pedimos los datos.
		 */
		System.out.print("Introduzca el n�mero de Referencia:");
		Referencia_Tipo = UtilidadesLP.leerEntero();
		System.out.print("Dame la Descripci�n:");
		Descripcion_Tipo = UtilidadesLP.leerCadena();
		System.out.print("Introduzca el Precio:");
		Precio_Tipo = UtilidadesLP.leerReal();

		/**
		 * Rodeamos con TRY CATCH para tratar excepcion.
		 */
		try {
			/**
			 * Pasamos los parametros para generer el objeto
			 */
			objGTipo.CrearTipoHoja(Referencia_Tipo, Descripcion_Tipo, Precio_Tipo);
			/**
			 * Mensaje para confirmar la introduccion de datos
			 */
			System.out.println("Datos introducidos correctamente!");
		} catch (SQLException e) {
			/**
			 * Mensaje de excepcion
			 */
			System.out.println("No se ha podido realizar el insert: " + e);
		}

	}

	/**
	 * Pedimos los datos a introducir en la entidad Envios.
	 * 
	 * @param objGEnvios objeto envios
	 */
	public static void IntroducirDatosEnvios(ClsGestorLN objGEnvios) {

		/**
		 * variables para recoger los datos de la entidad Envios.
		 */
		int NumeroDeEnvio;
		String NombreCliente;
		String DireccionDeEnvio;
		String PoblacionDeEnvio;
		int CPDeEnvio;
		String ProvinciaDeEnvio;
		int TelefonoDeEnvio;
		int NumeroDeCliente_Envio;

		/**
		 * pedimos los datos.
		 */
		System.out.print("Introduzca el N�mero de Envio:");
		NumeroDeEnvio = UtilidadesLP.leerEntero();
		System.out.print("Introduzca el Nombre del Cliente:");
		NombreCliente = UtilidadesLP.leerCadena();
		System.out.print("Introduzca la Direccion de Envio:");
		DireccionDeEnvio = UtilidadesLP.leerCadena();
		System.out.print("Introduzca la Poblaci�n de Envio:");
		PoblacionDeEnvio = UtilidadesLP.leerCadena();
		System.out.print("Introduzca el Codigo Postal de Envio:");
		CPDeEnvio = UtilidadesLP.leerEntero();
		System.out.print("Introduzca la Provincia de Envio:");
		ProvinciaDeEnvio = UtilidadesLP.leerCadena();
		System.out.print("Introduzca el N�mero de telefono de Envio:");
		TelefonoDeEnvio = UtilidadesLP.leerEntero();
		System.out.print("Introduzca el N�mero de Cliente:");
		NumeroDeCliente_Envio = UtilidadesLP.leerEntero();

		/**
		 * Rodeamos con TRY CATCH para tratar excepcion.
		 */
		try {
			/**
			 * Pasamos los parametros para generer el objeto
			 */
			objGEnvios.CrearEnvios(NumeroDeEnvio, NombreCliente, DireccionDeEnvio, PoblacionDeEnvio, CPDeEnvio,
					ProvinciaDeEnvio, TelefonoDeEnvio, NumeroDeCliente_Envio);
			/**
			 * Mensaje para confirmar la introduccion de datos
			 */
			System.out.println("Datos introducidos correctamente!");
		} catch (SQLException e) {
			/**
			 * Mensaje de excepcion
			 */
			System.out.println("No se ha podido realizar el insert: " + e);

		}
	}

	/**
	 * Pedimos los datos a introducir en la entidad Clientes.
	 * 
	 * @param objGClientes objeto clientes
	 */
	public static void IntroducirDatosClientes(ClsGestorLN objGClientes) {

		/**
		 * variables para recoger los datos de la entidad Clientes.
		 */
		int NumeroDeCliente;
		String NombreYApellidos;
		String DNI_NIF;
		String DireccionDeCliente;
		String Provincia;
		int Telefono;
		String Email;
		boolean correcto = false;
		ClsComprobarDNI_NIF objComprobarDNI_NIF = new ClsComprobarDNI_NIF();

		/**
		 * pedimos los datos.
		 */
		System.out.print("Introduzca N�mero de Cliente:");
		NumeroDeCliente = UtilidadesLP.leerEntero();
		System.out.print("Introduzca el Nombre y los Apellidos del cliente:");
		NombreYApellidos = UtilidadesLP.leerCadena();
		/**
		 * Comprobamos el DNI o NIF es correcto
		 */
		do {
			System.out.print("Introduzca el DNI/NIF del cliente:");
			DNI_NIF = UtilidadesLP.leerCadena().toUpperCase();

			try {
				correcto = objComprobarDNI_NIF.ComprobarDNI_NIF(DNI_NIF);
			} catch (clsDNI_Excepcion e) {
				/**
				 * Mensaje de excepcion
				 */
				System.out.println(e.getMessage());

			}
		} while (!correcto);
		System.out.print("Introduzca la direcci�n del cliente:");
		DireccionDeCliente = UtilidadesLP.leerCadena();
		System.out.print("Introduzca la provincia del cliente:");
		Provincia = UtilidadesLP.leerCadena();
		System.out.print("Introduzca el n�mero de telefono del cliente:");
		Telefono = UtilidadesLP.leerEntero();
		System.out.print("Introduzca el email del cliente:");
		Email = UtilidadesLP.leerCadena();

		/**
		 * Rodeamos con TRY CATCH para tratar excepcion.
		 */
		try {
			/**
			 * Pasamos los parametros para generer el objeto
			 */
			objGClientes.CrearClientes(NumeroDeCliente, NombreYApellidos, DNI_NIF, DireccionDeCliente, Provincia,
					Telefono, Email);
			/**
			 * Mensaje para confirmar la introduccion de datos
			 */
			System.out.println("Datos introducidos correctamente!");
		} catch (SQLException e) {
			/**
			 * Mensaje de excepcion
			 */
			System.out.println("No se ha podido realizar el insert: " + e);
		}
	}

	/**
	 * Pedimos los datos a introducir en la entidad Articulo
	 * 
	 * @param objGArticulos objeto articulos
	 */
	public static void IntroducirDatosArticulos(ClsGestorLN objGArticulos) {

		/**
		 * variables para recoger los datos de la entidad Articulos.
		 */
		int Referencia;
		int Serie;
		String Descripcion;
		int CantidadMaterial;
		double Precio;

		/**
		 * pedimos los datos.
		 */
		System.out.print("Introduzca el n�mero de referencia del art�culo:");
		Referencia = UtilidadesLP.leerEntero();
		System.out.print("Introduzca el numero de la serie:");
		Serie = UtilidadesLP.leerEntero();
		System.out.print("Introduzca la descripci�n del articulo:");
		Descripcion = UtilidadesLP.leerCadena();
		System.out.print("Introduzca la cantidad de material:");
		CantidadMaterial = UtilidadesLP.leerEntero();
		System.out.print("Introduzca el precio del art�culo");
		Precio = UtilidadesLP.leerReal();

		/**
		 * Rodeamos con TRY CATCH para tratar excepcion.
		 */
		try {
			/**
			 * Pasamos los parametros para generer el objeto
			 */
			objGArticulos.CrearArticulos(Referencia, Serie, Descripcion, CantidadMaterial, Precio);
			/**
			 * Mensaje para confirmar la introduccion de datos
			 */
			System.out.println("Datos introducidos correctamente!");
		} catch (SQLException e) {
			/**
			 * Mensaje de excepcion
			 */
			System.out.println("No se ha podido realizar el insert: " + e);
		}

	}

	/**
	 * pedimos los datos a introducir en la entidad Desglose de Pedidos.
	 * 
	 * @param objGDesgloseDePedido objeto desglose de pedido
	 */
	public static void IntroducirDatosDesgloseDePedido(ClsGestorLN objGDesgloseDePedido) {

		/**
		 * variables para recoger los datos de la entidad Desglose de Pedido.
		 */
		int NumeroDePedido;
		int ReferenciaDelArticulo;
		int Serie;
		String Tipo;
		String Tamano;
		int Pedidos_ndesglose;

		/**
		 * pedimos los datos.
		 */
		System.out.print("Introduzca el Numero de Desglose:");
		NumeroDePedido = UtilidadesLP.leerEntero();
		System.out.print("Introduzca la Referencia del Art�culo:");
		ReferenciaDelArticulo = UtilidadesLP.leerEntero();
		System.out.print("Introduzca el Numero de Serie:");
		Serie = UtilidadesLP.leerEntero();
		System.out.print("Introduzca el Tipo:");
		Tipo = UtilidadesLP.leerCadena();
		System.out.print("Introduzca el tama�o");
		Tamano = UtilidadesLP.leerCadena();
		Pedidos_ndesglose = UtilidadesLP.leerEntero();

		/**
		 * Rodeamos con TRY CATCH para tratar excepcion.
		 */
		try {
			/**
			 * llamada al metodo Crear Desgloses con paso de parametros
			 */
			objGDesgloseDePedido.CrearDesgloseDePedido(NumeroDePedido, ReferenciaDelArticulo, Serie, Tamano, Tipo,
					Pedidos_ndesglose);
			/**
			 * Mensaje para confirmar la introduccion de datos
			 */
			System.out.println("Datos introducidos correctamente!");
		} catch (SQLException e) {
			/**
			 * Mensaje de excepcion
			 */
			System.out.println("No se ha podido realizar el insert: " + e);
		}

	}

}
