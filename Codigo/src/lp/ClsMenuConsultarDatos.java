package lp;

import java.sql.SQLException;

import ln.ClsGestorLN;

/**
 * Creamos un menu para poder consultar los diferentes tipos de datos que
 * tengamos en nuestra base de datos.
 * 
 * @author Aitor Ubierna
 */
public class ClsMenuConsultarDatos {

	/**
	 * Metodo que reune el menu de consultar datos
	 * 
	 * @param objGestorMC     recibe el objeto gestor
	 * @param objMostrarDatos recibe el objeto de la clase mostrar datos
	 * @throws SQLException fallo bbdd
	 */
	public static void MenuConsultarDatos(ClsGestorLN objGestorMC, ClsMostrarDatos objMostrarDatos)
			throws SQLException {

		/**
		 * 
		 * Variable para guardar la opcion elegida por el usuario.
		 *
		 */
		int opcionConsultar;

		do {
			/**
			 * 
			 * Mostramos al usuario las opciones que tiene para elegir a la hora de
			 * consultar datos.
			 *
			 */
			System.out.println("Elije la entidad de la que deseas consultar datos: ");
			System.out.println("1- Consultar datos de las Tapas: \n" + "2- Consultar datos de Series: \n"
					+ "3- Consultar datos de Pedidos: \n" + "4- Consultar datos de Materiales: \n"
					+ "5- Consultar datos de Tipos de Hoja: \n" + "6- Consultar datos de Envios: \n"
					+ "7- Consultar datos de Clientes: \n" + "8- Consultar datos de Articulos: \n"
					+ "9- Consultar datos de Desglose de Pedido: \n" + "10- Regresar al Menu Principal: ");

			/**
			 * 
			 * Pedimos los datos que deseen introducir.
			 *
			 */
			opcionConsultar = UtilidadesLP.leerEntero();
			switch (opcionConsultar) {
			case 1:
				ConsultarDatosTapas(objGestorMC, objMostrarDatos);
				break;

			case 2:
				ConsultarDatosSeries(objGestorMC, objMostrarDatos);
				break;

			case 3:
				ConsultarDatosPedidos(objGestorMC, objMostrarDatos);
				break;

			case 4:
				ConsultarDatosMateriales(objGestorMC, objMostrarDatos);
				break;

			case 5:
				ConsultarDatosTipos(objGestorMC, objMostrarDatos);
				break;

			case 6:
				ConsultarDatosEnvios(objGestorMC, objMostrarDatos);
				break;

			case 7:
				ConsultarDatosClientes(objGestorMC, objMostrarDatos);
				break;

			case 8:
				ConsultarDatosArticulos(objGestorMC, objMostrarDatos);
				break;

			case 9:
				ConsultarDatosDesgloseDePedido(objGestorMC, objMostrarDatos);
				break;

			default:
				break;
			}

			/**
			 * Si la opcion es 10 regresamos al menu principal
			 */
		} while (opcionConsultar != 10);

	}

	/**
	 * Pedimos los datos a consultar en la entidad Tapas
	 * 
	 * @param ObjGestCTapas    relacion con LN
	 * @param objMostrarDatosSU relacion con ClsMostrarDatos
	 * @throws SQLException fallo bbdd
	 */
	public static void ConsultarDatosTapas(ClsGestorLN ObjGestCTapas, ClsMostrarDatos objMostrarDatosSU)
			throws SQLException {

		/**
		 * LLamada al metodo Ver Suelas
		 */
		objMostrarDatosSU.VerTapas(ObjGestCTapas);
	}

	/**
	 * Pedimos los datos a consultar en la entidad Series.
	 * 
	 * @param ObjGestCSeries    relacion con LN
	 * @param objMostrarDatosSE relacion con ClsMostrarDatos
	 * @throws SQLException fallo bbdd
	 */
	public static void ConsultarDatosSeries(ClsGestorLN ObjGestCSeries, ClsMostrarDatos objMostrarDatosSE)
			throws SQLException {

		/**
		 * Llamada al metodo Ver Series.
		 */
		objMostrarDatosSE.VerSeries(ObjGestCSeries);
	}

	/**
	 * Pedimos los datos a consultar en la entidad Pedidos.
	 * 
	 * @param ObjGestCPedidos  relacion con LN
	 * @param objMostrarDatosP relacion con ClsMostrarDatos
	 * @throws SQLException fallo bbdd
	 */
	public static void ConsultarDatosPedidos(ClsGestorLN ObjGestCPedidos, ClsMostrarDatos objMostrarDatosP)
			throws SQLException {

		/**
		 * Llamada al metodo ver datos
		 */
		objMostrarDatosP.VerPedidos(ObjGestCPedidos);

	}

	/**
	 * Pedimos los datos a consultar en la entidad Materiales.
	 * 
	 * @param ObjGestCMateriales relacion con LN
	 * @param objMostrarDatosM   relacion con ClsMostrarDatos
	 * @throws SQLException fallo bbdd
	 */
	public static void ConsultarDatosMateriales(ClsGestorLN ObjGestCMateriales, ClsMostrarDatos objMostrarDatosM)
			throws SQLException {

		/**
		 * Llamada al metodo mostrar Materiales
		 */
		objMostrarDatosM.VerMateriales(ObjGestCMateriales);
	}

	/**
	 * Pedimos los datos a consultar en la entidad Tipos de Hoja.
	 * 
	 * @param ObjGestCTipo relacion con LN
	 * @param objMostrarDatosT relacion con ClsMostrarDatos
	 * @throws SQLException fallo bbdd
	 */
	public static void ConsultarDatosTipos(ClsGestorLN ObjGestCTipo, ClsMostrarDatos objMostrarDatosT)
			throws SQLException {

		/**
		 * Llamada al metodo mostrar Tipos de Hoja
		 */
		objMostrarDatosT.VerTipos(ObjGestCTipo);

	}

	/**
	 * Pedimos los datos a consultar en la entidad Envios
	 * 
	 * @param ObjGestCEnvios   relacion con LN
	 * @param objMostrarDatosE relacion con ClsMostrarDatos
	 * @throws SQLException fallo bbdd
	 */
	public static void ConsultarDatosEnvios(ClsGestorLN ObjGestCEnvios, ClsMostrarDatos objMostrarDatosE)
			throws SQLException {
		/**
		 * Llamamos al metodo ver Envios.
		 */
		objMostrarDatosE.VerEnvios(ObjGestCEnvios);

	}

	/**
	 * Pedimos los datos a consultar en la entidad Clientes.
	 * 
	 * @param ObjGestCClientes relacion con LN
	 * @param objMostrarDatosC relacion con ClsMostrarDatos
	 * @throws SQLException fallo bbdd
	 */
	public static void ConsultarDatosClientes(ClsGestorLN ObjGestCClientes, ClsMostrarDatos objMostrarDatosC)
			throws SQLException {

		/**
		 * Llamamos al metodo ver clientes
		 */
		objMostrarDatosC.VerClientes(ObjGestCClientes);
	}

	/**
	 * Pedimos los datos a consultar en la entidad Articulo
	 * 
	 * @param ObjGestCArticulos relacion con LN
	 * @param objMostrarDatosA  relacion con ClsMostrarDatos
	 * @throws SQLException fallo bbdd
	 */
	public static void ConsultarDatosArticulos(ClsGestorLN ObjGestCArticulos, ClsMostrarDatos objMostrarDatosA)
			throws SQLException {
		/**
		 * Llamamos al metodo ver Articulos.
		 */
		objMostrarDatosA.VerArticulos(ObjGestCArticulos);
	}

	/**
	 * Pedimos los datos a consultar en la entidad Desglose de Pedidos.
	 * 
	 * @param ObjGestCDesglose relacion con LN
	 * @param objMostrarDatosD relacion con ClsMostrarDatos
	 * @throws SQLException fallo bbdd
	 */
	public static void ConsultarDatosDesgloseDePedido(ClsGestorLN ObjGestCDesglose, ClsMostrarDatos objMostrarDatosD)
			throws SQLException {

		/**
		 * Llamamos al metodo ver Desgloses.
		 */
		objMostrarDatosD.VerDesgloses(ObjGestCDesglose);

	}
}
