package excepciones;

/**
 * Clase para generar la excepcion y el mensaje de error.
 * 
 * @author Aitor Ubierna
 *
 */
public class clsDNI_Excepcion extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Mensaje que mandamos en caso de excepcion.
	 */
	private final String MENSAJE = "El DNI o NIF es incorrecto";

	/**
	 * Devolvemos el mansaje en caso de que salte la excepcion
	 */
	@Override
	public String getMessage() {
		return MENSAJE;
	}

}
