package excepciones;

/**
 * Clase para generar una excepcion implicita
 * 
 * @author Aitor Ubierna
 *
 */

public class clsRunTimeExcepcion extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Mensaje que mandamos en caso de excepcion.
	 */
	private final String MENSAJE;

	/**
	 * Generamos un mensaje en funcion de la propiedad pedida
	 * 
	 * @param propiedad parametro de propiedad
	 */
	public clsRunTimeExcepcion(String propiedad) {
		MENSAJE = "La propiedad pedida " + propiedad + " no existe";
	}

	/**
	 * Devolvemos el mansaje en caso de que salte la excepcion
	 */
	@Override
	public String getMessage() {
		return MENSAJE;
	}

}
