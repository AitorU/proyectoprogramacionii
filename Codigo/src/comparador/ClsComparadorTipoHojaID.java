package comparador;

import java.util.Comparator;

import ln.ClsTipoHoja;

/**
 * Clase para comparar el ID de suelas
 * 
 * @author Aitor Ubierna
 *
 */
public class ClsComparadorTipoHojaID implements Comparator<ClsTipoHoja> {

	/**
	 * Metodo para comparar la Referencia de las suelas
	 */
	@Override
	public int compare(ClsTipoHoja ID0, ClsTipoHoja ID1) {

		Integer a = ID0.getReferencia();
		Integer b = ID1.getReferencia();

		return a.compareTo(b);
	}

}
