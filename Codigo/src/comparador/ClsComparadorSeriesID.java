package comparador;

import java.util.Comparator;

import ln.ClsSeries;

/**
 * Clase para ordenar los IDs de Series
 * 
 * @author Aitor Ubierna
 *
 */
public class ClsComparadorSeriesID implements Comparator<ClsSeries> {

	/**
	 * Metodo para comparar los numeros de las series
	 */
	@Override
	public int compare(ClsSeries ID0, ClsSeries ID1) {

		Integer a = ID0.getNumeroDeSerie();
		Integer b = ID1.getNumeroDeSerie();

		return a.compareTo(b);
	}

}
