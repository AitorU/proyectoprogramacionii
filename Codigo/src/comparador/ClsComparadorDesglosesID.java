package comparador;

import java.util.Comparator;

import ln.ClsDesgloseDePedido;

/**
 * Clase para comparar desgloses por id
 * 
 * @author Aitor Ubierna
 *
 */
public class ClsComparadorDesglosesID implements Comparator<ClsDesgloseDePedido> {

	/**
	 * Metodo para comparar el Numero de pedido dentro de la tabla desgloses
	 */
	@Override
	public int compare(ClsDesgloseDePedido ID0, ClsDesgloseDePedido Id1) {

		/**
		 * Variables que recogen el Numero de Pedidos para compararlos
		 */
		Integer a = ID0.getNumeroDePedido();
		Integer b = Id1.getNumeroDePedido();

		return a.compareTo(b);
	}

}
