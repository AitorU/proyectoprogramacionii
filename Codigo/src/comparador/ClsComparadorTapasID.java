package comparador;

import java.util.Comparator;

import ln.ClsTapas;

/**
 * Clase para comparar los IDs de las tapas
 * 
 * @author Aitor Ubierna
 */
public class ClsComparadorTapasID implements Comparator<ClsTapas> {

	/**
	 * Metodo para comparar la Referencia de los Herrajes
	 */
	@Override
	public int compare(ClsTapas ID0, ClsTapas ID1) {

		/**
		 * Variables que recogen la Referencia para compararlos
		 */
		Integer a = ID0.getReferencia();
		Integer b = ID1.getReferencia();

		return a.compareTo(b);
	}

}
