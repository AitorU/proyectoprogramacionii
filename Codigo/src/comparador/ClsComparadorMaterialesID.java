package comparador;

import java.util.Comparator;

import ln.ClsMateriales;

/**
 * Clase para comparar Materiales por ID
 * 
 * @author Aitor Ubierna
 *
 */
public class ClsComparadorMaterialesID implements Comparator<ClsMateriales> {

	/**
	 * Metodo para comparar la Referencia de los Materiales
	 */
	@Override
	public int compare(ClsMateriales ID0, ClsMateriales ID1) {

		Integer a = ID0.getReferencia();
		Integer b = ID1.getReferencia();

		return a.compareTo(b);
	}

}
