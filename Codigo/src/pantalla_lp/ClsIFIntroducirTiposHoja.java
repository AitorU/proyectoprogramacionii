package pantalla_lp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JInternalFrame;

import ln.ClsGestorLN;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import static comun.clsConstantes.PROPIEDAD_TIPOHOJA_REFERENCIA;

import java.awt.Font;
import javax.swing.SwingConstants;

import comun.ItfProperty;
import javax.swing.ImageIcon;

/**
 * Internar Frame para introducir Suelas
 * 
 * @author Aitor Ubierna
 *
 */
public class ClsIFIntroducirTiposHoja extends JInternalFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	/**
	 * Objetos
	 */
	private JTextField CasillaNTipos;
	private JTextField CasillaDesTipos;
	private JTextField CasillaPrecioTipos;
	private JButton NTiposAuto, ConfirmarTipo;
	private int Ref;
	private Double Prec;
	/**
	 * Para tener el Gestor
	 */
	private ClsGestorLN objGestorIFIM;
	/**
	 * constantes para el ActionLisener
	 */
	private final String CONFIRMAR_BUTTON = "Boton de confirmar Suelas";
	private final String AUTOMATICO_BUTTON = "Boton para poner automatico el ID";

	/**
	 * Creamos el frame
	 * 
	 * @param ObjGestor recibimos el gestor
	 */
	public ClsIFIntroducirTiposHoja(ClsGestorLN ObjGestor) {
		setFrameIcon(new ImageIcon(ClsIFIntroducirTiposHoja.class.getResource("/PANTALLA_LP/DEUSTO.png")));
		setResizable(false);
		setMaximizable(true);
		setIconifiable(true);
		setClosable(true);
		setTitle("Introducir Tipos de Hoja");
		this.setBounds(75, 75, 600, 300);
		getContentPane().setLayout(new GridLayout(4, 3));

		Inicializar(ObjGestor);
	}

	/**
	 * Creamos lo objetos del frame
	 * 
	 * @param ObjGestor recibimos el gestor
	 */
	private void Inicializar(ClsGestorLN ObjGestor) {
		JLabel TxtNSuelas = new JLabel("Numero de Suela:");
		TxtNSuelas.setHorizontalAlignment(SwingConstants.CENTER);
		TxtNSuelas.setFont(new Font("Tahoma", Font.BOLD, 15));
		TxtNSuelas.setEnabled(false);
		getContentPane().add(TxtNSuelas);

		CasillaNTipos = new JTextField();
		CasillaNTipos.setFont(new Font("Tahoma", Font.BOLD, 15));
		CasillaNTipos.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(CasillaNTipos);
		CasillaNTipos.setColumns(10);

		NTiposAuto = new JButton("N\u00BA Suela Automatico");
		NTiposAuto.setFont(new Font("Tahoma", Font.PLAIN, 15));
		getContentPane().add(NTiposAuto);
		NTiposAuto.addActionListener(this);
		NTiposAuto.setActionCommand(AUTOMATICO_BUTTON);

		JLabel TextDesTipos = new JLabel("Descripcion:");
		TextDesTipos.setHorizontalAlignment(SwingConstants.CENTER);
		TextDesTipos.setFont(new Font("Tahoma", Font.BOLD, 15));
		TextDesTipos.setEnabled(false);
		getContentPane().add(TextDesTipos);

		CasillaDesTipos = new JTextField();
		CasillaDesTipos.setFont(new Font("Tahoma", Font.BOLD, 15));
		CasillaDesTipos.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(CasillaDesTipos);
		CasillaDesTipos.setColumns(10);

		JLabel Vacio1 = new JLabel("");
		Vacio1.setEnabled(false);
		getContentPane().add(Vacio1);

		JLabel TxtPrecioTipos = new JLabel("Precio:");
		TxtPrecioTipos.setFont(new Font("Tahoma", Font.BOLD, 15));
		TxtPrecioTipos.setEnabled(false);
		TxtPrecioTipos.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(TxtPrecioTipos);

		CasillaPrecioTipos = new JTextField();
		CasillaPrecioTipos.setHorizontalAlignment(SwingConstants.CENTER);
		CasillaPrecioTipos.setFont(new Font("Tahoma", Font.BOLD, 15));
		getContentPane().add(CasillaPrecioTipos);
		CasillaPrecioTipos.setColumns(10);

		JLabel Vacio2 = new JLabel("");
		Vacio2.setEnabled(false);
		getContentPane().add(Vacio2);

		JLabel Vacio3 = new JLabel("");
		Vacio3.setEnabled(false);
		getContentPane().add(Vacio3);

		JLabel Vacio4 = new JLabel("");
		getContentPane().add(Vacio4);

		ConfirmarTipo = new JButton("Confirmar Tipo de Hoja");
		ConfirmarTipo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		ConfirmarTipo.addActionListener(this);
		ConfirmarTipo.setActionCommand(CONFIRMAR_BUTTON);
		getContentPane().add(ConfirmarTipo);

		objGestorIFIM = ObjGestor;

	}

	/**
	 * Escuchador
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case AUTOMATICO_BUTTON:
			ObtenerUltimoID();
			break;

		case CONFIRMAR_BUTTON:
			Comprobar();
			break;

		default:
			break;
		}

	}

	/**
	 * Metodo para comprobar que los valores introducidos son adecuados
	 */
	private void Comprobar() {

		boolean comprobado = true;

		try {
			Ref = Integer.parseInt(CasillaNTipos.getText());
		} catch (Exception e) {
			comprobado = false;
			JOptionPane.showMessageDialog(null, "Numero de Suela incorrecto o vacio");
		}

		if (!CasillaPrecioTipos.getText().equals("")) {
			try {
				Prec = Double.parseDouble(CasillaPrecioTipos.getText());
			} catch (Exception e) {
				comprobado = false;
				JOptionPane.showMessageDialog(null, "El precio no puede ser texto");
			}
		}
		if (CasillaDesTipos.getText().equals("")) {
			comprobado = false;
			JOptionPane.showMessageDialog(null, "La descripción no puede estar vacia");
		}

		if (comprobado) {
			MandarAGestor();
			PonerVacio();
		}
	}

	/**
	 * Metodo para dejar vacios los huecos
	 */
	private void PonerVacio() {

		CasillaNTipos.setText("");
		CasillaDesTipos.setText("");
		CasillaPrecioTipos.setText("");

	}

	/**
	 * Mandamos datos al gestor
	 */
	private void MandarAGestor() {

		String Desc = CasillaDesTipos.getText();

		try {
			if (objGestorIFIM.CrearTipoHoja(Ref, Desc, Prec)) {
				JOptionPane.showMessageDialog(null, "Tipo de hoja introducido correctamente");
			} else {
				JOptionPane.showMessageDialog(null, "ID del tipo de hoja repetido!");
			}

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "No se ha podido realizar el insert: " + e);
		}

	}

	/**
	 * Metodo para obtener automaticamente el siguiente ID.
	 */
	private void ObtenerUltimoID() {

		ArrayList<ItfProperty> Suelas = objGestorIFIM.OrdenarTipoHoja();

		ItfProperty UltimoObjeto = Suelas.get(Suelas.size() - 1);

		int mostrar = UltimoObjeto.getIntegerProperty(PROPIEDAD_TIPOHOJA_REFERENCIA);

		int IDActualizadoINT = mostrar + 1;

		String IDActualizado = Integer.toString(IDActualizadoINT);

		CasillaNTipos.setText(IDActualizado);

	}

}
