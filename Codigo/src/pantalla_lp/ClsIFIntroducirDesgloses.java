package pantalla_lp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JInternalFrame;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JSplitPane;
import javax.swing.JButton;

import static comun.clsConstantes.PROPIEDAD_SERIES_NUMERO_DE_SERIE;
import static comun.clsConstantes.PROPIEDAD_PEDIDOS_NUMERO_DE_PEDIDO;
import static comun.clsConstantes.PROPIEDAD_ARTICULO_REFERENCIA;
import static comun.clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_DESGLOSE;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

import comun.ItfProperty;
import ln.ClsGestorLN;
import Tablas.ClsTablaArticulos;
import Tablas.ClsTablaPedidos;
import Tablas.ClsTablaSeries;

import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class ClsIFIntroducirDesgloses extends JInternalFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	/**
	 * Objetos de la clase
	 */
	private JTabbedPane Pesta�as;
	private JTextField RecogerNDesglose, RecogerNColor, RecogerCantidad;
	private JPanel General, Cantidades, Articulos;
	private JButton BotonConfirmar;
	private JComboBox<Integer> RecogerNPedido, RecogerRefArt, RecogerNSerie;
	private JSplitPane SeriesPedidos;
	private JScrollPane PanelPedidos, PanelArticulos, PanelSeries;
	private JTable TablaSeries, TablaArticulos, TablaPedidos;

	/**
	 * ArrayList para las tablas
	 */
	private ArrayList<ItfProperty> Pedidos, Series, ArticulosT;

	/**
	 * Para el Lisener
	 */
	private final String CONFIRMAR_BUTTON = "Boton de confirmar Desgloses";

	/**
	 * Para tener el Gestor
	 */
	private ClsGestorLN objGestorIFIDes;

	/**
	 * Constructor
	 * 
	 * @param ObjGestor recibe el gestor
	 */
	public ClsIFIntroducirDesgloses(ClsGestorLN ObjGestor) {
		setTitle("Introducir Desgloses");
		setFrameIcon(new ImageIcon(ClsIFIntroducirDesgloses.class.getResource("/PANTALLA_LP/DEUSTO.png")));
		setIconifiable(true);
		setClosable(true);
		getContentPane().setLayout(null);
		this.setBounds(175, 175, 491, 301);

		Inicializar(ObjGestor);
		ObtenerUltimoID();
	}

	/**
	 * Metodo para inicializar objetos
	 * 
	 * @param ObjGestor recibe el gestor
	 */
	private void Inicializar(ClsGestorLN ObjGestor) {

		objGestorIFIDes = ObjGestor;

		Pedidos = objGestorIFIDes.OrdenarPedidos();
		Series = objGestorIFIDes.ObtenerSeries();
		ArticulosT = objGestorIFIDes.ObtenerArticulos();

		Pesta�as = new JTabbedPane(JTabbedPane.TOP);
		Pesta�as.setBounds(0, 0, 475, 271);
		getContentPane().add(Pesta�as);

		General = new JPanel();
		Pesta�as.addTab("General", null, General, null);
		General.setBounds(175, 175, 445, 290);
		General.setLayout(null);

		JLabel TxtNDesglose = new JLabel("N\u00FAmero de Desglose:");
		TxtNDesglose.setBounds(10, 11, 166, 19);
		General.add(TxtNDesglose);
		TxtNDesglose.setEnabled(false);
		TxtNDesglose.setFont(new Font("Tahoma", Font.BOLD, 15));

		BotonConfirmar = new JButton("Confirmar Desglose");
		BotonConfirmar.setBounds(311, 216, 159, 27);
		General.add(BotonConfirmar);
		BotonConfirmar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		BotonConfirmar.addActionListener(this);
		BotonConfirmar.setActionCommand(CONFIRMAR_BUTTON);

		RecogerNDesglose = new JTextField();
		RecogerNDesglose.setBounds(186, 11, 70, 19);
		General.add(RecogerNDesglose);
		RecogerNDesglose.setHorizontalAlignment(SwingConstants.CENTER);
		RecogerNDesglose.setFont(new Font("Tahoma", Font.PLAIN, 15));
		RecogerNDesglose.setEnabled(false);
		RecogerNDesglose.setEditable(false);
		RecogerNDesglose.setColumns(10);

		JLabel TxtNPedido = new JLabel("N\u00FAmero de Pedido:");
		TxtNPedido.setEnabled(false);
		TxtNPedido.setFont(new Font("Tahoma", Font.BOLD, 15));
		TxtNPedido.setBounds(10, 57, 146, 19);
		General.add(TxtNPedido);

		RecogerNPedido = new JComboBox<Integer>();
		RecogerNPedido.setFont(new Font("Tahoma", Font.PLAIN, 15));
		RecogerNPedido.setBounds(166, 57, 49, 22);
		General.add(RecogerNPedido);

		JLabel TxtRefArt = new JLabel("Referencia del Art\u00EDculo:");
		TxtRefArt.setEnabled(false);
		TxtRefArt.setFont(new Font("Tahoma", Font.BOLD, 15));
		TxtRefArt.setBounds(10, 133, 177, 19);
		General.add(TxtRefArt);

		RecogerRefArt = new JComboBox<Integer>();
		RecogerRefArt.setFont(new Font("Tahoma", Font.PLAIN, 15));
		RecogerRefArt.setBounds(207, 131, 49, 22);
		General.add(RecogerRefArt);

		JLabel TxtCantidad = new JLabel("Cantidad:");
		TxtCantidad.setEnabled(false);
		TxtCantidad.setFont(new Font("Tahoma", Font.BOLD, 15));
		TxtCantidad.setBounds(10, 87, 92, 19);
		General.add(TxtCantidad);

		RecogerCantidad = new JTextField();
		RecogerCantidad.setFont(new Font("Tahoma", Font.PLAIN, 15));
		RecogerCantidad.setBounds(112, 87, 49, 22);
		General.add(RecogerCantidad);

		JLabel TxtNSerie = new JLabel("N\u00FAmero de Serie:");
		TxtNSerie.setFont(new Font("Tahoma", Font.BOLD, 15));
		TxtNSerie.setEnabled(false);
		TxtNSerie.setBounds(270, 109, 130, 15);
		General.add(TxtNSerie);

		RecogerNSerie = new JComboBox<Integer>();
		RecogerNSerie.setFont(new Font("Tahoma", Font.PLAIN, 15));
		RecogerNSerie.setBounds(410, 105, 49, 22);
		General.add(RecogerNSerie);

		JLabel TxtNColor = new JLabel("N\u00FAmero de Color:");
		TxtNColor.setFont(new Font("Tahoma", Font.BOLD, 15));
		TxtNColor.setEnabled(false);
		TxtNColor.setBounds(68, 163, 130, 19);
		General.add(TxtNColor);

		RecogerNColor = new JTextField();
		RecogerNColor.setHorizontalAlignment(SwingConstants.CENTER);
		RecogerNColor.setFont(new Font("Tahoma", Font.PLAIN, 15));
		RecogerNColor.setBounds(208, 164, 86, 20);
		General.add(RecogerNColor);
		RecogerNColor.setColumns(10);

		SeriesPedidos = new JSplitPane();
		SeriesPedidos.setOrientation(JSplitPane.VERTICAL_SPLIT);
		SeriesPedidos.setResizeWeight(0.5);
		Pesta�as.addTab("Pedidos/Series", null, SeriesPedidos, null);

		Articulos = new JPanel();
		Pesta�as.addTab("Art\u00EDculos", null, Articulos, null);
		Articulos.setLayout(null);

		/**
		 * Llamadas para crear las tablas
		 */
		CrearTablaSeries();
		CrearTablaPedidos();
		CrearTablaArticulos();

		/**
		 * Cargamos combobox series
		 */
		for (ItfProperty a : Series) {

			RecogerNSerie.addItem(a.getIntegerProperty(PROPIEDAD_SERIES_NUMERO_DE_SERIE));
		}

		/**
		 * cargamos combobox articulos
		 */
		for (ItfProperty a : ArticulosT) {

			RecogerRefArt.addItem(a.getIntegerProperty(PROPIEDAD_ARTICULO_REFERENCIA));
		}

		/**
		 * Cargamos combobox pedidos
		 */
		for (ItfProperty a : Pedidos) {

			RecogerNPedido.addItem(a.getIntegerProperty(PROPIEDAD_PEDIDOS_NUMERO_DE_PEDIDO));
		}

	}

	/**
	 * Metodo para crear tabla
	 */
	private void CrearTablaPedidos() {

		ClsTablaPedidos TPedidos = new ClsTablaPedidos(Pedidos);
		DefaultTableCellRenderer Alinear = new DefaultTableCellRenderer();

		TablaPedidos = new JTable(TPedidos);
		Alinear.setHorizontalAlignment(SwingConstants.CENTER);

		JTableHeader header = TablaPedidos.getTableHeader();
		header.setVisible(true);
		header.setFont(new Font("Tahoma", Font.BOLD, 13));

		TablaPedidos.getColumnModel().getColumn(0).setCellRenderer(Alinear);
		TablaPedidos.getColumnModel().getColumn(1).setCellRenderer(Alinear);
		TablaPedidos.getColumnModel().getColumn(2).setCellRenderer(Alinear);
		TablaPedidos.getColumnModel().getColumn(3).setCellRenderer(Alinear);
		TablaPedidos.getColumnModel().getColumn(4).setCellRenderer(Alinear);
		TablaPedidos.getColumnModel().getColumn(5).setCellRenderer(Alinear);
		TablaPedidos.setPreferredScrollableViewportSize(new Dimension(500, 70));
		TablaPedidos.setFillsViewportHeight(true);
		TablaPedidos.setEnabled(false);
		TablaPedidos.setRowSelectionAllowed(true);
		TPedidos.fireTableDataChanged();

		PanelPedidos = new JScrollPane();
		SeriesPedidos.setLeftComponent(PanelPedidos);
		PanelPedidos.setViewportView(TablaPedidos);
		TPedidos.setData(Pedidos);
	}

	/**
	 * Metodo para crear tabla
	 */
	private void CrearTablaArticulos() {

		ClsTablaArticulos TArticulos = new ClsTablaArticulos(ArticulosT);
		DefaultTableCellRenderer Alinear = new DefaultTableCellRenderer();

		TablaArticulos = new JTable(TArticulos);
		Alinear.setHorizontalAlignment(SwingConstants.CENTER);

		JTableHeader header = TablaArticulos.getTableHeader();
		header.setVisible(true);
		header.setFont(new Font("Tahoma", Font.BOLD, 13));

		TablaArticulos.getColumnModel().getColumn(0).setCellRenderer(Alinear);
		TablaArticulos.getColumnModel().getColumn(1).setCellRenderer(Alinear);
		TablaArticulos.getColumnModel().getColumn(2).setCellRenderer(Alinear);
		TablaArticulos.getColumnModel().getColumn(3).setCellRenderer(Alinear);
		TablaArticulos.getColumnModel().getColumn(4).setCellRenderer(Alinear);
		TablaArticulos.getColumnModel().getColumn(5).setCellRenderer(Alinear);
		TablaArticulos.getColumnModel().getColumn(6).setCellRenderer(Alinear);
		TablaArticulos.setPreferredScrollableViewportSize(new Dimension(500, 70));
		TablaArticulos.setFillsViewportHeight(true);
		TablaArticulos.setEnabled(false);
		TablaArticulos.setRowSelectionAllowed(true);
		TArticulos.fireTableDataChanged();

		PanelArticulos = new JScrollPane();
		PanelArticulos.setBounds(0, 25, 470, 207);
		PanelArticulos.setViewportView(TablaArticulos);
		Articulos.add(PanelArticulos);

		JLabel TxtArticulos = new JLabel("Tabla de Art\u00EDculos");
		TxtArticulos.setFont(new Font("Tahoma", Font.BOLD, 15));
		TxtArticulos.setHorizontalAlignment(SwingConstants.CENTER);
		TxtArticulos.setBounds(10, 0, 450, 29);
		Articulos.add(TxtArticulos);
	}

	/**
	 * Metodo para crear tabla
	 */
	private void CrearTablaSeries() {

		ClsTablaSeries TSeries = new ClsTablaSeries(Series);
		DefaultTableCellRenderer Alinear = new DefaultTableCellRenderer();

		TablaSeries = new JTable(TSeries);
		Alinear.setHorizontalAlignment(SwingConstants.CENTER);

		JTableHeader header = TablaSeries.getTableHeader();
		header.setVisible(true);
		header.setFont(new Font("Tahoma", Font.BOLD, 13));

		TablaSeries.getColumnModel().getColumn(0).setCellRenderer(Alinear);
		TablaSeries.getColumnModel().getColumn(1).setCellRenderer(Alinear);
		TablaSeries.setPreferredScrollableViewportSize(new Dimension(500, 70));
		TablaSeries.setFillsViewportHeight(true);
		TablaSeries.setEnabled(false);
		TablaSeries.setRowSelectionAllowed(true);
		TSeries.fireTableDataChanged();

		PanelSeries = new JScrollPane();
		SeriesPedidos.setRightComponent(PanelSeries);
		PanelSeries.setViewportView(TablaSeries);
		TSeries.setData(objGestorIFIDes);
	}

	/**
	 * Escuchador
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

		switch (e.getActionCommand()) {
		case CONFIRMAR_BUTTON:
			Comporobar();
			ObtenerUltimoID();
			break;

		default:
			break;
		}

	}

	/**
	 * Metodo para comprobar que los valores introducidos son adecuados
	 */
	private void Comporobar() {

		boolean comprobado = true;

	}

	/**
	 * Metodo para poner campos vacios
	 */
	private void PonerVacio() {

	}

	/**
	 * Metodo para mandar los datos al gestor
	 */
	private void MandarAGestor() {

		int NumeroDePedido = Integer.parseInt(RecogerNDesglose.getText());
		int ReferenciaDelArticulo = Integer.parseInt(RecogerRefArt.getSelectedItem().toString());
		int Serie = Integer.parseInt(RecogerNSerie.getSelectedItem().toString());

		String Cantidad = RecogerCantidad.getText();
		int Pedidos_NPedido = Integer.parseInt(RecogerNPedido.getSelectedItem().toString());
		String tipo = RecogerNColor.getText();

		try {
			if (objGestorIFIDes.CrearDesgloseDePedido(NumeroDePedido, ReferenciaDelArticulo, Serie, Cantidad, tipo,
					Pedidos_NPedido)) {
				JOptionPane.showMessageDialog(null, "Desglose introducido correctamente");
			} else {
				JOptionPane.showMessageDialog(null, "ID del Desglose repetido!");
			}

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "No se ha podido realizar el insert: " + e);
		}

		PonerVacio();

	}

	/**
	 * Metodo para obtener automaticamente el siguiente ID.
	 */
	private void ObtenerUltimoID() {

		ArrayList<ItfProperty> Desgloses = objGestorIFIDes.OrdenarDesgloses();

		ItfProperty UltimoObjeto = Desgloses.get(Desgloses.size() - 1);

		int mostrar = UltimoObjeto.getIntegerProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_DESGLOSE);

		int IDActualizadoINT = mostrar + 1;

		String IDActualizado = Integer.toString(IDActualizadoINT);

		RecogerNDesglose.setText(IDActualizado);

	}
}
