package pantalla_lp;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

import comun.ItfProperty;
import excepciones.ClsBorrarExcepcion;
import ln.ClsGestorLN;
import Tablas.ClsTablaSeries;
import Tablas.ClsTablaTiposHoja;

import javax.swing.ListSelectionModel;

/**
 * Clase para consultar y borrar suelas
 * 
 * @author Aitor Ubierna
 */
public class ClsConsultarBorrarTiposHoja extends JInternalFrame implements ActionListener, ListSelectionListener {

	private static final long serialVersionUID = 1L;
	private JTable TablaTiposHoja;
	private JLabel TxtTablaTiposHoja;
	private JButton BotonBorrar, BotonActualizar;
	private JScrollPane PanelTipos;
	@SuppressWarnings("unused")
	private ClsTablaSeries TTiposHoja;
	private DefaultTableCellRenderer Alinear;

	/**
	 * Para tener el Gestor
	 */
	private ClsGestorLN objGestorIFCBTH;

	/**
	 * ArrayList para las tablas
	 */
	private ArrayList<ItfProperty> TiposHoja;
	int ObjetoRecuperado;

	/**
	 * Para el Lisener
	 */
	private final String BORRAR_BUTTON = "Boton de confirmar Tipos de Hojas";
	private final String ACTUALIZAR_BUTTON = "Boton de actualizar Tipos de Hojas";

	/**
	 * Constructro
	 * 
	 * @param ObjGestor recibe el gestor
	 */
	public ClsConsultarBorrarTiposHoja(ClsGestorLN ObjGestor) {
		setFrameIcon(new ImageIcon(ClsConsultarBorrarSeries.class.getResource("/PANTALLA_LP/DEUSTO.png")));
		setTitle("Consultar Tipos de Hojas");
		setIconifiable(true);
		setClosable(true);
		getContentPane().setLayout(null);
		this.setBounds(25, 25, 453, 302);
		Inicializar(ObjGestor);
	}

	/**
	 * Metodo inicializador de objetos
	 * 
	 * @param ObjGestor recibe el gestro
	 */
	private void Inicializar(ClsGestorLN ObjGestor) {

		objGestorIFCBTH = ObjGestor;

		TxtTablaTiposHoja = new JLabel("Tipos de Hoja");
		TxtTablaTiposHoja.setEnabled(false);
		TxtTablaTiposHoja.setHorizontalAlignment(SwingConstants.CENTER);
		TxtTablaTiposHoja.setFont(new Font("Tahoma", Font.BOLD, 25));
		TxtTablaTiposHoja.setBounds(10, 11, 414, 23);
		getContentPane().add(TxtTablaTiposHoja);
		CrearTablaTipos();

		BotonBorrar = new JButton("Borrar");
		BotonBorrar.setEnabled(false);
		BotonBorrar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		BotonBorrar.setBounds(335, 236, 89, 23);
		getContentPane().add(BotonBorrar);
		BotonBorrar.addActionListener(this);
		BotonBorrar.setActionCommand(BORRAR_BUTTON);

		BotonActualizar = new JButton("\u27F3");
		BotonActualizar.setBounds(381, 11, 43, 31);
		getContentPane().add(BotonActualizar);
		BotonActualizar.addActionListener(this);
		BotonActualizar.setActionCommand(ACTUALIZAR_BUTTON);
	}

	/**
	 * Metodo para crear tabla
	 */
	private void CrearTablaTipos() {

		TiposHoja = objGestorIFCBTH.ObtenerTiposHoja();

		ClsTablaTiposHoja TTapas = new ClsTablaTiposHoja(TiposHoja);
		Alinear = new DefaultTableCellRenderer();

		TablaTiposHoja = new JTable(TTiposHoja);
		TablaTiposHoja.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JTableHeader header = TablaTiposHoja.getTableHeader();
		header.setVisible(true);
		header.setBackground(Color.black);
		header.setForeground(Color.black);
		header.setFont(new Font("Tahoma", Font.BOLD, 13));

		Alinear.setHorizontalAlignment(SwingConstants.CENTER);
		TablaTiposHoja.getColumnModel().getColumn(0).setCellRenderer(Alinear);
		TablaTiposHoja.getColumnModel().getColumn(1).setCellRenderer(Alinear);
		TablaTiposHoja.getColumnModel().getColumn(2).setCellRenderer(Alinear);
		TablaTiposHoja.setPreferredScrollableViewportSize(new Dimension(500, 70));
		TablaTiposHoja.getSelectionModel().addListSelectionListener(this);
		TablaTiposHoja.setFillsViewportHeight(true);
		TablaTiposHoja.setRowSelectionAllowed(true);
		TTiposHoja.fireTableDataChanged();

		PanelTipos = new JScrollPane();
		PanelTipos.setBounds(10, 45, 414, 190);
		PanelTipos.setViewportView(TablaTiposHoja);
		getContentPane().add(PanelTipos);
		// TSeries.setData(objGestorIFCBS);

	}

	/**
	 * Escuchador
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case BORRAR_BUTTON:

			String dato = String.valueOf(TablaTiposHoja.getValueAt(TablaTiposHoja.getSelectedRow(), 0));
			int NTipo = Integer.parseInt(dato);
			if (PreguntarEntregado() == 0) {
				MandarABorrar(NTipo);
				ActualizarTabla();
			}
			break;

		case ACTUALIZAR_BUTTON:
			BotonBorrar.setEnabled(false);
			ActualizarTabla();
			break;

		default:
			break;
		}

	}

	/**
	 * Metodo para preguntar por la confirmacion de borrado
	 * 
	 * @return devolvemos confirm.
	 */
	private int PreguntarEntregado() {

		return JOptionPane.showConfirmDialog(null, "�Esta seguro de que desea eliminar el registro?", "BORRAR",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

	}

	/**
	 * Metodo para mandar a borrar
	 * 
	 * @param NSuela mandamos parametro
	 */
	private void MandarABorrar(int NTipo) {

		try {
			if (objGestorIFCBTH.EliminarTapasDeArray(NTipo)) {
				JOptionPane.showMessageDialog(null, "Registro eliminado correctamente");

			}
		} catch (SQLException e) {

			JOptionPane.showMessageDialog(null, "No se ha podido realizar el insert: " + e);
		} catch (ClsBorrarExcepcion e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}

	}

	/**
	 * Escuchador de tabla
	 */
	@Override
	public void valueChanged(ListSelectionEvent e) {

		int Seleccionado = TablaTiposHoja.getSelectedRowCount();

		if (Seleccionado > 0) {
			BotonBorrar.setEnabled(true);
		}

	}

	/**
	 * Metodo para actualizar la tabla
	 */
	private void ActualizarTabla() {
		TablaTiposHoja.setVisible(false);
		TiposHoja = objGestorIFCBTH.ObtenerTiposHoja();
		ClsTablaTiposHoja TablaActualizada = new ClsTablaTiposHoja(TiposHoja);
		TablaTiposHoja.setModel(TablaActualizada);
		Alinear.setHorizontalAlignment(SwingConstants.CENTER);
		TablaTiposHoja.getColumnModel().getColumn(0).setCellRenderer(Alinear);
		TablaTiposHoja.getColumnModel().getColumn(1).setCellRenderer(Alinear);
		TablaTiposHoja.getColumnModel().getColumn(2).setCellRenderer(Alinear);
		TablaTiposHoja.setVisible(true);
	}

}
