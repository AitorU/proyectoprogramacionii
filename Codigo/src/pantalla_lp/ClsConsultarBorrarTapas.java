package pantalla_lp;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

import comun.ItfProperty;
import excepciones.ClsBorrarExcepcion;
import ln.ClsGestorLN;
import Tablas.ClsTablaSeries;
import Tablas.ClsTablaTapas;

import javax.swing.ListSelectionModel;

/**
 * Clase para consultar y borrar suelas
 * 
 * @author Aitor Ubierna
 */
public class ClsConsultarBorrarTapas extends JInternalFrame implements ActionListener, ListSelectionListener {
	private static final long serialVersionUID = 1L;
	private JTable TablaTapas;
	private JLabel TxtTablaTapas;
	private JButton BotonBorrar, BotonActualizar;
	private JScrollPane PanelTapas;
	@SuppressWarnings("unused")
	private ClsTablaSeries TTapas;
	private DefaultTableCellRenderer Alinear;

	/**
	 * Para tener el Gestor
	 */
	private ClsGestorLN objGestorIFCBTa;

	/**
	 * ArrayList para las tablas
	 */
	private ArrayList<ItfProperty> Tapas;
	int ObjetoRecuperado;

	/**
	 * Para el Lisener
	 */
	private final String BORRAR_BUTTON = "Boton de confirmar Tapas";
	private final String ACTUALIZAR_BUTTON = "Boton de actualizar Tapas";

	/**
	 * Constructro
	 * 
	 * @param ObjGestor recibe el gestor
	 */
	public ClsConsultarBorrarTapas(ClsGestorLN ObjGestor) {
		setFrameIcon(new ImageIcon(ClsConsultarBorrarSeries.class.getResource("/PANTALLA_LP/DEUSTO.png")));
		setTitle("Consultar Tapas");
		setIconifiable(true);
		setClosable(true);
		getContentPane().setLayout(null);
		this.setBounds(25, 25, 453, 302);
		Inicializar(ObjGestor);
	}

	/**
	 * Metodo inicializador de objetos
	 * 
	 * @param ObjGestor recibe el gestro
	 */
	private void Inicializar(ClsGestorLN ObjGestor) {

		objGestorIFCBTa = ObjGestor;

		TxtTablaTapas = new JLabel("Tapas");
		TxtTablaTapas.setEnabled(false);
		TxtTablaTapas.setHorizontalAlignment(SwingConstants.CENTER);
		TxtTablaTapas.setFont(new Font("Tahoma", Font.BOLD, 25));
		TxtTablaTapas.setBounds(10, 11, 414, 23);
		getContentPane().add(TxtTablaTapas);
		CrearTablaTapas();

		BotonBorrar = new JButton("Borrar");
		BotonBorrar.setEnabled(false);
		BotonBorrar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		BotonBorrar.setBounds(335, 236, 89, 23);
		getContentPane().add(BotonBorrar);
		BotonBorrar.addActionListener(this);
		BotonBorrar.setActionCommand(BORRAR_BUTTON);

		BotonActualizar = new JButton("\u27F3");
		BotonActualizar.setBounds(381, 11, 43, 31);
		getContentPane().add(BotonActualizar);
		BotonActualizar.addActionListener(this);
		BotonActualizar.setActionCommand(ACTUALIZAR_BUTTON);
	}

	/**
	 * Metodo para crear tabla
	 */
	private void CrearTablaTapas() {

		Tapas = objGestorIFCBTa.ObtenerTapas();

		ClsTablaTapas TTapas = new ClsTablaTapas(Tapas);
		Alinear = new DefaultTableCellRenderer();

		TablaTapas = new JTable(TTapas);
		TablaTapas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JTableHeader header = TablaTapas.getTableHeader();
		header.setVisible(true);
		header.setBackground(Color.black);
		header.setForeground(Color.black);
		header.setFont(new Font("Tahoma", Font.BOLD, 13));

		Alinear.setHorizontalAlignment(SwingConstants.CENTER);
		TablaTapas.getColumnModel().getColumn(0).setCellRenderer(Alinear);
		TablaTapas.getColumnModel().getColumn(1).setCellRenderer(Alinear);
		TablaTapas.getColumnModel().getColumn(2).setCellRenderer(Alinear);
		TablaTapas.setPreferredScrollableViewportSize(new Dimension(500, 70));
		TablaTapas.getSelectionModel().addListSelectionListener(this);
		TablaTapas.setFillsViewportHeight(true);
		TablaTapas.setRowSelectionAllowed(true);
		TTapas.fireTableDataChanged();

		PanelTapas = new JScrollPane();
		PanelTapas.setBounds(10, 45, 414, 190);
		PanelTapas.setViewportView(TablaTapas);
		getContentPane().add(PanelTapas);
		// TSeries.setData(objGestorIFCBS);

	}

	/**
	 * Escuchador
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case BORRAR_BUTTON:

			String dato = String.valueOf(TablaTapas.getValueAt(TablaTapas.getSelectedRow(), 0));
			int NTapa = Integer.parseInt(dato);
			if (PreguntarEntregado() == 0) {
				MandarABorrar(NTapa);
				ActualizarTabla();
			}
			break;

		case ACTUALIZAR_BUTTON:
			BotonBorrar.setEnabled(false);
			ActualizarTabla();
			break;

		default:
			break;
		}

	}

	/**
	 * Metodo para preguntar por la confirmacion de borrado
	 * 
	 * @return devolvemos confirm.
	 */
	private int PreguntarEntregado() {

		return JOptionPane.showConfirmDialog(null, "�Esta seguro de que desea eliminar el registro?", "BORRAR",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

	}

	/**
	 * Metodo para mandar a borrar
	 * 
	 * @param NSuela mandamos parametro
	 */
	private void MandarABorrar(int NTapa) {

		try {
			if (objGestorIFCBTa.EliminarTapasDeArray(NTapa)) {
				JOptionPane.showMessageDialog(null, "Registro eliminado correctamente");

			}
		} catch (SQLException e) {

			JOptionPane.showMessageDialog(null, "No se ha podido realizar el insert: " + e);
		} catch (ClsBorrarExcepcion e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}

	}

	/**
	 * Escuchador de tabla
	 */
	@Override
	public void valueChanged(ListSelectionEvent e) {

		int Seleccionado = TablaTapas.getSelectedRowCount();

		if (Seleccionado > 0) {
			BotonBorrar.setEnabled(true);
		}

	}

	/**
	 * Metodo para actualizar la tabla
	 */
	private void ActualizarTabla() {
		TablaTapas.setVisible(false);
		Tapas = objGestorIFCBTa.ObtenerTapas();
		ClsTablaTapas TablaActualizada = new ClsTablaTapas(Tapas);
		TablaTapas.setModel(TablaActualizada);
		Alinear.setHorizontalAlignment(SwingConstants.CENTER);
		TablaTapas.getColumnModel().getColumn(0).setCellRenderer(Alinear);
		TablaTapas.getColumnModel().getColumn(1).setCellRenderer(Alinear);
		TablaTapas.getColumnModel().getColumn(2).setCellRenderer(Alinear);
		TablaTapas.setVisible(true);
	}

}
