package pantalla_lp;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;

import pantalla_lp.ClsActualizarEntregas;
import pantalla_lp.ClsConsultarBorrarArticulos;
import pantalla_lp.ClsConsultarBorrarClientes;
import pantalla_lp.ClsConsultarBorrarDesgloses;
import pantalla_lp.ClsConsultarBorrarEnvios;
import pantalla_lp.ClsConsultarBorrarTiposHoja;
import pantalla_lp.ClsConsultarBorrarMateriales;
import pantalla_lp.ClsConsultarBorrarPedidos;
import pantalla_lp.ClsConsultarBorrarSeries;
import pantalla_lp.ClsConsultarBorrarTapas;
import pantalla_lp.ClsIFIntroducirArticulos;
import pantalla_lp.ClsIFIntroducirClientes;
import pantalla_lp.ClsIFIntroducirDesgloses;
import pantalla_lp.ClsIFIntroducirEnvios;
import pantalla_lp.ClsIFIntroducirTiposHoja;
import pantalla_lp.ClsIFIntroducirMateriales;
import pantalla_lp.ClsIFIntroducirPedidos;
import pantalla_lp.ClsIFIntroducirSeries;
import pantalla_lp.ClsIFIntroducirTapas;
import pantalla_lp.ClsPedidosMasDesglose;
import pantalla_lp.ClsPonerFotoPantalla;
import ln.ClsGestorLN;

/**
 * Pantalla con los distintos menus
 * 
 * @author Aitor Ubierna
 *
 */

public class ClsVerMenus extends JFrame implements ActionListener {

	/**
	 * Variable para guardar el gestor
	 */
	private ClsGestorLN objGestorMID;

	private static final long serialVersionUID = 1L;
	/**
	 * Constantes para el LISENER de insertar.
	 */
	private final String ARTICULOS_INSERTAR_BUTTON = "Boton de insertar articulos";
	private final String CLIENTES_INSERTAR_BUTTON = "Boton de insertar clientes";
	private final String DESGLOSE_INSERTAR_BUTTON = "Boton de insertar desglose";
	private final String ENVIOS_INSERTAR_BUTTON = "Boton de insertar envios";
	private final String TIPOHOJA_INSERTAR_BUTTON = "Boton de insertar tipos de hoja";
	private final String MATERIALES_INSERTAR_BUTTON = "Boton de insertar materiales";
	private final String PEDIDOS_INSERTAR_BUTTON = "Boton de insertar pedidos";
	private final String SERIES_INSERTAR_BUTTON = "Boton de insertar series";
	private final String TAPAS_INSERTAR_BUTTON = "Boton de insertar tapas";
	/**
	 * Constantes para el LISENER de borrar.
	 */
	private final String ARTICULOS_BORRAR_BUTTON = "Boton de borrar articulos";
	private final String CLIENTES_BORRAR_BUTTON = "Boton de borrar clientes";
	private final String DESGLOSE_BORRAR_BUTTON = "Boton de borrar desglose";
	private final String ENVIOS_BORRAR_BUTTON = "Boton de borrar envios";
	private final String TIPOHOJA_BORRAR_BUTTON = "Boton de borrar tiops de hoja";
	private final String MATERIALES_BORRAR_BUTTON = "Boton de borrar materiales";
	private final String PEDIDOS_BORRAR_BUTTON = "Boton de borrar pedidos";
	private final String SERIES_BORRAR_BUTTON = "Boton de borrar series";
	private final String TAPAS_BORRAR_BUTTON = "Boton de borrar tapas";
	private Random random = new Random();

	/**
	 * Constante para el LISENER de actualizar.
	 */
	private final String ENTREGAS_ACTUALIZAR_BUTTON = "Boton de actualizar entregas";

	/**
	 * Constante para el LISENER informes
	 */
	private final String INFORMES_PEDIDOS_MAS_DESGLOSES_BUTTON = "Informes pedidos mas desgloses";

	/**
	 * Objetos instanciados
	 */
	private ClsPonerFotoPantalla PanelMenuIntrducirDatos;
	private JMenuBar menuBar;
	private JMenu Actualizar, ConsultarBorrar, Introducir;
	private JMenuItem ArticulosIntroducir, ClientesIntroducir, DesgloseIntroducir, EnviosIntroducir, TapasIntroducir,
			MaterialesIntroducir, PedidosIntroducir, SeriesIntroducir, TiposIntroducir;
	private JMenuItem EntregasActualizar;
	private JMenuItem ArticulosBorrar, ClientesBorrar, DesgloseBorrar, EnviosBorrar, TapasBorrar, MaterialesBorrar,
			PedidosBorrar, SeriesBorrar, TiposBorrar;
	private ClsIFIntroducirClientes IntFrameClientes;
	private ClsIFIntroducirSeries IntFrameSeries;
	private ClsIFIntroducirTiposHoja IntFrameHojas;
	private ClsIFIntroducirTapas IntFrameTapas;
	private ClsIFIntroducirMateriales IntFrameMateriales;
	private ClsIFIntroducirEnvios IntFrameEnvios;
	private ClsIFIntroducirArticulos IntFrameArticulos;
	private ClsIFIntroducirPedidos IntFramePedidos;
	private ClsIFIntroducirDesgloses IntFrameDesgloses;
	private ClsPedidosMasDesglose IntFramePedidosMasDesgloses;
	private JMenuItem PedidosDesglose;
	private ClsConsultarBorrarSeries IntFrameConsultarBorrarSeries;
	private ClsConsultarBorrarTapas IntFrameConsultarBorrarTapas;
	private ClsConsultarBorrarArticulos IntFrameConsultarBorrarArticulos;
	private ClsConsultarBorrarPedidos IntFrameConsultarBorrarPedidos;
	private ClsConsultarBorrarClientes IntFrameConsultarBorrarClientes;
	private ClsConsultarBorrarEnvios IntFrameConsultarBorrarEnvios;
	private ClsConsultarBorrarMateriales IntFrameConsultarBorrarMateriales;
	private ClsConsultarBorrarDesgloses IntFrameConsultarBorrarDesgloses;
	private ClsConsultarBorrarTiposHoja IntFrameConsultarBorrarHojas;
	private ClsActualizarEntregas IntFrameActualizarEntregas;

	/**
	 * Launch the application.
	 * 
	 * @param args      recibe parametro
	 * @param ObjGestor recibe gestor
	 */
	public static void main(String[] args, ClsGestorLN ObjGestor) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClsVerMenus frame = new ClsVerMenus(ObjGestor);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Constructor
	 * 
	 * @param ObjGestor recibe el gestor
	 */
	public ClsVerMenus(ClsGestorLN ObjGestor) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ClsVerMenus.class.getResource("/PANTALLA_LP/DEUSTO.png")));
		objGestorMID = ObjGestor;
		IniciarComponentes(ObjGestor);

	}

	/**
	 * Crea los componentes
	 * 
	 * @param ObjGestor recibe el gestor
	 */
	private void IniciarComponentes(ClsGestorLN ObjGestor) {

		setTitle("Menu");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		/**
		 * Inicializamos panel
		 */

		String ruta = "./imagen/Satellite" + random.nextInt(3) + ".png";

		PanelMenuIntrducirDatos = new ClsPonerFotoPantalla(ruta);

		PanelMenuIntrducirDatos.setBackground(Color.WHITE);
		PanelMenuIntrducirDatos.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(PanelMenuIntrducirDatos);
		PanelMenuIntrducirDatos.setLayout(null);
		PanelMenuIntrducirDatos.getMaximumSize();

		menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 1366, 40);
		PanelMenuIntrducirDatos.add(menuBar);
		menuBar.setForeground(Color.BLUE);
		menuBar.setFont(new Font("Segoe UI", Font.PLAIN, 13));

		Actualizar = new JMenu("Actualizar");
		Actualizar.setFont(new Font("Segoe UI", Font.BOLD, 25));
		Actualizar.setForeground(Color.BLUE);
		menuBar.add(Actualizar);

		EntregasActualizar = new JMenuItem("Entregas");
		Actualizar.add(EntregasActualizar);
		EntregasActualizar.addActionListener(this);
		EntregasActualizar.setActionCommand(ENTREGAS_ACTUALIZAR_BUTTON);

		ConsultarBorrar = new JMenu("Consultar");
		ConsultarBorrar.setFont(new Font("Segoe UI", Font.BOLD, 25));
		ConsultarBorrar.setForeground(Color.BLUE);
		menuBar.add(ConsultarBorrar);

		ArticulosBorrar = new JMenuItem("Articulos");
		ConsultarBorrar.add(ArticulosBorrar);
		ArticulosBorrar.addActionListener(this);
		ArticulosBorrar.setActionCommand(ARTICULOS_BORRAR_BUTTON);

		JSeparator separator_8 = new JSeparator();
		ConsultarBorrar.add(separator_8);

		ClientesBorrar = new JMenuItem("Clientes");
		ConsultarBorrar.add(ClientesBorrar);
		ClientesBorrar.addActionListener(this);
		ClientesBorrar.setActionCommand(CLIENTES_BORRAR_BUTTON);

		JSeparator separator_9 = new JSeparator();
		ConsultarBorrar.add(separator_9);

		DesgloseBorrar = new JMenuItem("Desglose de Pedido");
		ConsultarBorrar.add(DesgloseBorrar);
		DesgloseBorrar.addActionListener(this);
		DesgloseBorrar.setActionCommand(DESGLOSE_BORRAR_BUTTON);

		JSeparator separator_10 = new JSeparator();
		ConsultarBorrar.add(separator_10);

		EnviosBorrar = new JMenuItem("Envios");
		ConsultarBorrar.add(EnviosBorrar);
		EnviosBorrar.addActionListener(this);
		EnviosBorrar.setActionCommand(ENVIOS_BORRAR_BUTTON);

		JSeparator separator_11 = new JSeparator();
		ConsultarBorrar.add(separator_11);

		TapasBorrar = new JMenuItem("Tapas");
		ConsultarBorrar.add(TapasBorrar);
		TapasBorrar.addActionListener(this);
		TapasBorrar.setActionCommand(TAPAS_BORRAR_BUTTON);

		JSeparator separator_12 = new JSeparator();
		ConsultarBorrar.add(separator_12);

		MaterialesBorrar = new JMenuItem("Materiales");
		ConsultarBorrar.add(MaterialesBorrar);
		MaterialesBorrar.addActionListener(this);
		MaterialesBorrar.setActionCommand(MATERIALES_BORRAR_BUTTON);

		JSeparator separator_13 = new JSeparator();
		ConsultarBorrar.add(separator_13);

		PedidosBorrar = new JMenuItem("Pedidos");
		ConsultarBorrar.add(PedidosBorrar);
		PedidosBorrar.addActionListener(this);
		PedidosBorrar.setActionCommand(PEDIDOS_BORRAR_BUTTON);

		JSeparator separator_14 = new JSeparator();
		ConsultarBorrar.add(separator_14);

		SeriesBorrar = new JMenuItem("Series");
		ConsultarBorrar.add(SeriesBorrar);
		SeriesBorrar.addActionListener(this);
		SeriesBorrar.setActionCommand(SERIES_BORRAR_BUTTON);

		JSeparator separator_15 = new JSeparator();
		ConsultarBorrar.add(separator_15);

		TiposBorrar = new JMenuItem("Tipos de Hoja");
		ConsultarBorrar.add(TiposBorrar);
		TiposBorrar.addActionListener(this);
		TiposBorrar.setActionCommand(TIPOHOJA_BORRAR_BUTTON);

		Introducir = new JMenu("Introducir ");
		Introducir.setForeground(Color.BLUE);
		Introducir.setFont(new Font("Segoe UI", Font.BOLD, 25));
		menuBar.add(Introducir);
		ArticulosIntroducir = new JMenuItem("Articulos");
		Introducir.add(ArticulosIntroducir);
		ArticulosIntroducir.addActionListener(this);
		ArticulosIntroducir.setActionCommand(ARTICULOS_INSERTAR_BUTTON);
		JSeparator separator = new JSeparator();
		Introducir.add(separator);
		ClientesIntroducir = new JMenuItem("Clientes");
		Introducir.add(ClientesIntroducir);
		ClientesIntroducir.addActionListener(this);
		ClientesIntroducir.setActionCommand(CLIENTES_INSERTAR_BUTTON);
		JSeparator separator_1 = new JSeparator();
		Introducir.add(separator_1);
		DesgloseIntroducir = new JMenuItem("Desglose de Pedido");
		Introducir.add(DesgloseIntroducir);
		DesgloseIntroducir.addActionListener(this);
		DesgloseIntroducir.setActionCommand(DESGLOSE_INSERTAR_BUTTON);
		JSeparator separator_2 = new JSeparator();
		Introducir.add(separator_2);
		EnviosIntroducir = new JMenuItem("Envios");
		Introducir.add(EnviosIntroducir);
		EnviosIntroducir.addActionListener(this);
		EnviosIntroducir.setActionCommand(ENVIOS_INSERTAR_BUTTON);
		JSeparator separator_3 = new JSeparator();
		Introducir.add(separator_3);
		TapasIntroducir = new JMenuItem("Tapas");
		Introducir.add(TapasIntroducir);
		TapasIntroducir.addActionListener(this);
		TapasIntroducir.setActionCommand(TAPAS_INSERTAR_BUTTON);
		JSeparator separator_4 = new JSeparator();
		Introducir.add(separator_4);
		MaterialesIntroducir = new JMenuItem("Materiales");
		Introducir.add(MaterialesIntroducir);
		MaterialesIntroducir.addActionListener(this);
		MaterialesIntroducir.setActionCommand(MATERIALES_INSERTAR_BUTTON);
		JSeparator separator_5 = new JSeparator();
		Introducir.add(separator_5);
		PedidosIntroducir = new JMenuItem("Pedidos");
		Introducir.add(PedidosIntroducir);
		PedidosIntroducir.addActionListener(this);
		PedidosIntroducir.setActionCommand(PEDIDOS_INSERTAR_BUTTON);
		JSeparator separator_6 = new JSeparator();
		Introducir.add(separator_6);
		SeriesIntroducir = new JMenuItem("Series");
		Introducir.add(SeriesIntroducir);
		SeriesIntroducir.addActionListener(this);
		SeriesIntroducir.setActionCommand(SERIES_INSERTAR_BUTTON);
		JSeparator separator_7 = new JSeparator();
		Introducir.add(separator_7);
		TiposIntroducir = new JMenuItem("Tipos de Hoja");
		Introducir.add(TiposIntroducir);
		TiposIntroducir.addActionListener(this);
		TiposIntroducir.setActionCommand(TIPOHOJA_INSERTAR_BUTTON);

		JMenu Informes = new JMenu("Informes");
		Informes.setForeground(Color.BLUE);
		Informes.setFont(new Font("Segoe UI", Font.BOLD, 25));
		menuBar.add(Informes);

		PedidosDesglose = new JMenuItem("Pedidos + Desgloses");
		Informes.add(PedidosDesglose);
		PedidosDesglose.addActionListener(this);
		PedidosDesglose.setActionCommand(INFORMES_PEDIDOS_MAS_DESGLOSES_BUTTON);
		PanelMenuIntrducirDatos.addComponentListener(null);

	}

	/**
	 * Escuchador
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

		switch (e.getActionCommand()) {
		/**
		 * si pulsa en introducir series
		 */
		case SERIES_INSERTAR_BUTTON:
			IntroducirSeries();
			break;
		/**
		 * Si pulsa en introducir Tapas
		 */
		case TAPAS_INSERTAR_BUTTON:
			IntroducirTapas();
			break;
		/**
		 * Si pulsa en introducir Materiales
		 */
		case MATERIALES_INSERTAR_BUTTON:
			IntroducirMateriales();
			break;
		/**
		 * Si pulsa en introducir Tipos de Hoja
		 */
		case TIPOHOJA_INSERTAR_BUTTON:
			IntroducirTipos();
			break;
		/**
		 * Si pulsa en introducir Clientes
		 */
		case CLIENTES_INSERTAR_BUTTON:
			IntroducirClientes();
			break;
		/**
		 * si pulsa en introducir envios
		 */
		case ENVIOS_INSERTAR_BUTTON:
			IntroducirEnvios();
			break;

		/**
		 * Si pulsa introducir Articulos
		 */
		case ARTICULOS_INSERTAR_BUTTON:
			IntroducirArticulos();
			break;

		/**
		 * Si pulsa introducir Pedidos
		 */
		case PEDIDOS_INSERTAR_BUTTON:
			IntroducirPedidos();
			break;

		/**
		 * Si pulsa introducir Desgloses.
		 */
		case DESGLOSE_INSERTAR_BUTTON:
			IntroducirDesgloses();
			break;
		/**
		 * Si pulsa informes pedidos + desgloses.
		 */
		case INFORMES_PEDIDOS_MAS_DESGLOSES_BUTTON:
			InformesPedidosDesgloses();
			break;

		/**
		 * Si pulsa consultar series.
		 */
		case SERIES_BORRAR_BUTTON:
			ConsultarBorrarSeries();
			break;

		/**
		 * Si pulsa consultar tipos de hoja.
		 */
		case TIPOHOJA_BORRAR_BUTTON:
			ConsultarBorrarTipos();
			break;

		/**
		 * Si pulsa consultar articulos.
		 */
		case ARTICULOS_BORRAR_BUTTON:
			ConsultarBorrarArticulos();
			break;

		/**
		 * Si pulsa consultar pedidos.
		 */
		case PEDIDOS_BORRAR_BUTTON:
			ConsultarBorrarPedidos();
			break;

		/**
		 * Si pulsa consultar clientes.
		 */
		case CLIENTES_BORRAR_BUTTON:
			ConsultarBorrarClientes();
			break;

		/**
		 * Si pulsa consultar desgloses.
		 */
		case DESGLOSE_BORRAR_BUTTON:
			ConsultarBorrarDesgloses();
			break;

		/**
		 * Si pulsa consultar tapas.
		 */
		case TAPAS_BORRAR_BUTTON:
			ConsultarBorrarTapas();
			break;

		/**
		 * Si pulsa consultar materiales.
		 */
		case MATERIALES_BORRAR_BUTTON:
			ConsultarBorrarMateriales();
			break;

		/**
		 * Si pulsa consultar envios.
		 */
		case ENVIOS_BORRAR_BUTTON:
			ConsultarBorrarEnvios();
			break;

		/**
		 * Si pulsa consultar envios.
		 */
		case ENTREGAS_ACTUALIZAR_BUTTON:
			ActualizarEntregas();
			break;

		default:
			break;
		}

	}

	/**
	 * Llamada a InternalFrame para actualizar entregas
	 */
	private void ActualizarEntregas() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaActualizarEntregasAbierta()) {
			/**
			 * Creamos objeto IFrameSeries
			 */
			IntFrameActualizarEntregas = new ClsActualizarEntregas(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameActualizarEntregas);
		}
		/**
		 * Lo hacemos visible
		 */
		IntFrameActualizarEntregas.setVisible(true);
		try {
			IntFrameActualizarEntregas.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/**
	 * Llamada a InternalFrame para consultar y borrar envios
	 */
	private void ConsultarBorrarEnvios() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaConsultarBorrarEnviosAbierta()) {
			/**
			 * Creamos objeto IFrameSeries
			 */
			IntFrameConsultarBorrarEnvios = new ClsConsultarBorrarEnvios(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameConsultarBorrarEnvios);
		}
		/**
		 * Lo hacemos visible
		 */
		IntFrameConsultarBorrarEnvios.setVisible(true);
		try {
			IntFrameConsultarBorrarEnvios.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/**
	 * Llamada a InternalFrame para consultar y borrar materiales
	 */
	private void ConsultarBorrarMateriales() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaConsultarBorrarMaterialesAbierta()) {
			/**
			 * Creamos objeto IFrameSeries
			 */
			IntFrameConsultarBorrarMateriales = new ClsConsultarBorrarMateriales(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameConsultarBorrarMateriales);
		}
		/**
		 * Lo hacemos visible
		 */
		IntFrameConsultarBorrarMateriales.setVisible(true);
		try {
			IntFrameConsultarBorrarMateriales.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/**
	 * Llamada a InternalFrame para consultar y borrar herrajes
	 */
	private void ConsultarBorrarTapas() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaConsultarBorrarTapasAbierta()) {
			/**
			 * Creamos objeto IFrameSeries
			 */
			IntFrameConsultarBorrarTapas = new ClsConsultarBorrarTapas(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameConsultarBorrarTapas);
		}
		/**
		 * Lo hacemos visible
		 */
		IntFrameConsultarBorrarTapas.setVisible(true);
		try {
			IntFrameConsultarBorrarTapas.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/**
	 * Llamada a InternalFrame para consultar y borrar desgloses
	 */
	private void ConsultarBorrarDesgloses() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaConsultarBorrarDesglosesAbierta()) {
			/**
			 * Creamos objeto IFrameSeries
			 */
			IntFrameConsultarBorrarDesgloses = new ClsConsultarBorrarDesgloses(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameConsultarBorrarDesgloses);
		}
		/**
		 * Lo hacemos visible
		 */
		IntFrameConsultarBorrarDesgloses.setVisible(true);
		try {
			IntFrameConsultarBorrarDesgloses.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/**
	 * Llamada a InternalFrame para consultar y borrar clientes
	 */
	private void ConsultarBorrarClientes() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaConsultarBorrarClientesAbierta()) {
			/**
			 * Creamos objeto IFrameSeries
			 */
			IntFrameConsultarBorrarClientes = new ClsConsultarBorrarClientes(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameConsultarBorrarClientes);
		}
		/**
		 * Lo hacemos visible
		 */
		IntFrameConsultarBorrarClientes.setVisible(true);
		try {
			IntFrameConsultarBorrarClientes.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/**
	 * Llamada a InternalFrame para consultar y borrar pedios
	 */
	private void ConsultarBorrarPedidos() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaConsultarBorrarPedidosAbierta()) {
			/**
			 * Creamos objeto IFrameSeries
			 */
			IntFrameConsultarBorrarPedidos = new ClsConsultarBorrarPedidos(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameConsultarBorrarPedidos);
		}
		/**
		 * Lo hacemos visible
		 */
		IntFrameConsultarBorrarPedidos.setVisible(true);
		try {
			IntFrameConsultarBorrarPedidos.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/**
	 * Llamada a InternalFrame para consultar y borrar articulos
	 */
	private void ConsultarBorrarArticulos() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaConsultarBorrarArticulosAbierta()) {
			/**
			 * Creamos objeto IFrameSeries
			 */
			IntFrameConsultarBorrarArticulos = new ClsConsultarBorrarArticulos(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameConsultarBorrarArticulos);
		}
		/**
		 * Lo hacemos visible
		 */
		IntFrameConsultarBorrarArticulos.setVisible(true);
		try {
			IntFrameConsultarBorrarArticulos.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/**
	 * Llamada a InternalFrame para consultar y borrar suelas
	 */
	private void ConsultarBorrarTipos() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaConsultarBorrarTiposAbierta()) {
			/**
			 * Creamos objeto IFrameSeries
			 */
			IntFrameConsultarBorrarHojas = new ClsConsultarBorrarTiposHoja(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameConsultarBorrarHojas);
		}
		/**
		 * Lo hacemos visible
		 */
		IntFrameConsultarBorrarHojas.setVisible(true);
		try {
			IntFrameConsultarBorrarHojas.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/**
	 * Llamada a InternalFrame para consultar y borrar series
	 */
	private void ConsultarBorrarSeries() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaConsultarBorrarSeriesAbierta()) {
			/**
			 * Creamos objeto IFrameSeries
			 */
			IntFrameConsultarBorrarSeries = new ClsConsultarBorrarSeries(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameConsultarBorrarSeries);
		}
		/**
		 * Lo hacemos visible
		 */
		IntFrameConsultarBorrarSeries.setVisible(true);
		try {
			IntFrameConsultarBorrarSeries.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/**
	 * Llamada a InternalFrame para informes pedidos mas desgloses
	 */
	private void InformesPedidosDesgloses() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaPedidosDesglosesAbierta()) {
			/**
			 * Creamos objeto IFrameSeries
			 */
			IntFramePedidosMasDesgloses = new ClsPedidosMasDesglose(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFramePedidosMasDesgloses);
		}
		/**
		 * Lo hacemos visible
		 */
		IntFramePedidosMasDesgloses.setVisible(true);
		try {
			IntFramePedidosMasDesgloses.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/**
	 * Llamada a InternalFrame para introducir Desgloses
	 */
	private void IntroducirDesgloses() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaDesglosesAbierta()) {
			/**
			 * Creamos objeto IFrameSeries
			 */
			IntFrameDesgloses = new ClsIFIntroducirDesgloses(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameDesgloses);
		}
		/**
		 * Lo hacemos visible
		 */
		IntFrameDesgloses.setVisible(true);
		try {
			IntFrameDesgloses.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/**
	 * Llamada al InternalFrame Para introducir Series
	 */
	private void IntroducirSeries() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaSeriesAbierta()) {
			/**
			 * Creamos objeto IFrameSeries
			 */
			IntFrameSeries = new ClsIFIntroducirSeries(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameSeries);
		}
		/**
		 * Lo hacemos visible
		 */
		IntFrameSeries.setVisible(true);
		try {
			IntFrameSeries.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/**
	 * Llamada al InternalFrame para introducir Tapas
	 */
	private void IntroducirTapas() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaTapasAbierta()) {
			/**
			 * Creamos Objeto IFrameHerrajes
			 */
			IntFrameTapas = new ClsIFIntroducirTapas(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameTapas);
		}

		/**
		 * Lo hacemos visible
		 */
		IntFrameTapas.setVisible(true);
		IntFrameTapas.moveToFront();
		try {
			IntFrameTapas.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}

	}

	/**
	 * Llamada al InternalFrame para introducir Materiales
	 */
	private void IntroducirMateriales() {
		/**
		 * Comprobamos si esta abierta la ventana
		 */
		if (!ComprobarVentanaMaterialesAbierta()) {
			/**
			 * Creamos el objeto
			 */
			IntFrameMateriales = new ClsIFIntroducirMateriales(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameMateriales);
		}

		/**
		 * Lo hacemos visible
		 */
		IntFrameMateriales.setVisible(true);
		try {
			IntFrameMateriales.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}

	}

	/**
	 * Llamada al InternalFrame para introducir Tapas
	 */
	private void IntroducirTipos() {
		/**
		 * Comprobamos que no este ya abierto
		 */
		if (!ComprobarVentanaTiposAbierta()) {
			/**
			 * Creamos el objeto
			 */
			IntFrameHojas = new ClsIFIntroducirTiposHoja(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameHojas);
		}
		/**
		 * Lo hacemos visible
		 */
		IntFrameHojas.setVisible(true);
		try {
			IntFrameHojas.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}

	}

	/**
	 * Llamada al InternalFrame para introducir Clientes
	 */
	private void IntroducirClientes() {
		/**
		 * Comprobamos que la ventana no este abierta.
		 */
		if (!ComprobarVentanaClientesAbierta()) {
			/**
			 * creamos el objeto
			 */
			IntFrameClientes = new ClsIFIntroducirClientes(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameClientes);
		}

		/**
		 * Lo hacemos visible
		 */
		IntFrameClientes.setVisible(true);
		try {
			IntFrameClientes.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}

	}

	/**
	 * Metodo para acceder al InternalFrame de introducir Envios
	 */
	private void IntroducirEnvios() {
		/**
		 * Comprobamos que la ventana no este abierta.
		 */
		if (!ComprobarVentanaEnviosAbierta()) {
			/**
			 * creamos el objeto
			 */
			IntFrameEnvios = new ClsIFIntroducirEnvios(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameEnvios);
		}

		/**
		 * Lo hacemos visible
		 */
		IntFrameEnvios.setVisible(true);
		try {
			IntFrameEnvios.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}

	}

	/**
	 * Metodo para ir al InternalFrame introducir Articulos
	 */
	private void IntroducirArticulos() {
		/**
		 * Comprobamos que la ventana no este abierta.
		 */
		if (!ComprobarVentanaArticulosAbierta()) {
			/**
			 * creamos el objeto
			 */
			IntFrameArticulos = new ClsIFIntroducirArticulos(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFrameArticulos);
		}

		/**
		 * Lo hacemos visible
		 */
		IntFrameArticulos.setVisible(true);
		try {
			IntFrameArticulos.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}

	}

	/**
	 * Metodo para ir al InternalFrame introducir Pedidos
	 */
	private void IntroducirPedidos() {
		/**
		 * Comprobamos que la ventana no este abierta.
		 */
		if (!ComprobarVentanaPedidosAbierta()) {
			/**
			 * creamos el objeto
			 */
			IntFramePedidos = new ClsIFIntroducirPedidos(objGestorMID);
			PanelMenuIntrducirDatos.add(IntFramePedidos);
		}

		/**
		 * Lo hacemos visible
		 */
		IntFramePedidos.setVisible(true);
		try {
			IntFramePedidos.setSelected(true);
		} catch (PropertyVetoException e) {
			JOptionPane.showMessageDialog(null, e);
		}

	}

	/**
	 * Metodo para comprobar que la ventana esta abierta o no
	 * 
	 * @return nos dice si esta abierta o no
	 */
	private boolean ComprobarVentanaPedidosDesglosesAbierta() {

		boolean mostrar = false;

		boolean cerrado;
		if (IntFramePedidosMasDesgloses == null || IntFramePedidosMasDesgloses.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFramePedidosMasDesgloses.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFramePedidosMasDesgloses.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFramePedidosMasDesgloses);

			mostrar = true;

		}

		return mostrar;
	}

	/**
	 * Metodo para detectar si InternalFrame Series esta abierto
	 * 
	 * @return nos dice si esta o no
	 */
	private boolean ComprobarVentanaSeriesAbierta() {

		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameSeries == null || IntFrameSeries.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameSeries.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameSeries.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameSeries);

			mostrar = true;

		}
		return mostrar;

	}

	/**
	 * Metodo para detectar si InternalFrame Suelas esta abierto
	 * 
	 * @return nos dice si esta o no
	 */
	private boolean ComprobarVentanaTiposAbierta() {
		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameHojas == null || IntFrameHojas.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameHojas.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameHojas.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameHojas);

			mostrar = true;

		}
		return mostrar;

	}

	/**
	 * Metodo para detectar si InternalFrame Materiales esta abierto
	 * 
	 * @return nos dice si esta o no
	 */
	private boolean ComprobarVentanaMaterialesAbierta() {

		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameMateriales == null || IntFrameMateriales.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameMateriales.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameMateriales.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameMateriales);

			mostrar = true;

		}
		return mostrar;

	}

	/**
	 * Metodo para detectar si InternalFrame Clientes esta abierto
	 * 
	 * @return nos dice si esta o no
	 */
	private boolean ComprobarVentanaClientesAbierta() {

		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameClientes == null || IntFrameClientes.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameClientes.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameClientes.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameClientes);

			mostrar = true;

		}
		return mostrar;

	}

	/**
	 * Metodo para comprobar si la ventana envios esta abierta
	 * 
	 * @return nos dice si esta o no
	 */
	private boolean ComprobarVentanaEnviosAbierta() {

		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameEnvios == null || IntFrameEnvios.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameEnvios.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameEnvios.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameEnvios);

			mostrar = true;

		}

		return mostrar;
	}

	/**
	 * Metodo para comprobar si la ventana articulos esta abierta
	 * 
	 * @return nos devuelve el resultado
	 */
	private boolean ComprobarVentanaArticulosAbierta() {

		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameArticulos == null || IntFrameArticulos.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameArticulos.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameArticulos.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameArticulos);

			mostrar = true;

		}

		return mostrar;
	}

	/**
	 * Metodo para comprobar si la ventana pedidos esta abierta
	 * 
	 * @return nos devuelve el resultado
	 */
	private boolean ComprobarVentanaPedidosAbierta() {

		boolean mostrar = false;

		boolean cerrado;
		if (IntFramePedidos == null || IntFramePedidos.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFramePedidos.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFramePedidos.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFramePedidos);

			mostrar = true;

		}

		return mostrar;
	}

	/**
	 * Metodo para comprobar si la ventana desgloses esta abierta
	 * 
	 * @return nos devuelve el resultado
	 */
	private boolean ComprobarVentanaDesglosesAbierta() {

		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameDesgloses == null || IntFrameDesgloses.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameDesgloses.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameDesgloses.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameDesgloses);

			mostrar = true;

		}

		return mostrar;
	}

	/**
	 * Metodo para comprobar que la ventana esta abierta o no
	 * 
	 * @return nos dice si esta abierta o no
	 */
	private boolean ComprobarVentanaConsultarBorrarSeriesAbierta() {

		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameConsultarBorrarSeries == null || IntFrameConsultarBorrarSeries.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameConsultarBorrarSeries.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameConsultarBorrarSeries.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameConsultarBorrarSeries);

			mostrar = true;

		}

		return mostrar;
	}

	/**
	 * Metodo para comprobar que la ventana esta abierta o no
	 * 
	 * @return nos dice si esta abierta o no
	 */
	private boolean ComprobarVentanaConsultarBorrarTiposAbierta() {

		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameConsultarBorrarHojas == null || IntFrameConsultarBorrarHojas.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameConsultarBorrarHojas.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameConsultarBorrarHojas.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameConsultarBorrarHojas);

			mostrar = true;

		}

		return mostrar;
	}

	/**
	 * Metodo para comprobar que la ventana esta abierta o no
	 * 
	 * @return nos dice si esta abierta o no
	 */
	private boolean ComprobarVentanaTapasAbierta() {

		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameTapas == null || IntFrameTapas.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameTapas.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameTapas.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameTapas);

			mostrar = true;

		}

		return mostrar;
	}

	/**
	 * Metodo para comprobar que la ventana esta abierta o no
	 * 
	 * @return nos dice si esta abierta o no
	 */
	private boolean ComprobarVentanaConsultarBorrarArticulosAbierta() {
		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameConsultarBorrarArticulos == null || IntFrameConsultarBorrarArticulos.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameConsultarBorrarArticulos.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameConsultarBorrarArticulos.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameConsultarBorrarArticulos);

			mostrar = true;

		}

		return mostrar;

	}

	/**
	 * Metodo para comprobar que la ventana esta abierta o no
	 * 
	 * @return nos dice si esta abierta o no
	 */
	private boolean ComprobarVentanaConsultarBorrarPedidosAbierta() {

		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameConsultarBorrarPedidos == null || IntFrameConsultarBorrarPedidos.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameConsultarBorrarPedidos.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameConsultarBorrarPedidos.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameConsultarBorrarPedidos);

			mostrar = true;

		}

		return mostrar;
	}

	/**
	 * Metodo para comprobar que la ventana esta abierta o no
	 * 
	 * @return nos dice si esta abierta o no
	 */
	private boolean ComprobarVentanaConsultarBorrarClientesAbierta() {

		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameConsultarBorrarClientes == null || IntFrameConsultarBorrarClientes.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameConsultarBorrarClientes.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameConsultarBorrarClientes.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameConsultarBorrarClientes);

			mostrar = true;

		}

		return mostrar;
	}

	/**
	 * Metodo para comprobar que la ventana esta abierta o no
	 * 
	 * @return nos dice si esta abierta o no
	 */
	private boolean ComprobarVentanaConsultarBorrarEnviosAbierta() {
		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameConsultarBorrarEnvios == null || IntFrameConsultarBorrarEnvios.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameConsultarBorrarEnvios.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameConsultarBorrarEnvios.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameConsultarBorrarEnvios);

			mostrar = true;

		}

		return mostrar;
	}

	/**
	 * Metodo para comprobar que la ventana esta abierta o no
	 * 
	 * @return nos dice si esta abierta o no
	 */
	private boolean ComprobarVentanaConsultarBorrarMaterialesAbierta() {
		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameConsultarBorrarMateriales == null || IntFrameConsultarBorrarMateriales.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameConsultarBorrarMateriales.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameConsultarBorrarMateriales.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameConsultarBorrarMateriales);

			mostrar = true;

		}

		return mostrar;

	}

	/**
	 * Metodo para comprobar que la ventana esta abierta o no
	 * 
	 * @return nos dice si esta abierta o no
	 */
	private boolean ComprobarVentanaConsultarBorrarTapasAbierta() {
		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameConsultarBorrarTapas == null || IntFrameConsultarBorrarTapas.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameConsultarBorrarTapas.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameConsultarBorrarTapas.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameConsultarBorrarTapas);

			mostrar = true;

		}

		return mostrar;
	}

	/**
	 * Metodo para comprobar que la ventana esta abierta o no
	 * 
	 * @return nos dice si esta abierta o no
	 */
	private boolean ComprobarVentanaConsultarBorrarDesglosesAbierta() {
		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameConsultarBorrarDesgloses == null || IntFrameConsultarBorrarDesgloses.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameConsultarBorrarDesgloses.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameConsultarBorrarDesgloses.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameConsultarBorrarDesgloses);

			mostrar = true;

		}

		return mostrar;
	}

	/**
	 * Metodo para comprobar que la ventana esta abierta o no
	 * 
	 * @return nos dice si esta abierta o no
	 */
	private boolean ComprobarVentanaActualizarEntregasAbierta() {

		boolean mostrar = false;

		boolean cerrado;
		if (IntFrameActualizarEntregas == null || IntFrameActualizarEntregas.isClosed()) {
			cerrado = true;
		} else {
			cerrado = false;
		}

		if (!cerrado) {
			String Nombre = IntFrameActualizarEntregas.getTitle();

			JOptionPane.showMessageDialog(rootPane, "La ventana que intenta abrir ya est� abierta", Nombre,
					JOptionPane.WARNING_MESSAGE);

			IntFrameActualizarEntregas.toFront();

			PanelMenuIntrducirDatos.moveToFront(IntFrameActualizarEntregas);

			mostrar = true;

		}

		return mostrar;

	}
}
