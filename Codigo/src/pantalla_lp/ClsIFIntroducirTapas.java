package pantalla_lp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JInternalFrame;

import ln.ClsGestorLN;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import static comun.clsConstantes.PROPIEDAD_TAPAS_REFERENCIA;

import java.awt.Font;
import javax.swing.SwingConstants;

import comun.ItfProperty;
import javax.swing.ImageIcon;

/**
 * Internar Frame para introducir tapas
 * 
 * @author Aitor Ubierna
 */
public class ClsIFIntroducirTapas extends JInternalFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	/**
	 * Objetos
	 */
	private JTextField CasillaNTapa;
	private JTextField CasillaDesTapa;
	private JTextField CasillaPrecioTapa;
	private JButton NTapaAuto, ConfirmarTapa;
	private int Ref;
	private Double Prec;
	/**
	 * Para tener el Gestor
	 */
	private ClsGestorLN objGestorIFIM;
	/**
	 * constantes para el ActionLisener
	 */
	private final String CONFIRMAR_BUTTON = "Boton de confirmar Tapas";
	private final String AUTOMATICO_BUTTON = "Boton para poner automatico el ID";

	/**
	 * Creamos el frame
	 * 
	 * @param ObjGestor recibimos el gestor
	 */
	public ClsIFIntroducirTapas(ClsGestorLN ObjGestor) {
		setFrameIcon(new ImageIcon(ClsIFIntroducirTapas.class.getResource("/PANTALLA_LP/DEUSTO.png")));
		setResizable(false);
		setMaximizable(true);
		setIconifiable(true);
		setClosable(true);
		setTitle("Introducir Tapas");
		this.setBounds(75, 75, 600, 300);
		getContentPane().setLayout(new GridLayout(4, 3));

		Inicializar(ObjGestor);
	}

	/**
	 * Creamos lo objetos del frame
	 * 
	 * @param ObjGestor recibimos el gestor
	 */
	private void Inicializar(ClsGestorLN ObjGestor) {
		JLabel TxtNSuelas = new JLabel("Numero de Tapa:");
		TxtNSuelas.setHorizontalAlignment(SwingConstants.CENTER);
		TxtNSuelas.setFont(new Font("Tahoma", Font.BOLD, 15));
		TxtNSuelas.setEnabled(false);
		getContentPane().add(TxtNSuelas);

		CasillaNTapa = new JTextField();
		CasillaNTapa.setFont(new Font("Tahoma", Font.BOLD, 15));
		CasillaNTapa.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(CasillaNTapa);
		CasillaNTapa.setColumns(10);

		NTapaAuto = new JButton("N\u00BA Tapa Automatico");
		NTapaAuto.setFont(new Font("Tahoma", Font.PLAIN, 15));
		getContentPane().add(NTapaAuto);
		NTapaAuto.addActionListener(this);
		NTapaAuto.setActionCommand(AUTOMATICO_BUTTON);

		JLabel TextDesTapa = new JLabel("Descripcion:");
		TextDesTapa.setHorizontalAlignment(SwingConstants.CENTER);
		TextDesTapa.setFont(new Font("Tahoma", Font.BOLD, 15));
		TextDesTapa.setEnabled(false);
		getContentPane().add(TextDesTapa);

		CasillaDesTapa = new JTextField();
		CasillaDesTapa.setFont(new Font("Tahoma", Font.BOLD, 15));
		CasillaDesTapa.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(CasillaDesTapa);
		CasillaDesTapa.setColumns(10);

		JLabel Vacio1 = new JLabel("");
		Vacio1.setEnabled(false);
		getContentPane().add(Vacio1);

		JLabel TxtPrecioSuela = new JLabel("Precio:");
		TxtPrecioSuela.setFont(new Font("Tahoma", Font.BOLD, 15));
		TxtPrecioSuela.setEnabled(false);
		TxtPrecioSuela.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(TxtPrecioSuela);

		CasillaPrecioTapa = new JTextField();
		CasillaPrecioTapa.setHorizontalAlignment(SwingConstants.CENTER);
		CasillaPrecioTapa.setFont(new Font("Tahoma", Font.BOLD, 15));
		getContentPane().add(CasillaPrecioTapa);
		CasillaPrecioTapa.setColumns(10);

		JLabel Vacio2 = new JLabel("");
		Vacio2.setEnabled(false);
		getContentPane().add(Vacio2);

		JLabel Vacio3 = new JLabel("");
		Vacio3.setEnabled(false);
		getContentPane().add(Vacio3);

		JLabel Vacio4 = new JLabel("");
		getContentPane().add(Vacio4);

		ConfirmarTapa = new JButton("Confirmar Tapa");
		ConfirmarTapa.setFont(new Font("Tahoma", Font.PLAIN, 15));
		ConfirmarTapa.addActionListener(this);
		ConfirmarTapa.setActionCommand(CONFIRMAR_BUTTON);
		getContentPane().add(ConfirmarTapa);

		objGestorIFIM = ObjGestor;

	}

	/**
	 * Escuchador
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case AUTOMATICO_BUTTON:
			ObtenerUltimoID();
			break;

		case CONFIRMAR_BUTTON:
			Comprobar();
			break;

		default:
			break;
		}

	}

	/**
	 * Metodo para comprobar que los valores introducidos son adecuados
	 */
	private void Comprobar() {

		boolean comprobado = true;

		try {
			Ref = Integer.parseInt(CasillaNTapa.getText());
		} catch (Exception e) {
			comprobado = false;
			JOptionPane.showMessageDialog(null, "Numero de Suela incorrecto o vacio");
		}

		if (!CasillaPrecioTapa.getText().equals("")) {
			try {
				Prec = Double.parseDouble(CasillaPrecioTapa.getText());
			} catch (Exception e) {
				comprobado = false;
				JOptionPane.showMessageDialog(null, "El precio no puede ser texto");
			}
		}
		if (CasillaDesTapa.getText().equals("")) {
			comprobado = false;
			JOptionPane.showMessageDialog(null, "La descripción no puede estar vacia");
		}

		if (comprobado) {
			MandarAGestor();
			PonerVacio();
		}
	}

	/**
	 * Metodo para dejar vacios los huecos
	 */
	private void PonerVacio() {

		CasillaNTapa.setText("");
		CasillaDesTapa.setText("");
		CasillaPrecioTapa.setText("");

	}

	/**
	 * Mandamos datos al gestor
	 */
	private void MandarAGestor() {

		String Desc = CasillaDesTapa.getText();

		try {
			if (objGestorIFIM.CrearTapa(Ref, Desc, Prec)) {
				JOptionPane.showMessageDialog(null, "Suela introducida correctamente");
			} else {
				JOptionPane.showMessageDialog(null, "ID de la Suela repetido!");
			}

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "No se ha podido realizar el insert: " + e);
		}

	}

	/**
	 * Metodo para obtener automaticamente el siguiente ID.
	 */
	private void ObtenerUltimoID() {

		ArrayList<ItfProperty> Tapas = objGestorIFIM.OrdenarTapas();

		ItfProperty UltimoObjeto = Tapas.get(Tapas.size() - 1);

		int mostrar = UltimoObjeto.getIntegerProperty(PROPIEDAD_TAPAS_REFERENCIA);

		int IDActualizadoINT = mostrar + 1;

		String IDActualizado = Integer.toString(IDActualizadoINT);

		CasillaNTapa.setText(IDActualizado);

	}

}
