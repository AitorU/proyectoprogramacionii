package comun;
/**
 * Interfaz que implementan aquellos objetos que representen una fila de un base de datos.
 * La teórica fila estaría compuesta de columnas y valores para esas respectivas columnas.
 * 
 * @author javier.cerro
 *
 */

public interface itfData {
	
	Object getData (String columna);

}
