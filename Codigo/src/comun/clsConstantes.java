package comun;

/**
 * Clase para generar Propiedades
 * 
 * @author Aitor Ubierna
 *
 */

public class clsConstantes {

	/**
	 * Aqui empiezan las de TAPAS
	 */
	/**
	 * propiedad REFERENCIA de la clase Tapas
	 */
	public static final String PROPIEDAD_TAPAS_REFERENCIA = "Referencia";
	/**
	 * propiedad DESCRIPCION de la clase Tapas
	 */
	public static final String PROPIEDAD_TAPAS_DESCRIPCION = "Descripcion";
	/**
	 * propiedad PRECIO de la clase Tapas
	 */
	public static final String PROPIEDAD_TAPAS_PRECIO = "Precio";

	/**
	 * Aqui empiezan las de SERIES
	 */
	/**
	 * Propiedad NUMERO_DE_SERIE de la clase Series.
	 */
	public static final String PROPIEDAD_SERIES_NUMERO_DE_SERIE = "Numero De Serie";
	/**
	 * Propiedad DESCRIPCION de la clase Series.
	 */
	public static final String PROPIEDAD_SERIES_DESCRIPCION = "Descripcion";

	/**
	 * Aqui empiezan las de PEDIDOS
	 */
	/**
	 * Propiedad NUMERO_DE_PEDIDO de la clase Pedidos.
	 */
	public static final String PROPIEDAD_PEDIDOS_NUMERO_DE_PEDIDO = "Numero de pedido";
	/**
	 * Propiedad FECHA_DE_PEDIDO de la clase Pedidos.
	 */
	public static final String PROPIEDAD_PEDIDOS_FECHA_DE_PEDIDO = "Fecha de pedido";
	/**
	 * Propiedad FECHA_DE_ENTREGA de la clase Pedidos.
	 */
	public static final String PROPIEDAD_PEDIDOS_FECHA_DE_ENTREGA = "Fecha de entrega";
	/**
	 * Propiedad ENTREGADO de la clase Pedidos.
	 */
	public static final String PROPIEDAD_PEDIDOS_ENTREGADO = "Entregado";
	/**
	 * Propiedad NUMERO_DE_CLIENTE de la clase Pedidos.
	 */
	public static final String PROPIEDAD_PEDIDOS_NUMERO_DE_CLIENTE_PEDIDO = "Numero de cliente pedido";
	/**
	 * Propiedad NOMBRE_Y_APELLIDOS_DEL_CLIENTE de la clase Pedidos.
	 */
	public static final String PROPIEDAD_PEDIDOS_NOMBRE_Y_APELLIDOS_DEL_CLIENTE = "Nombre y apellidos del cliente";

	/**
	 * Aqui empiezan las de MATERIALES
	 */
	/**
	 * Porpiedad REFERENCIA de la clase Materiales.
	 */
	public static final String PROPIEDAD_MATERIALES_REFERENCIA = "Referencia";
	/**
	 * Porpiedad DESCRIPCION de la clase Materiales.
	 */
	public static final String PROPIEDAD_MATERIALES_DESCRIPCION = "Descripcion";
	/**
	 * Porpiedad PRECIO de la clase Materiales.
	 */
	public static final String PROPIEDAD_MATERIALES_PRECIO = "Precio";

	/**
	 * Aqui empiezan los TIPOS DE HOJAS
	 */
	/**
	 * Propiedad REFERENCIA de la clase Tipos de Hojas.
	 */
	public static final String PROPIEDAD_TIPOHOJA_REFERENCIA = "Referencia";
	/**
	 * Propiedad DESCRIPCION de la clase Tipos de Hojas.
	 */
	public static final String PROPIEDAD_TIPOHOJA_DESCRIPCION = "Descripcion";
	/**
	 * Propiedad PRECIO de la clase Tipos de Hojas.
	 */
	public static final String PROPIEDAD_TIPOHOJA_PRECIO = "Precio";

	/**
	 * Aqui empiezan las de ENVIOS
	 */
	/**
	 * Propiedad NUMERO_DE_ENVIO de la clase Envios.
	 */
	public static final String PROPIEDAD_ENVIOS_NUMERO_DE_ENVIO = "Numero de envio";
	/**
	 * Propiedad NOMBRE_CLIENTE de la clase Envios.
	 */
	public static final String PROPIEDAD_ENVIOS_NOMBRE_CLIENTE = "Nombre del cliente";
	/**
	 * Propiedad DIRECCION_DE_ENVIO de la clase Envios.
	 */
	public static final String PROPIEDAD_ENVIOS_DIRECCION_DE_ENVIO = "Direccion de envio";
	/**
	 * Propiedad POBLACION_DE_ENVIO de la clase Envios.
	 */
	public static final String PROPIEDAD_ENVIOS_POBLACION_DE_ENVIO = "Poblacion de envio";
	/**
	 * Propiedad CPD_DE_ENVIO de la clase Envios.
	 */
	public static final String PROPIEDAD_ENVIOS_CPD_DE_ENVIO = "Codigo postal de envio";
	/**
	 * Propiedad PROVINCIA_DE_ENVIO de la clase Envios.
	 */
	public static final String PROPIEDAD_ENVIOS_PROVINCIA_DE_ENVIO = " Provinvia de envio";
	/**
	 * Propiedad TELEFONO_DE_ENVIO de la clase Envios.
	 */
	public static final String PROPIEDAD_ENVIOS_TELEFONO_DE_ENVIO = "Telefono de envio";
	/**
	 * Propiedad NUMERO_DE_CLIENTE de la clase Envios.
	 */
	public static final String PROPIEDAD_ENVIOS_NUMERO_DE_CLIENTE_ENVIO = "Numero de cliente envio";

	/**
	 * Aqui empiezan las de DESGLOSE
	 */
	/**
	 * Propiedad NUMERO_DE_DESGLOSE de la clase Desglose de Pedido.
	 */
	public static final String PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_DESGLOSE = "Numero de desglose";
	/**
	 * Propiedad REFERENCIA_DEL_ARTICULO de la clase Desglose de Pedido.
	 */
	public static final String PROPIEDAD_DESGLOSE_DE_PEDIDO_REFERENCIA_DEL_ARTICULO = "Referencia del articulo";
	/**
	 * Propiedad SERIE de la clase Desglose de Pedido.
	 */
	public static final String PROPIEDAD_DESGLOSE_DE_PEDIDO_SERIE = "Numero de serie";
	/**
	 * Propiedad COLOR de la clase Desglose de Pedido.
	 */
	public static final String PROPIEDAD_DESGLOSE_DE_PEDIDO_TIPO = "Tipo";
	/**
	 * Propiedad TAMA�O de la clase Desglose de Pedido.
	 */
	public static final String PROPIEDAD_DESGLOSE_DE_PEDIDO_TAMA�O = "Tama�o";

	/**
	 * Pripiedad NUMERO_DE_PEDIDO
	 */
	public static final String PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_PEDIDO = "Numero de pedido";

	/**
	 * Aqui empiezan las de CLIENTES
	 */
	/**
	 * Propiedad NUMERO de la clase Clientes.
	 */
	public static final String PROPIEDAD_CLIENTE_NUMERO = "Numero de cliente";
	/**
	 * Propiedad NOMBRE_Y_APELLIDOS de la clase Clientes.
	 */
	public static final String PROPIEDAD_CLIENTE_NOMBRE_Y_APELLIDOS = "Nombre y apellidos del cliente";
	/**
	 * Propiedad DNI_NIF de la clase Clientes.
	 */
	public static final String PROPIEDAD_CLIENTE_DNI_NIF = "DNI o NIF del cliente";
	/**
	 * Propiedad DIRECCION de la clase Clientes.
	 */
	public static final String PROPIEDAD_CLIENTE_DIRECCION = "Direccion del cliente";
	/**
	 * Propiedad PROVINCIA de la clase Clientes.
	 */
	public static final String PROPIEDAD_CLIENTE_PROVINCIA = "Provincia del cliente";
	/**
	 * Propiedad TELEFONO de la clase Clientes.
	 */
	public static final String PROPIEDAD_CLIENTE_TELEFONO = "Telefono del cliente";
	/**
	 * Propiedad EMAIL de la clase Clientes.
	 */
	public static final String PROPIEDAD_CLIENTE_EMAIL = "Email del cliente";

	/**
	 * Aqui empiezan las de ARTICULO
	 */
	/**
	 * Propiedad REFERENCIA de la clase Articulos.
	 */
	public static final String PROPIEDAD_ARTICULO_REFERENCIA = "Referencia del articulo";
	/**
	 * Propiedad SERIE de la clase Articulos.
	 */
	public static final String PROPIEDAD_ARTICULO_SERIE = "Numero de Serie";
	/**
	 * Propiedad DESCRIPCION de la clase Articulos.
	 */
	public static final String PROPIEDAD_ARTICULO_DESCRIPCION = "Descripcion del articulo";
	/**
	 * Propiedad CANTIDAD_DE_MATERIAL de la clase Articulos.
	 */
	public static final String PROPIEDAD_ARTICULO_CANTIDAD_DE_MATERIAL = "Cantidad de material";

	/**
	 * Propiedad PRECIO de la clase Articulos.
	 */
	public static final String PROPIEDAD_ARTICULO_PRECIO = "Precio del articulo";

}
