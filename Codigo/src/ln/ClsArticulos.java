package ln;

import static comun.clsConstantes.PROPIEDAD_ARTICULO_CANTIDAD_DE_MATERIAL;
import static comun.clsConstantes.PROPIEDAD_ARTICULO_DESCRIPCION;
import static comun.clsConstantes.PROPIEDAD_ARTICULO_PRECIO;
import static comun.clsConstantes.PROPIEDAD_ARTICULO_REFERENCIA;
import static comun.clsConstantes.PROPIEDAD_ARTICULO_SERIE;

import java.util.Date;

import comun.ItfProperty;
import excepciones.clsRunTimeExcepcion;

/**
 * @author Aitor Ubierna Esta seria la clase donde generamos los objetos de la
 *         entidad Articulos.
 */

public class ClsArticulos implements ItfProperty {

	/**
	 * Atributo para recoger la referencia
	 */
	private int Referencia;
	/**
	 * Atributo para recoger el numero de serie
	 */
	private int Serie;
	/**
	 * Atributo para recoger la descripcion
	 */
	private String Descripcion;
	/**
	 * Atributo para recoger la cantidad de materiales
	 */
	private int CantidadMaterial;
	/**
	 * Atributo para recoger el precio
	 */
	private double Precio;

	/**
	 * Este seria el constructor de la entidad Articulos.
	 * 
	 * @param referencia       parametro referencia
	 * @param serie            parametro serie
	 * @param descripcion      parametro descripcion
	 * @param cantidadMaterial parametro cantidad de material
	 * @param precio           parametro precio
	 */
	public ClsArticulos(int referencia, int serie, String descripcion, int cantidadMaterial, double precio) {
		Referencia = referencia;
		Serie = serie;
		Descripcion = descripcion;
		CantidadMaterial = cantidadMaterial;
		Precio = precio;
	}

	/**
	 * Estos serian los metodos getters y setters de la clase.
	 * 
	 * @return nos genera unos returns
	 */

	public int getReferencia() {
		return Referencia;
	}

	public void setReferencia(int referencia) {
		Referencia = referencia;
	}

	public int getSerie() {
		return Serie;
	}

	public void setSerie(int serie) {
		Serie = serie;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	public int getCantidadMaterial() {
		return CantidadMaterial;
	}

	public void setCantidadMaterial(int cantidadMaterial) {
		CantidadMaterial = cantidadMaterial;
	}

	public double getPrecio() {
		return Precio;
	}

	public void setPrecio(double precio) {
		Precio = precio;
	}

	/**
	 * Metodos implementados de itfProperty
	 */
	@Override
	public String getStringProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_ARTICULO_DESCRIPCION:
			return this.getDescripcion();
		default:
			throw new clsRunTimeExcepcion(propiedad);

		}

	}

	@Override
	public Integer getIntegerProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_ARTICULO_CANTIDAD_DE_MATERIAL:
			return this.getCantidadMaterial();
		case PROPIEDAD_ARTICULO_REFERENCIA:
			return this.getReferencia();
		case PROPIEDAD_ARTICULO_SERIE:
			return this.getSerie();
		default:
			throw new clsRunTimeExcepcion(propiedad);
		}

	}

	@Override
	public Float getFloatProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDoubleProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_ARTICULO_PRECIO:
			return this.getPrecio();
		default:
			throw new clsRunTimeExcepcion(propiedad);
		}

	}

	@Override
	public char getCharProperty(String propiedad) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Date getDateProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getBooleanProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

}
