package ln;

import static comun.clsConstantes.PROPIEDAD_SERIES_DESCRIPCION;
import static comun.clsConstantes.PROPIEDAD_SERIES_NUMERO_DE_SERIE;

import java.util.Date;

import comun.ItfProperty;
import excepciones.clsRunTimeExcepcion;

/**
 * @author Aitor Ubierna En esta clase vamos a recoger los atributos de la tabla
 *         series.
 */
public class ClsSeries implements ItfProperty {

	/**
	 * Atributo para recoger el numero de la serie
	 */
	private int NumeroDeSerie;
	/**
	 * Atributo para recoger la descripcion
	 */
	private String Descripcion;

	/**
	 * Aqui generamos el constructor de la clase
	 * 
	 * @param numeroDeSerie parametro numero de serie
	 * @param descripcion   parametro descripcion
	 * 
	 */
	public ClsSeries(int numeroDeSerie, String descripcion) {
		NumeroDeSerie = numeroDeSerie;
		Descripcion = descripcion;
	}

	/**
	 * 
	 * Ahora generamos los metodos getters y setters
	 * 
	 * @return nos genera unos return.
	 * 
	 */

	public int getNumeroDeSerie() {
		return NumeroDeSerie;
	}

	public void setNumeroDeSerie(int numeroDeSerie) {
		NumeroDeSerie = numeroDeSerie;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	/**
	 * Implementación de itfProperty
	 */
	@Override
	public String getStringProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_SERIES_DESCRIPCION:
			return this.getDescripcion();
		default:
			throw new clsRunTimeExcepcion(propiedad);
		}
	}

	@Override
	public Integer getIntegerProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_SERIES_NUMERO_DE_SERIE:
			return this.getNumeroDeSerie();
		default:
			throw new clsRunTimeExcepcion(propiedad);
		}
	}

	@Override
	public Float getFloatProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDoubleProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public char getCharProperty(String propiedad) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Date getDateProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getBooleanProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

}
