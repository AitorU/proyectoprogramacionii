package ln;

import static comun.clsConstantes.PROPIEDAD_TAPAS_DESCRIPCION;
import static comun.clsConstantes.PROPIEDAD_TAPAS_PRECIO;
import static comun.clsConstantes.PROPIEDAD_TAPAS_REFERENCIA;

import java.util.Date;

import comun.ItfProperty;
import excepciones.clsRunTimeExcepcion;

/**
 * 
 * @author Aitor Ubierna Clase para la generación de tapas heredadad de materias
 */

public class ClsTapas extends ClsMaterias implements ItfProperty {

	/**
	 * 
	 * Este es el constructor de la entidad Tapas con herencia de la clase Materias
	 * Primas.
	 * 
	 * @param referencia  parametro referencia.
	 * @param descripcion parametro descripcion.
	 * @param precio      parametro precio.
	 * 
	 */
	public ClsTapas(int referencia, String descripcion, double precio) {
		super(referencia, descripcion, precio);
	}

	/**
	 * Implementación de itfProperty
	 */
	@Override
	public String getStringProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_TAPAS_DESCRIPCION:
			return this.getDescripcion();
		default:
			throw new clsRunTimeExcepcion(propiedad);
		}
	}

	@Override
	public Integer getIntegerProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_TAPAS_REFERENCIA:
			return this.getReferencia();
		default:
			throw new clsRunTimeExcepcion(propiedad);
		}
	}

	@Override
	public Float getFloatProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDoubleProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_TAPAS_PRECIO:
			return this.getPrecio();
		default:
			throw new clsRunTimeExcepcion(propiedad);
		}
	}

	@Override
	public char getCharProperty(String propiedad) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Date getDateProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getBooleanProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

}
