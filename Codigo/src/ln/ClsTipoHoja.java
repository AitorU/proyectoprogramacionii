package ln;

import static comun.clsConstantes.PROPIEDAD_TIPOHOJA_DESCRIPCION;
import static comun.clsConstantes.PROPIEDAD_TIPOHOJA_PRECIO;
import static comun.clsConstantes.PROPIEDAD_TIPOHOJA_REFERENCIA;

import java.util.Date;

import comun.ItfProperty;
import excepciones.clsRunTimeExcepcion;

/**
 * 
 * @author Aitor Ubierna Clase para la generación de tiposdehojas heredadad de
 *         materias
 */

public class ClsTipoHoja extends ClsMaterias implements ItfProperty {

	/**
	 * 
	 * Este es el constructor de la entidad TipoHoja con herencia de la clase
	 * Materias Primas.
	 * 
	 * @param referencia  parametro referencia.
	 * @param descripcion parametro descripcion.
	 * @param precio      parametro precio.
	 * 
	 */
	public ClsTipoHoja(int referencia, String descripcion, double precio) {
		super(referencia, descripcion, precio);
	}

	@Override
	public String getStringProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_TIPOHOJA_DESCRIPCION:
			return this.getDescripcion();
		default:
			throw new clsRunTimeExcepcion(propiedad);
		}
	}

	@Override
	public Integer getIntegerProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_TIPOHOJA_REFERENCIA:
			return this.getReferencia();
		default:
			throw new clsRunTimeExcepcion(propiedad);
		}
	}

	@Override
	public Float getFloatProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDoubleProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_TIPOHOJA_PRECIO:
			return this.getPrecio();
		default:
			throw new clsRunTimeExcepcion(propiedad);
		}
	}

	@Override
	public char getCharProperty(String propiedad) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Date getDateProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getBooleanProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

}
