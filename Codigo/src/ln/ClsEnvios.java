package ln;

import static comun.clsConstantes.PROPIEDAD_ENVIOS_CPD_DE_ENVIO;
import static comun.clsConstantes.PROPIEDAD_ENVIOS_DIRECCION_DE_ENVIO;
import static comun.clsConstantes.PROPIEDAD_ENVIOS_NOMBRE_CLIENTE;
import static comun.clsConstantes.PROPIEDAD_ENVIOS_NUMERO_DE_CLIENTE_ENVIO;
import static comun.clsConstantes.PROPIEDAD_ENVIOS_NUMERO_DE_ENVIO;
import static comun.clsConstantes.PROPIEDAD_ENVIOS_POBLACION_DE_ENVIO;
import static comun.clsConstantes.PROPIEDAD_ENVIOS_PROVINCIA_DE_ENVIO;
import static comun.clsConstantes.PROPIEDAD_ENVIOS_TELEFONO_DE_ENVIO;

import java.util.Date;

import comun.ItfProperty;
import excepciones.clsRunTimeExcepcion;

/**
 * @author Aitor Ubierna En esta clase vamos a recoger los atributos de la tabla
 *         envios.
 */

public class ClsEnvios implements ItfProperty {

	/**
	 * Atributo para recoger numero de envio
	 */
	private int NumeroDeEnvio;
	/**
	 * Atributo para recoger nombre y apellidos del cliente
	 */
	private String NombreCliente;
	/**
	 * Atributo para recoger direccion de envio
	 */
	private String DireccionDeEnvio;
	/**
	 * Atributo para recoger poblacion de envio
	 */
	private String PoblacionDeEnvio;
	/**
	 * Atributo para recoger codigo postal de envio
	 */
	private int CPDeEnvio;
	/**
	 * Atributo para recoger provincia de envio
	 */
	private String ProvinciaDeEnvio;
	/**
	 * Atributo para recoger telefono de envio
	 */
	private int TelefonoDeEnvio;
	/**
	 * Atributo para recoger numero del cliente (Herencia de la tabla cliente)
	 */
	private int NumeroDeCliente_Envio;

	public ClsEnvios(int numeroDeEnvio, String nombreCliente, String direccionDeEnvio, String poblacionDeEnvio,
			int cPDeEnvio, String provinciaDeEnvio, int telefonoDeEnvio, int numeroDeCliente_Envio) {
		NumeroDeEnvio = numeroDeEnvio;
		NombreCliente = nombreCliente;
		DireccionDeEnvio = direccionDeEnvio;
		PoblacionDeEnvio = poblacionDeEnvio;
		CPDeEnvio = cPDeEnvio;
		ProvinciaDeEnvio = provinciaDeEnvio;
		TelefonoDeEnvio = telefonoDeEnvio;
		NumeroDeCliente_Envio = numeroDeCliente_Envio;
	}

	/**
	 * Ahora generamos los metodos getters y setters
	 * 
	 * @return nos genera unos returns
	 */

	public int getNumeroDeEnvio() {
		return NumeroDeEnvio;
	}

	public void setNumeroDeEnvio(int numeroDeEnvio) {
		NumeroDeEnvio = numeroDeEnvio;
	}

	public String getNombreCliente() {
		return NombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		NombreCliente = nombreCliente;
	}

	public String getDireccionDeEnvio() {
		return DireccionDeEnvio;
	}

	public void setDireccionDeEnvio(String direccionDeEnvio) {
		DireccionDeEnvio = direccionDeEnvio;
	}

	public String getPoblacionDeEnvio() {
		return PoblacionDeEnvio;
	}

	public void setPoblacionDeEnvio(String poblacionDeEnvio) {
		PoblacionDeEnvio = poblacionDeEnvio;
	}

	public int getCPDeEnvio() {
		return CPDeEnvio;
	}

	public void setCPDeEnvio(int cPDeEnvio) {
		CPDeEnvio = cPDeEnvio;
	}

	public String getProvinciaDeEnvio() {
		return ProvinciaDeEnvio;
	}

	public void setProvinciaDeEnvio(String provinciaDeEnvio) {
		ProvinciaDeEnvio = provinciaDeEnvio;
	}

	public int getTelefonoDeEnvio() {
		return TelefonoDeEnvio;
	}

	public void setTelefonoDeEnvio(int telefonoDeEnvio) {
		TelefonoDeEnvio = telefonoDeEnvio;
	}

	public int getNumeroDeCliente_Envio() {
		return NumeroDeCliente_Envio;
	}

	public void setNumeroDeCliente_Envio(int numeroDeCliente_Envio) {
		NumeroDeCliente_Envio = numeroDeCliente_Envio;
	}

	/**
	 * Implementación de itfProperty
	 */
	@Override
	public String getStringProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_ENVIOS_NOMBRE_CLIENTE:
			return this.getNombreCliente();
		case PROPIEDAD_ENVIOS_DIRECCION_DE_ENVIO:
			return this.getDireccionDeEnvio();
		case PROPIEDAD_ENVIOS_POBLACION_DE_ENVIO:
			return this.getPoblacionDeEnvio();
		case PROPIEDAD_ENVIOS_PROVINCIA_DE_ENVIO:
			return this.getProvinciaDeEnvio();
		default:
			throw new clsRunTimeExcepcion(propiedad);
		}
		
	}

	@Override
	public Integer getIntegerProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_ENVIOS_NUMERO_DE_ENVIO:
			return this.getNumeroDeEnvio();
		case PROPIEDAD_ENVIOS_CPD_DE_ENVIO:
			return this.getCPDeEnvio();
		case PROPIEDAD_ENVIOS_TELEFONO_DE_ENVIO:
			return this.getTelefonoDeEnvio();
		case PROPIEDAD_ENVIOS_NUMERO_DE_CLIENTE_ENVIO:
			return this.getNumeroDeCliente_Envio();
		default:
			throw new clsRunTimeExcepcion(propiedad);
		}
	}

	@Override
	public Float getFloatProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDoubleProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public char getCharProperty(String propiedad) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Date getDateProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getBooleanProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

}
