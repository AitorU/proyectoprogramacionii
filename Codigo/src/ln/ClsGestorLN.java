package ln;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import com.sun.javadoc.ThrowsTag;

import comparador.ClsComparadorDesglosesID;
import comparador.ClsComparadorMaterialesID;
import comparador.ClsComparadorPedidosID;
import comparador.ClsComparadorSeriesID;
import comparador.ClsComparadorTapasID;
import comparador.ClsComparadorTipoHojaID;
import comparador.clsComparadorEnvio;
import excepciones.ClsBorrarExcepcion;
import comun.ItfProperty;
import comun.clsConstantes;
import comun.itfData;
import ld.clsDatos;

/**
 * 
 * @author Aitor Ubierna Clase para el gestor de LN
 */

public class ClsGestorLN {

	clsDatos objDatos = new clsDatos();

	/**
	 * Instanciamos los Arrays donde guardar los objetos.
	 */
	ArrayList<ClsArticulos> MiListaDeArticulos;
	ArrayList<ClsClientes> MiListaDeClientes;
	ArrayList<ClsDesgloseDePedido> MiListaDeDesgloses;
	ArrayList<ClsEnvios> MiListaDeEnvios;
	ArrayList<ClsPedidos> MiListaDePedidos;
	ArrayList<ClsSeries> MiListaDeSeries;
	ArrayList<ClsTapas> MiListaDeTapas;
	ArrayList<ClsTipoHoja> MiListaDeHojas;
	ArrayList<ClsMateriales> MiListaDeMateriales;

	public ClsGestorLN() {

		/**
		 * Inicializamos los Arrays.
		 */
		MiListaDeArticulos = new ArrayList<ClsArticulos>();
		MiListaDeClientes = new ArrayList<ClsClientes>();
		MiListaDeDesgloses = new ArrayList<ClsDesgloseDePedido>();
		MiListaDeEnvios = new ArrayList<ClsEnvios>();
		MiListaDePedidos = new ArrayList<ClsPedidos>();
		MiListaDeSeries = new ArrayList<ClsSeries>();
		MiListaDeTapas = new ArrayList<ClsTapas>();
		MiListaDeHojas = new ArrayList<ClsTipoHoja>();
		MiListaDeMateriales = new ArrayList<ClsMateriales>();

	}

	/**
	 * Metodo para crear Articulos.
	 * 
	 * @param Referencia       parametro referencia
	 * @param Serie            parametro serie
	 * @param Descripcion      parametro descripcion
	 * @param CantidadMaterial parametro cantidad de material
	 * @param Precio           parametro precio
	 * @return nos dice si se ha hecho o no
	 * @throws SQLException lanza excepcion
	 */
	public boolean CrearArticulos(int Referencia, int Serie, String Descripcion, int CantidadMaterial, double Precio)
			throws SQLException {

		boolean Hecho = false;

		/**
		 * Instanciamos y crearmos el objeto
		 */
		ClsArticulos objArticulos = new ClsArticulos(Referencia, Serie, Descripcion, CantidadMaterial, Precio);

		/**
		 * Miramos que no se repitan los objetos y los a�adimos al Array y al la BD.
		 */
		if (!ExisteArticulos(objArticulos)) {
			Hecho = true;
			/**
			 * A�adimos el objeto a el array.
			 */
			MiListaDeArticulos.add(objArticulos);
			/**
			 * Mandamos los datos a LD para introducir a BD
			 */
			objDatos.conectarBD();
			objDatos.InsertarArticulos(Referencia, Serie, Descripcion, CantidadMaterial, Precio);
			objDatos.desconectarBD();
		}
		return Hecho;
	}

	/**
	 * Inicializaci�n del metodo para crear objetos tapa.
	 * 
	 * @throws SQLException fallo bbdd
	 * @return hecho
	 * @param Referencia_Tapa referencia tapa
	 * @param Descripcion_Tapa descripcion tapa
	 * @param Precio_Tapa precio tapa
	 */
	public boolean CrearTapa(int Referencia_Tapa, String Descripcion_Tapa, Double Precio_Tapa) throws SQLException {

		/**
		 * comprobar que no se repite
		 */
		boolean hecho = false;
		/**
		 * Instanciamos y crearmos el objeto
		 */
		ClsTapas objTapa = new ClsTapas(Referencia_Tapa, Descripcion_Tapa, Precio_Tapa);

		/**
		 * Miramos que no se repitan los objetos y los a�adimos al Array y al la BD.
		 */
		if (!ExisteTapa(objTapa)) {

			hecho = true;
			/**
			 * A�adimos el objeto a el array.
			 */
			MiListaDeTapas.add(objTapa);

			/**
			 * Llamada a introducir datos con paso de parametros.
			 */
			objDatos.conectarBD();
			objDatos.InsertarTapas(Referencia_Tapa, Descripcion_Tapa, Precio_Tapa);
			;
			objDatos.desconectarBD();
		}

		return hecho;

	}

	/**
	 * Inicializaci�n del metodo para crear objetos tipohoja.
	 * 
	 * @throws SQLException fallo bbdd
	 * @return hecho
	 * @param Referencia_TipoHoja referencia tipo de hoja
	 * @param Descripcion_TipoHoja descripcion tipo de hoja
	 * @param Precio_TipoHoja precio tipo de hoja
	 */
	public boolean CrearTipoHoja(int Referencia_TipoHoja, String Descripcion_TipoHoja, Double Precio_TipoHoja)
			throws SQLException {

		/**
		 * comprobar que no se repite
		 */
		boolean hecho = false;
		/**
		 * Instanciamos y crearmos el objeto
		 */
		ClsTipoHoja objTipo = new ClsTipoHoja(Referencia_TipoHoja, Descripcion_TipoHoja, Precio_TipoHoja);

		/**
		 * Miramos que no se repitan los objetos y los a�adimos al Array y al la BD.
		 */
		if (!ExisteTipo(objTipo)) {

			hecho = true;
			/**
			 * A�adimos el objeto a el array.
			 */
			MiListaDeHojas.add(objTipo);

			/**
			 * Llamada a introducir datos con paso de parametros.
			 */
			objDatos.conectarBD();
			objDatos.InsertarTiposHoja(Referencia_TipoHoja, Descripcion_TipoHoja, Precio_TipoHoja);
			;
			objDatos.desconectarBD();
		}

		return hecho;

	}

	/**
	 * Inicializaci�n del metodo para crear objetos tipo materiales.
	 * 
	 * @throws SQLException fallo bbdd
	 * @return hecho
	 * @param Referencia_Material referencia material
	 * @param Descripcion_Material descripcion material
	 * @param Precio_Material precio material
	 */
	public boolean CrearMateriales(int Referencia_Material, String Descripcion_Material, Double Precio_Material)
			throws SQLException {

		/**
		 * comprobar que no se repite
		 */
		boolean hecho = false;
		/**
		 * Instanciamos y crearmos el objeto
		 */
		ClsMateriales objMateriales = new ClsMateriales(Referencia_Material, Descripcion_Material, Precio_Material);

		/**
		 * Miramos que no se repitan los objetos y los a�adimos al Array y al la BD.
		 */
		if (!ExisteMateriales(objMateriales)) {

			hecho = true;
			/**
			 * A�adimos el objeto a el array.
			 */
			MiListaDeMateriales.add(objMateriales);

			/**
			 * Llamada a introducir datos con paso de parametros.
			 */
			objDatos.conectarBD();
			objDatos.InsertarMateriales(Referencia_Material, Descripcion_Material, Precio_Material);
			objDatos.desconectarBD();
		}

		return hecho;

	}

	/**
	 * Inicializaci�n del metodo para crear envios.
	 * @throws SQLException fallo bbdd
	 * @return hecho
	 * @param NumeroDeEnvio numero envio
	 * @param NombreCliente nombre cliente
	 * @param DireccionDeEnvio direccion de envio
	 * @param PoblacionDeEnvio poblacion
	 * @param CPDeEnvio cp
	 * @param ProvinciaDeEnvio provincia
	 * @param TelefonoDeEnvio telefono
	 * @param NumeroDeCliente_Envio numero del cliente
	 */
	public boolean CrearEnvios(int NumeroDeEnvio, String NombreCliente, String DireccionDeEnvio,
			String PoblacionDeEnvio, int CPDeEnvio, String ProvinciaDeEnvio, int TelefonoDeEnvio,
			int NumeroDeCliente_Envio) throws SQLException {

		/**
		 * comprobar que no se repite
		 */
		boolean hecho = false;
		/**
		 * Instanciamos y crearmos el objeto
		 */

		ClsEnvios objEnvios = new ClsEnvios(NumeroDeEnvio, NombreCliente, DireccionDeEnvio, PoblacionDeEnvio, CPDeEnvio,
				ProvinciaDeEnvio, TelefonoDeEnvio, NumeroDeCliente_Envio);

		/**
		 * Miramos que no se repitan los objetos y los a�adimos al Array y al la BD.
		 */
		if (!ExisteEnvios(objEnvios)) {

			hecho = true;
			/**
			 * A�adimos el objeto a el array.
			 */
			MiListaDeEnvios.add(objEnvios);

			/**
			 * Llamada a introducir datos con paso de parametros.
			 */
			objDatos.conectarBD();
			objDatos.InsertarEnvios(NumeroDeEnvio, NombreCliente, DireccionDeEnvio, PoblacionDeEnvio, CPDeEnvio,
					ProvinciaDeEnvio, TelefonoDeEnvio, NumeroDeCliente_Envio);
			objDatos.desconectarBD();
		}

		return hecho;
	}

	/**
	 * Inicializaci�n del metodo para crear clientes.
	 * @throws SQLException fallo bbdd
	 * @return hecho
	 * @param NumeroDeCliente numero del cliente
	 * @param NombreYApellidos nombre y apellidos
	 * @param DNI dni
	 * @param DireccionDeCliente direccion
	 * @param Provincia provincia
	 * @param Telefono telefono
	 * @param Email e-mail
	 */
	public boolean CrearClientes(int NumeroDeCliente, String NombreYApellidos, String DNI, String DireccionDeCliente,
			String Provincia, int Telefono, String Email) throws SQLException {

		/**
		 * comprobar que no se repite
		 */
		boolean hecho = false;
		/**
		 * Instanciamos y crearmos el objeto
		 */

		ClsClientes objCliente = new ClsClientes(NumeroDeCliente, NombreYApellidos, DNI, DireccionDeCliente, Provincia,
				Telefono, Email);

		/**
		 * Miramos que no se repitan los objetos y los a�adimos al Array y al la BD.
		 */
		if (!ExisteClientes(objCliente)) {

			hecho = true;
			/**
			 * A�adimos el objeto a el array.
			 */
			MiListaDeClientes.add(objCliente);

			/**
			 * Llamada a introducir datos con paso de parametros.
			 */
			objDatos.conectarBD();
			objDatos.InsertarClientes(NumeroDeCliente, NombreYApellidos, DNI, DireccionDeCliente, Provincia, Telefono,
					Email);
			objDatos.desconectarBD();
		}

		return hecho;
	}

	/**
	 * Metodo para crear series.
	 * 
	 * @param NumeroDeSerie     parametro numero de serie
	 * @param Descripcion_Serie parametro descripcion de serie.
	 * @return nos dice si se ha hecho o no
	 * @throws SQLException lanzamos excepcion
	 */
	public boolean CrearSerie(int NumeroDeSerie, String Descripcion_Serie) throws SQLException {

		/**
		 * comprobar que no se repite
		 */
		boolean hecho = false;
		/**
		 * Instanciamos y crearmos el objeto
		 */
		ClsSeries objSeries;
		objSeries = new ClsSeries(NumeroDeSerie, Descripcion_Serie);

		/**
		 * Miramos que no se repitan los objetos y los a�adimos al Array y al la BD.
		 */
		if (!ExisteSeries(objSeries)) {

			hecho = true;
			/**
			 * A�adimos el objeto a el array.
			 */
			MiListaDeSeries.add(objSeries);

			/**
			 * Llamada a introducir datos con paso de parametros.
			 */
			objDatos.conectarBD();
			objDatos.InsertarSerie(NumeroDeSerie, Descripcion_Serie);
			objDatos.desconectarBD();
		}

		return hecho;
	}

	/**
	 * Metodo para crear Pedidos.
	 * 
	 * @param NumeroDePedido            parametro numero de pedido
	 * @param FechaDePedido             parametro fecha de pedido
	 * @param FechaDeEntrega            parametro fecha de entrega
	 * @param Entregado                 parametro entregado
	 * @param NumeroDeCliente_Pedidos   parametro numero de cliente
	 * @param NombreYApelliosDelCliente parametro nombre y apellidos del cliente
	 * @return nos dice si se ha hecho o no
	 * @throws SQLException lanzamos excepcion.
	 */
	public boolean CrearPedidos(int NumeroDePedido, Date FechaDePedido, Date FechaDeEntrega, Boolean Entregado,
			int NumeroDeCliente_Pedidos, String NombreYApelliosDelCliente) throws SQLException {

		boolean Hecho = false;

		/**
		 * Instanciamos y creamos el objeto
		 */
		ClsPedidos objPedido = new ClsPedidos(NumeroDePedido, FechaDePedido, FechaDeEntrega, Entregado,
				NombreYApelliosDelCliente, NumeroDeCliente_Pedidos);

		/**
		 * Miramos que no se repitan los objetos y los a�adimos al Array y al la BD.
		 */
		if (!ExistePedidos(objPedido)) {
			Hecho = true;
			/**
			 * A�adimos el objeto a el array.
			 */
			MiListaDePedidos.add(objPedido);

			/**
			 * Llamada a introducir datos con paso de parametros
			 */
			objDatos.conectarBD();
			objDatos.InsertarPedidos(NumeroDePedido, FechaDePedido, FechaDeEntrega, Entregado, NumeroDeCliente_Pedidos,
					NombreYApelliosDelCliente);
			objDatos.desconectarBD();
		}
		return Hecho;
	}

	/**
	 * Metodo para crear desgloses
	 * 
	 * @param NumeroPedido numero de pedido
	 * @param Referencia referencia
	 * @param serie serie
	 * @param Tipo tipo
	 * @param Tamano tama�o
	 * @param NPedidoDesglose numero
	 * @return Hecho
	 * @throws SQLException fallo bbdd
	 */

	public boolean CrearDesgloseDePedido(int NumeroPedido, int Referencia, int serie, String Tipo, String Tamano,
			int NPedidoDesglose) throws SQLException {

		boolean Hecho = false;
		/**
		 * Instanciamos y creamos el objeto
		 */
		ClsDesgloseDePedido objdesglose = new ClsDesgloseDePedido(NumeroPedido, Referencia, serie, Tipo, Tamano,
				NPedidoDesglose);
		/**
		 * Miramos que no se repita el objeto y lo a�adimos a la BD y Array
		 */
		if (!ExisteDesglose(objdesglose)) {
			Hecho = true;
			MiListaDeDesgloses.add(objdesglose);
			/**
			 * Llamada a introducir datos con paso de parametros
			 */
			objDatos.conectarBD();
			objDatos.InsertarDesglose(NumeroPedido, Referencia, serie, Tipo, Tamano, NPedidoDesglose);
			objDatos.desconectarBD();
		}
		return Hecho;
	}

	/**
	 * Metodo para generar los datos de la series con los datos recuperados de la BD
	 * @throws SQLException fallo bbdd
	 */
	public void DameSeries() throws SQLException {

		objDatos.conectarBD();
		/**
		 * Recogemos datos desde LD y consturimos objetos.
		 */
		ResultSet Resultado = objDatos.consultarSeries();

		/**
		 * recorremos los parametros del objeto
		 */
		while (Resultado.next()) {
			/**
			 * los guardamos en variables
			 */
			int NumeroDeSerie = Resultado.getInt("NDeSerie");
			String Descripcion_Series = Resultado.getString("Descripcion");
			/**
			 * generamos objeto del tipo de la clase
			 */
			ClsSeries objSeries = new ClsSeries(NumeroDeSerie, Descripcion_Series);

			/**
			 * A�adimos el objeto al Array
			 */
			MiListaDeSeries.add(objSeries);

		}
		objDatos.desconectarBD();

	}

	/**
	 * Metodo para sacar por pantalla en LP los datos de series
	 * 
	 * @return genera un return.
	 */
	public ArrayList<ItfProperty> ObtenerSeries() {

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * grabamos un array en otro y lo devolvemos
		 */
		for (ClsSeries a : MiListaDeSeries) {
			retorno.add(a);

		}

		return retorno;

	}

	/**
	 * Metodo para generar los datos de las tapas con los datos recuperados de la BD
	 * @throws SQLException fallo bbdd
	 */
	public void DameTapas() throws SQLException {

		objDatos.conectarBD();
		/**
		 * Recogemos datos desde LD y consturimos objetos.
		 */
		ResultSet Resultado = objDatos.consultarTapas();

		/**
		 * obtenemos lo dato del objeto devuelto
		 */
		while (Resultado.next()) {
			/**
			 * guardamos los datos en variables
			 */
			int Referencia = Resultado.getInt("Referencia");
			String Descripcion = Resultado.getString("Descripcion");
			Double Precio = Resultado.getDouble("Precio");
			/**
			 * generamos un objeto del tipo de la clase
			 */
			ClsTapas Objtapa = new ClsTapas(Referencia, Descripcion, Precio);

			/**
			 * A�adimos el objetos al Array
			 */
			MiListaDeTapas.add(Objtapa);

		}
		objDatos.desconectarBD();

	}

	/**
	 * Metodo para sacar por pantalla las tapas
	 * @return retorno
	 */
	public ArrayList<ItfProperty> ObtenerTapas() {

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * grabamos un array en otro y lo devolvemos
		 */
		for (ClsTapas a : MiListaDeTapas) {
			retorno.add(a);

		}

		return retorno;

	}

	/**
	 * Metodo para generar los datos de los tipos de hojas con los datos recuperados
	 * de la BD
	 * @throws SQLException fallo bbdd
	 */
	public void DameTipos() throws SQLException {

		objDatos.conectarBD();
		/**
		 * Recogemos datos desde LD y consturimos objetos.
		 */
		ResultSet Resultado = objDatos.consultarTiposHoja();

		/**
		 * obtenemos lo dato del objeto devuelto
		 */
		while (Resultado.next()) {
			/**
			 * guardamos los datos en variables
			 */
			int Referencia = Resultado.getInt("Referencia");
			String Descripcion = Resultado.getString("Descripcion");
			Double Precio = Resultado.getDouble("Precio");
			/**
			 * generamos un objeto del tipo de la clase
			 */
			ClsTipoHoja Objtipo = new ClsTipoHoja(Referencia, Descripcion, Precio);

			/**
			 * A�adimos el objetos al Array
			 */
			MiListaDeHojas.add(Objtipo);

		}
		objDatos.desconectarBD();

	}

	/**
	 * Metodo para generar sacar por pantalla los tipos de hoja
	 * @return retorno
	 */
	public ArrayList<ItfProperty> ObtenerTiposHoja() {

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * grabamos un array en otro y lo devolvemos
		 */
		for (ClsTipoHoja a : MiListaDeHojas) {
			retorno.add(a);

		}

		return retorno;

	}

	/**
	 * Metodo para generar los datos de los tipos de materiales con los datos
	 * recuperados de la BD
	 * @throws SQLException fallo bdd
	 */
	public void DameMateriales() throws SQLException {

		objDatos.conectarBD();
		/**
		 * Recogemos datos desde LD y consturimos objetos.
		 */
		ResultSet Resultado = objDatos.consultarMateriales();

		/**
		 * obtenemos lo dato del objeto devuelto
		 */
		while (Resultado.next()) {
			/**
			 * guardamos los datos en variables
			 */
			int Referencia = Resultado.getInt("Referencia");
			String Descripcion = Resultado.getString("Descripcion");
			Double Precio = Resultado.getDouble("Precio");
			/**
			 * generamos un objeto del tipo de la clase
			 */
			ClsMateriales Objmaterial = new ClsMateriales(Referencia, Descripcion, Precio);

			/**
			 * A�adimos el objetos al Array
			 */
			MiListaDeMateriales.add(Objmaterial);

		}
		objDatos.desconectarBD();

	}

	/**
	 * Metodo para sacar por pantalla los materiales
	 * @return retorno
	 */
	public ArrayList<ItfProperty> ObtenerMateriales() {

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * grabamos un array en otro y lo devolvemos
		 */
		for (ClsMateriales a : MiListaDeMateriales) {
			retorno.add(a);

		}

		return retorno;

	}
	
	/**
	 * Metodo para generar los datos de los envios con los datos
	 * recuperados de la BD
	 * @throws SQLException fallo bbdd
	 */
	public void DameEnvios() throws SQLException {

		objDatos.conectarBD();
		/**
		 * Recogemos datos desde LD y consturimos objetos.
		 */
		ResultSet Resultado = objDatos.consultarEnvios();

		/**
		 * Obtenemos los datos del objeto
		 */
		while (Resultado.next()) {
			/**
			 * Los guardamos en variables
			 */
			int NumeroDeEnvio = Resultado.getInt("NEnvio");
			String NombreCliente = Resultado.getString("NombreCliente");
			String DireccionDeEnvio = Resultado.getString("DireccionDeEnvio");
			String PoblacionDeEnvio = Resultado.getString("PoblacionDeEnvio");
			int CPDeENVIO = Resultado.getInt("CPDeEnvio");
			String ProvinciaDeEnvio = Resultado.getString("ProvinciaDeEnvio");
			int TelefonoDeEnvio = Resultado.getInt("TelefonoDeEnvio");
			int NumeroDeCliente_Envio = Resultado.getInt("Clientes_NCliente");
			/**
			 * Creamos un objeto del tipo de la clase
			 */
			ClsEnvios objEnvios = new ClsEnvios(NumeroDeEnvio, NombreCliente, DireccionDeEnvio, PoblacionDeEnvio,
					CPDeENVIO, ProvinciaDeEnvio, TelefonoDeEnvio, NumeroDeCliente_Envio);

			/**
			 * A�adimos el objeto al Array
			 */
			MiListaDeEnvios.add(objEnvios);

		}
		objDatos.desconectarBD();

	}

	/**
	 * Metodo para sacar por pantalla los datos de los envios
	 * @return retorno
	 */
	public ArrayList<ItfProperty> ObtenerEnvios(){

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * grabamos un array en otro y lo devolvemos
		 */
		for (ClsEnvios a : MiListaDeEnvios) {
			retorno.add(a);

		}

		return retorno;

	}
	
	/**
	 * Metodo intermedio para recuperar objetos cliente de la BD
	 * 
	 * @throws SQLException lanzamos excepcion a LP
	 */
	public void DameClientes() throws SQLException {
		objDatos.conectarBD();
		/**
		 * Recogemos datos desde LD y consturimos objetos.
		 */
		ResultSet Resultado = objDatos.consultarClientes();

		/**
		 * Optenemos los datos del objeto
		 */
		while (Resultado.next()) {
			/**
			 * los guardamos en variables
			 */
			int NDeClientes = Resultado.getInt("NCliente");
			String NombreYApellido = Resultado.getString("NombreYApellidos");
			String DNI_NIF = Resultado.getString("DNI_NIF");
			String DireccionDeClientes = Resultado.getString("DireccionDeCliente");
			String Provincia = Resultado.getString("Provincia");
			int Telefono = Resultado.getInt("Telefono");
			String Email = Resultado.getString("Email");
			/**
			 * Creamos un objeto del tipo de la clase
			 */
			ClsClientes objClientes = new ClsClientes(NDeClientes, NombreYApellido, DNI_NIF, DireccionDeClientes,
					Provincia, Telefono, Email);

			/**
			 * A�adimos el objeto al Array
			 */
			MiListaDeClientes.add(objClientes);

		}
		objDatos.desconectarBD();
	}

	/**
	 * Metodo para sacar por pantalla los datos de los clientes
	 * @return retorno
	 */
	public ArrayList<ItfProperty> ObtenerClientes(){

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * grabamos un array en otro y lo devolvemos
		 */
		for (ClsClientes a : MiListaDeClientes) {
			retorno.add(a);

		}

		return retorno;

	}
	
	/**
	 * Metodo intermedio para recuperar objetos pedido de la BD
	 * 
	 * @throws SQLException lanzamos excepcion a LP
	 */
	public void DamePedidos() throws SQLException {
		objDatos.conectarBD();
		/**
		 * Recogemos datos desde LD y consturimos objetos.
		 */
		ResultSet Resultado = objDatos.consultarPedidos();

		/**
		 * Obtenemos los datos del objeto
		 */
		while (Resultado.next()) {
			/**
			 * guardamos los datos en variables
			 */
			int NumeroDePedido = Resultado.getInt("NPedido");
			Date FechaDePedido = Resultado.getDate("Fecha_de_pedido");
			Date FechaDeEntrega = Resultado.getDate("Fecha_de_entrega");
			Boolean Entregado = Resultado.getBoolean("Entregado");
			int NumeroDeCliente_Pedidos = Resultado.getInt("Clientes_NCliente");
			String NombreYApelliosDelCliente = Resultado.getString("NombreYApellidos");
			/**
			 * Creamos un objeto del tipo de la clase
			 */
			ClsPedidos objPedido = new ClsPedidos(NumeroDePedido, FechaDePedido, FechaDeEntrega, Entregado,
					NombreYApelliosDelCliente, NumeroDeCliente_Pedidos);

			/**
			 * A�adimos el objeto al Array
			 */
			MiListaDePedidos.add(objPedido);

		}
		objDatos.desconectarBD();
	}

	/**
	 * Metodo para sacar por pantalla los datos de los pedidos
	 * @return retorno
	 */
	public ArrayList<ItfProperty> ObtenerPedidos() {

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * grabamos un array en otro y lo devolvemos
		 */
		for (ClsPedidos a : MiListaDePedidos) {
			retorno.add(a);

		}

		return retorno;


	}
	/**
	 * Metodo intermedio para recuperar objetos articulo de la BD
	 * 
	 * @throws SQLException lanzamos excepcion a LP
	 */
	public void DameArticulos() throws SQLException {
		objDatos.conectarBD();
		/**
		 * Recogemos datos desde LD y consturimos objetos.
		 */
		ResultSet Resultado = objDatos.consultarArticulos();

		/**
		 * Obtenemos los datos del objeto
		 */
		while (Resultado.next()) {
			/**
			 * Los guardamos en variables
			 */
			int Referencia = Resultado.getInt("Referencia");
			int Serie = Resultado.getInt("Serie");
			String Descripcion = Resultado.getString("Descripcion");
			int CantidadMaterial = Resultado.getInt("CantidadMaterial");
			double Precio = Resultado.getDouble("Precio");
			/**
			 * Creamos un objeto del tipo de la clase
			 */
			ClsArticulos objArticulos = new ClsArticulos(Referencia, Serie, Descripcion, CantidadMaterial,Precio);

			/**
			 * A�adimos el objeto al Array
			 */
			MiListaDeArticulos.add(objArticulos);

		}
		objDatos.desconectarBD();
	}
	

	/**
	 * Metodo para sacar por pantalla los articulos
	 * @return retorno
	 */
	public ArrayList<ItfProperty> ObtenerArticulos(){

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * grabamos un array en otro y lo devolvemos
		 */
		for (ClsArticulos a : MiListaDeArticulos) {
			retorno.add(a);

		}

		return retorno;
	}
	
	/**
	 * Metodo para recuperar objetos desglose de BD
	 * 
	 * @throws SQLException lanza excepcion de BD
	 */
	public void DameDesgloses() throws SQLException {
		objDatos.conectarBD();
		/**
		 * Recogemos datos desde LD y consturimos objetos.
		 */
		ResultSet Resultado = objDatos.consultarDesglose();

		/**
		 * obtenemos lo datos del objeto
		 */
		while (Resultado.next()) {
			/**
			 * Guardamos los datos en variables
			 */
			int NumeroDePedido = Resultado.getInt("NPedidoD");
			int ReferenciaDelArticulo = Resultado.getInt("Articulos_Referencia");
			int Serie = Resultado.getInt("Serie");
			String tipo = Resultado.getString("Tipo");
			String tama�o = Resultado.getString("Tama�o");
			int NumeroDeCliente_Desglose = Resultado.getInt("Pedidos_NPedido");

			/**
			 * Generamos un objeto del tipo de la clase
			 */
			ClsDesgloseDePedido objDesgloseDePedido = new ClsDesgloseDePedido(NumeroDePedido, ReferenciaDelArticulo,
					Serie, tipo,tama�o, NumeroDeCliente_Desglose);

			/**
			 * Los a�adimos al Array
			 */
			MiListaDeDesgloses.add(objDesgloseDePedido);

		}
		objDatos.desconectarBD();
	}

	/**
	 * Metodo para sacar por pantalla los desgloses
	 * @return retorno
	 */
	public ArrayList<ItfProperty> ObtenerDesgloses(){
		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * grabamos un array en otro y lo devolvemos
		 */
		for (ClsDesgloseDePedido a : MiListaDeDesgloses) {
			retorno.add(a);

		}

		return retorno;

		
	}

	/**
	 * Metodo para comprobar si los Objetos Series estan repetidos o no en nuestro
	 * Array
	 * 
	 * @param Series parametro serie
	 * @return nos devuelve true si esta repetido.
	 */
	public boolean ExisteSeries(ClsSeries Series) {

		/**
		 * Variable que determina el resultado
		 */
		boolean retorno = false;

		/**
		 * comprobacion a traves del metodo Equals
		 */
		for (ClsSeries b : MiListaDeSeries) {
			if (b.equals(Series))
				/**
				 * si se repiten devuelve true
				 */
				return true;

		}

		return retorno;
	}

	public boolean ExisteTapa(ClsTapas Tapa) {

		/**
		 * Variable que determina el resultado
		 */
		boolean retorno = false;

		/**
		 * comprobacion a traves del metodo Equals
		 */
		for (ClsTapas b : MiListaDeTapas) {
			if (b.equals(Tapa))
				/**
				 * si se repiten devuelve true
				 */
				return true;

		}

		return retorno;
	}

	public boolean ExisteTipo(ClsTipoHoja Tipo) {

		/**
		 * Variable que determina el resultado
		 */
		boolean retorno = false;

		/**
		 * comprobacion a traves del metodo Equals
		 */
		for (ClsTipoHoja b : MiListaDeHojas) {
			if (b.equals(Tipo))
				/**
				 * si se repiten devuelve true
				 */
				return true;

		}

		return retorno;
	}

	public boolean ExisteMateriales(ClsMateriales Material) {

		/**
		 * Variable que determina el resultado
		 */
		boolean retorno = false;

		/**
		 * comprobacion a traves del metodo Equals
		 */
		for (ClsMateriales b : MiListaDeMateriales) {
			if (b.equals(Material))
				/**
				 * si se repiten devuelve true
				 */
				return true;

		}

		return retorno;
	}

	public boolean ExisteEnvios(ClsEnvios Envio) {

		/**
		 * Variable que determina el resultado
		 */
		boolean retorno = false;

		/**
		 * comprobacion a traves del metodo Equals
		 */
		for (ClsEnvios b : MiListaDeEnvios) {
			if (b.equals(Envio))
				/**
				 * si se repiten devuelve true
				 */
				return true;

		}

		return retorno;
	}

	public boolean ExisteClientes(ClsClientes Cliente) {

		/**
		 * Variable que determina el resultado
		 */
		boolean retorno = false;

		/**
		 * comprobacion a traves del metodo Equals
		 */
		for (ClsClientes b : MiListaDeClientes) {
			if (b.equals(Cliente))
				/**
				 * si se repiten devuelve true
				 */
				return true;

		}

		return retorno;
	}

	public boolean ExistePedidos(ClsPedidos pedidos) {

		/**
		 * Variable que determina el resultado
		 */
		boolean retorno = false;

		/**
		 * comprobacion a traves del metodo Equals
		 */
		for (ClsPedidos b : MiListaDePedidos) {
			if (b.equals(pedidos))
				/**
				 * si se repiten devuelve true
				 */
				return true;

		}

		return retorno;
	}

	public boolean ExisteArticulos(ClsArticulos articulo) {

		/**
		 * Variable que determina el resultado
		 */
		boolean retorno = false;

		/**
		 * comprobacion a traves del metodo Equals
		 */
		for (ClsArticulos b : MiListaDeArticulos) {
			if (b.equals(articulo))
				/**
				 * si se repiten devuelve true
				 */
				return true;

		}

		return retorno;
	}

	public boolean ExisteDesglose(ClsDesgloseDePedido desglose) {
		/**
		 * Variable que determina el resultado
		 */
		boolean retorno = false;
		/**
		 * comprobaci�n a trav�s del m�todo equals
		 */
		for (ClsDesgloseDePedido b : MiListaDeDesgloses) {
			if (b.equals(desglose))
				/**
				 * si se repiten devuelve true
				 */
				return true;
		}

		return retorno;
	}

	public ArrayList<ItfProperty> OrdenaSeries() {

		/**
		 * Objeto comparador
		 */
		comparador.ClsComparadorSeriesID comp = new ClsComparadorSeriesID();

		/**
		 * funcion de ordenamiento (ArrayList, Patron)
		 */
		Collections.sort(MiListaDeSeries, comp);

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * pasarlas a ClsMostrarDatos para verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * compiamos un array en el otro ya ordenado
		 */
		for (ClsSeries a : MiListaDeSeries) {
			retorno.add(a);
		}

		return retorno;

	}

	/**
	 * Oredena el Array de Herrajes por IDs
	 * 
	 * @return devuelve el array ordenado
	 */
	public ArrayList<ItfProperty> OrdenarTapas() {
		/**
		 * Objeto comparador
		 */
		ClsComparadorTapasID comp = new ClsComparadorTapasID();

		/**
		 * funcion de ordenamiento (ArrayList, Patron)
		 */
		Collections.sort(MiListaDeTapas, comp);

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * pasarlas a ClsMostrarDatos para verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * compiamos un array en el otro ya ordenado
		 */
		for (ClsTapas a : MiListaDeTapas) {
			retorno.add(a);
		}

		return retorno;
	}

	/**
	 * Ordenar el array de Materiales por ID
	 * 
	 * @return nos lo devuelve ordenado
	 */
	public ArrayList<ItfProperty> OrdenarMateriales() {

		/**
		 * Objeto comparador
		 */
		ClsComparadorMaterialesID comp = new ClsComparadorMaterialesID();

		/**
		 * funcion de ordenamiento (ArrayList, Patron)
		 */
		Collections.sort(MiListaDeMateriales, comp);

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * pasarlas a ClsMostrarDatos para verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * compiamos un array en el otro ya ordenado
		 */
		for (ClsMateriales a : MiListaDeMateriales) {
			retorno.add(a);
		}

		return retorno;
	}

	/**
	 * Metodo para ordenar suelas por ID
	 * 
	 * @return devuelve array ordenado
	 */
	public ArrayList<ItfProperty> OrdenarTipoHoja() {

		/**
		 * Objeto comparador
		 */
		ClsComparadorTipoHojaID comp = new ClsComparadorTipoHojaID();

		/**
		 * funcion de ordenamiento (ArrayList, Patron)
		 */
		Collections.sort(MiListaDeHojas, comp);

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * pasarlas a ClsMostrarDatos para verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * compiamos un array en el otro ya ordenado
		 */
		for (ClsTipoHoja a : MiListaDeHojas) {
			retorno.add(a);
		}

		return retorno;
	}

	/**
	 * Metodo para ordenar array de envios por ID
	 * 
	 * @return nos lo devuelve ordenado
	 */
	public ArrayList<ItfProperty> OrdenarEnvios() {

		/**
		 * Objeto comparador
		 */
		clsComparadorEnvio comp = new clsComparadorEnvio();

		/**
		 * Funcion de ordenamiento (ArrayList, Patron)
		 */
		Collections.sort(MiListaDeEnvios, comp);

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * pasarlas a ClsMostrarDatos para verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * compiamos un array en el otro ya ordenado
		 */
		for (ClsEnvios a : MiListaDeEnvios) {
			retorno.add(a);
		}

		return retorno;
	}

	/**
	 * Metodo para ordenar array de envios por ID
	 * 
	 * @return nos lo devuelve ordenado
	 */
	public ArrayList<ItfProperty> OrdenarPedidos() {

		/**
		 * Objeto comparador
		 */
		ClsComparadorPedidosID comp = new ClsComparadorPedidosID();

		/**
		 * Funcion de ordenamiento (ArrayList, Patron)
		 */
		Collections.sort(MiListaDePedidos, comp);

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * pasarlas a ClsMostrarDatos para verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * compiamos un array en el otro ya ordenado
		 */
		for (ClsPedidos a : MiListaDePedidos) {
			retorno.add(a);
		}

		return retorno;
	}

	/**
	 * Metodo para ordenar array de desgloses por ID
	 * 
	 * @return nos lo devuelve ordenado
	 */
	public ArrayList<ItfProperty> OrdenarDesgloses() {

		/**
		 * Objeto comparador
		 */
		ClsComparadorDesglosesID comp = new ClsComparadorDesglosesID();

		/**
		 * Funcion de ordenamiento (ArrayList, Patron)
		 */
		Collections.sort(MiListaDeDesgloses, comp);

		/**
		 * Generamos ArrayList De tipo ITF para recuperar las propiedades del objeto y
		 * pasarlas a ClsMostrarDatos para verlos por pantalla
		 */
		ArrayList<ItfProperty> retorno;
		retorno = new ArrayList<ItfProperty>();

		/**
		 * compiamos un array en el otro ya ordenado
		 */
		for (ClsDesgloseDePedido a : MiListaDeDesgloses) {
			retorno.add(a);
		}

		return retorno;
	}

	/**
	 * Metodo para borrar objetos Serie del Array y de la BD.
	 * 
	 * @param NDeSerie parametro para seleccionar que borrar.
	 * @throws SQLException lanzamos la excepcion.
	 * @return nos genera un return para saber si se ha realizado el borrado o no
	 * @throws ClsBorrarExcepcion excepcion en caso de que falle el borrado
	 */
	public boolean EliminarSeriesDeArray(int NDeSerie) throws SQLException, ClsBorrarExcepcion {

		/**
		 * variable para saber si se ha hecho el borrado o no
		 */
		boolean hecho = true;

		/**
		 * Variables para buscar la posicion de objeto en el array
		 */
		int index = -1;
		int bound = MiListaDeSeries.size();
		/**
		 * miramos en que posicion de Array se encuentra nuestro objeto buscado
		 */
		for (int userInd = 0; userInd < bound; userInd++) {
			if (MiListaDeSeries.get(userInd).getIntegerProperty(clsConstantes.PROPIEDAD_SERIES_NUMERO_DE_SERIE)
					.equals(NDeSerie)) {
				index = userInd;
				break;
			}

		}

		/**
		 * si encontramos posicion del objeto en el array borramos si no devolvemos
		 * false
		 */
		if (index == -1) {
			hecho = false;
			throw new ClsBorrarExcepcion();
		} else {
			/**
			 * borramos del array
			 */
			MiListaDeSeries.remove(index);
			/**
			 * mandamos borrar de la BD.
			 */
			objDatos.conectarBD();
			objDatos.eliminarSeries(NDeSerie);
			objDatos.desconectarBD();
		}

		return hecho;
	}

	/**
	 * Metodo para eliminar Tipos de Hoja por referencia de los Arrays y de BD.
	 * 
	 * @param Referencia parametro para buscar.
	 * @throws SQLException       lanzamos la excepcion a LP.
	 * @throws ClsBorrarExcepcion excepcion para el borrado
	 * @return nos dice si se ha eliminado o no.
	 */
	public boolean EliminarTiposDeArray(int Referencia) throws SQLException, ClsBorrarExcepcion {

		/**
		 * variable para saber si se ha hecho el borrado o no
		 */
		boolean hecho = true;

		/**
		 * Variables para buscar la posicion de objeto en el array
		 */
		int index = -1;
		int bound = MiListaDeHojas.size();
		/**
		 * miramos en que posicion de Array se encuentra nuestro objeto buscado
		 */
		for (int userInd = 0; userInd < bound; userInd++) {
			if (MiListaDeHojas.get(userInd).getIntegerProperty(clsConstantes.PROPIEDAD_TIPOHOJA_REFERENCIA)
					.equals(Referencia)) {
				index = userInd;
				break;
			}

		}

		/**
		 * si encontramos posicion del objeto en el array borramos si no devolvemos
		 * false
		 */
		if (index == -1) {
			hecho = false;
			throw new ClsBorrarExcepcion();
		} else {

			/**
			 * borramos del array
			 */
			MiListaDeHojas.remove(index);
			/**
			 * mandamos borrar de la BD.
			 */
			objDatos.conectarBD();
			objDatos.eliminarTiposHoja(Referencia);
			objDatos.desconectarBD();
		}

		return hecho;

	}

	/**
	 * Metodo para eliminar Tapas por referencia de los Arrays y de BD.
	 * 
	 * @param Referencia parametro para buscar.
	 * @throws SQLException       lanzamos la excepcion a LP.
	 * @throws ClsBorrarExcepcion excepcion para el borrado
	 * @return nos dice si se ha eliminado o no.
	 */
	public boolean EliminarTapasDeArray(int Referencia) throws SQLException, ClsBorrarExcepcion {

		/**
		 * variable para saber si se ha hecho el borrado o no
		 */
		boolean hecho = true;

		/**
		 * Variables para buscar la posicion de objeto en el array
		 */
		int index = -1;
		int bound = MiListaDeTapas.size();
		/**
		 * miramos en que posicion de Array se encuentra nuestro objeto buscado
		 */
		for (int userInd = 0; userInd < bound; userInd++) {
			if (MiListaDeTapas.get(userInd).getIntegerProperty(clsConstantes.PROPIEDAD_TAPAS_REFERENCIA)
					.equals(Referencia)) {
				index = userInd;
				break;
			}

		}

		/**
		 * si encontramos posicion del objeto en el array borramos si no devolvemos
		 * false
		 */
		if (index == -1) {
			hecho = false;
			throw new ClsBorrarExcepcion();
		} else {

			/**
			 * borramos del array
			 */
			MiListaDeTapas.remove(index);
			/**
			 * mandamos borrar de la BD.
			 */
			objDatos.conectarBD();
			objDatos.eliminarTapas(Referencia);
			objDatos.desconectarBD();
		}

		return hecho;

	}

	/**
	 * Metopo para eliminar cliente de Array y BD
	 * 
	 * @param NCliente parametro de eliminacion
	 * @throws SQLException       lanzamos excepcion
	 * @throws ClsBorrarExcepcion excepcion de borrado.
	 * @return nos dice si se ha eliminado o no.
	 */
	public boolean EliminarClientesDeArray(int NCliente) throws SQLException, ClsBorrarExcepcion {

		/**
		 * variable para saber si se ha hecho el borrado o no
		 */
		boolean hecho = true;

		/**
		 * Variables para buscar la posicion de objeto en el array
		 */
		int index = -1;
		int bound = MiListaDeClientes.size();
		/**
		 * miramos en que posicion de Array se encuentra nuestro objeto buscado
		 */
		for (int userInd = 0; userInd < bound; userInd++) {
			if (MiListaDeClientes.get(userInd).getIntegerProperty(clsConstantes.PROPIEDAD_CLIENTE_NUMERO)
					.equals(NCliente)) {
				index = userInd;
				break;
			}

		}

		/**
		 * si encontramos posicion del objeto en el array borramos si no devolvemos
		 * false
		 */
		if (index == -1) {
			hecho = false;
			throw new ClsBorrarExcepcion();
		} else {

			/**
			 * borramos del array
			 */
			MiListaDeClientes.remove(index);
			/**
			 * mandamos borrar de la BD.
			 */
			objDatos.conectarBD();
			objDatos.eliminarClientes(NCliente);
			objDatos.desconectarBD();
		}

		return hecho;

	}

	/**
	 * Metopo para eliminar envios de Array y BD
	 * 
	 * @param NEnvio parametro por el cual borrar.
	 * @throws SQLException       lanzamos excepcion
	 * @throws ClsBorrarExcepcion excepcion para el borrado
	 * @return nos devuelve si lo ha hecho o no.
	 */
	public boolean EliminarEnviosDeArray(int NEnvio) throws SQLException, ClsBorrarExcepcion {

		/**
		 * variable para saber si se ha hecho el borrado o no
		 */
		boolean hecho = true;

		/**
		 * Variables para buscar la posicion de objeto en el array
		 */
		int index = -1;
		int bound = MiListaDeEnvios.size();
		/**
		 * miramos en que posicion de Array se encuentra nuestro objeto buscado
		 */
		for (int userInd = 0; userInd < bound; userInd++) {
			if (MiListaDeEnvios.get(userInd).getIntegerProperty(clsConstantes.PROPIEDAD_ENVIOS_NUMERO_DE_ENVIO)
					.equals(NEnvio)) {
				index = userInd;
				break;
			}

		}

		/**
		 * si encontramos posicion del objeto en el array borramos si no devolvemos
		 * false
		 */
		if (index == -1) {
			hecho = false;
			throw new ClsBorrarExcepcion();
		} else {

			/**
			 * borramos del array
			 */
			MiListaDeEnvios.remove(index);
			/**
			 * mandamos borrar de la BD.
			 */
			objDatos.conectarBD();
			objDatos.eliminarEnvios(NEnvio);
			objDatos.desconectarBD();
		}

		return hecho;

	}

	/**
	 * Metodo para eliminar pedidos de array y bd
	 * 
	 * @param NPedido parametro por el cual borrar
	 * @return nos dice si se ha hecho o no
	 * @throws SQLException       lanza excepcion de bd
	 * @throws ClsBorrarExcepcion lanza excepcion de borrado
	 */
	public boolean EliminarPedidosDeArray(int NPedido) throws SQLException, ClsBorrarExcepcion {

		/**
		 * variable para saber si se ha hecho el borrado o no
		 */
		boolean hecho = true;

		/**
		 * Variables para buscar la posicion de objeto en el array
		 */
		int index = -1;
		int bound = MiListaDePedidos.size();
		/**
		 * miramos en que posicion de Array se encuentra nuestro objeto buscado
		 */
		for (int userInd = 0; userInd < bound; userInd++) {
			if (MiListaDePedidos.get(userInd).getIntegerProperty(clsConstantes.PROPIEDAD_PEDIDOS_NUMERO_DE_PEDIDO)
					.equals(NPedido)) {
				index = userInd;
				break;
			}

		}

		/**
		 * si encontramos posicion del objeto en el array borramos si no devolvemos
		 * false
		 */
		if (index == -1) {
			hecho = false;
			throw new ClsBorrarExcepcion();
		} else {

			/**
			 * borramos del array
			 */
			MiListaDePedidos.remove(index);
			/**
			 * mandamos borrar de la BD.
			 */
			objDatos.conectarBD();
			objDatos.eliminarPedidos(NPedido);
			objDatos.desconectarBD();
		}

		return hecho;

	}

	/**
	 * Metodo para eliminar Articulos de Array y de BD
	 * 
	 * @param Referencia parametro por el cual borrar
	 * @return genera un return
	 * @throws SQLException       lanza excepcion de BD
	 * @throws ClsBorrarExcepcion lanza excepcion si no se ha borrado
	 */
	public boolean EliminarArticulosDeArray(int Referencia) throws SQLException, ClsBorrarExcepcion {

		/**
		 * variable para saber si se ha hecho el borrado o no
		 */
		boolean hecho = true;

		/**
		 * Variables para buscar la posicion de objeto en el array
		 */
		int index = -1;
		int bound = MiListaDeArticulos.size();
		/**
		 * miramos en que posicion de Array se encuentra nuestro objeto buscado
		 */
		for (int userInd = 0; userInd < bound; userInd++) {
			if (MiListaDeArticulos.get(userInd).getIntegerProperty(clsConstantes.PROPIEDAD_ARTICULO_REFERENCIA)
					.equals(Referencia)) {
				index = userInd;
				break;
			}

		}

		/**
		 * si encontramos posicion del objeto en el array borramos si no devolvemos
		 * false
		 */
		if (index == -1) {
			hecho = false;
			throw new ClsBorrarExcepcion();
		} else {
			/**
			 * borramos del array
			 */
			MiListaDeArticulos.remove(index);
			/**
			 * mandamos borrar de la BD.
			 */
			objDatos.conectarBD();
			objDatos.eliminarArticulos(Referencia);
			objDatos.desconectarBD();
		}

		return hecho;
	}

	/**
	 * Metodo para eliminar materiales del array y de bd
	 * 
	 * @param Referencia parametro por el cual borrar
	 * @throws SQLException       lanzamos la excepcion a LP
	 * @throws ClsBorrarExcepcion excepcion por si no se realiza el borrado
	 * @return nos dice si se ha eliminado o no
	 */
	public boolean EliminarMaterialesDeArray(int Referencia) throws SQLException, ClsBorrarExcepcion {

		/**
		 * variable para saber si se ha hecho el borrado o no
		 */
		boolean hecho = true;

		/**
		 * Variables para buscar la posicion de objeto en el array
		 */
		int index = -1;
		int bound = MiListaDeMateriales.size();
		/**
		 * miramos en que posicion de Array se encuentra nuestro objeto buscado
		 */
		for (int userInd = 0; userInd < bound; userInd++) {
			if (MiListaDeMateriales.get(userInd).getIntegerProperty(clsConstantes.PROPIEDAD_MATERIALES_REFERENCIA)
					.equals(Referencia)) {
				index = userInd;
				break;
			}

		}

		/**
		 * si encontramos posicion del objeto en el array borramos si no devolvemos
		 * false
		 */
		if (index == -1) {
			hecho = false;
			throw new ClsBorrarExcepcion();
		} else {

			/**
			 * borramos del array
			 */
			MiListaDeMateriales.remove(index);
			/**
			 * mandamos borrar de la BD.
			 */
			objDatos.conectarBD();
			objDatos.eliminarMateriales(Referencia);
			objDatos.desconectarBD();
		}

		return hecho;

	}

	/**
	 * Metodo para eliminar Desgloses de Array y de BD
	 * 
	 * @param NPedidoD parametro por el cual borramos
	 * @return nos devuelve un resultado
	 * @throws SQLException       lanza la excepcion de BD
	 * @throws ClsBorrarExcepcion lanza excepcion si no se ha podido hacer el
	 *                            borrado
	 */
	public boolean EliminarDesglosesDeArray(int NPedidoD) throws SQLException, ClsBorrarExcepcion {

		/**
		 * variable para saber si se ha hecho el borrado o no
		 */
		boolean hecho = true;

		/**
		 * Variables para buscar la posicion de objeto en el array
		 */
		int index = -1;
		int bound = MiListaDeDesgloses.size();
		/**
		 * miramos en que posicion de Array se encuentra nuestro objeto buscado
		 */
		for (int userInd = 0; userInd < bound; userInd++) {
			if (MiListaDeDesgloses.get(userInd)
					.getIntegerProperty(clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_DESGLOSE)
					.equals(NPedidoD)) {
				index = userInd;
				break;
			}

		}

		/**
		 * si encontramos posicion del objeto en el array borramos si no devolvemos
		 * false
		 */
		if (index == -1) {
			hecho = false;
			throw new ClsBorrarExcepcion();
		} else {
			/**
			 * borramos del array
			 */
			MiListaDeDesgloses.remove(index);
			/**
			 * mandamos borrar de la BD.
			 */
			objDatos.conectarBD();
			objDatos.eliminarDesglose(NPedidoD);
			objDatos.desconectarBD();
		}

		return hecho;
	}

	/**
	 * Metodo para actualizar el estado de los pedidos
	 * 
	 * @param NPedido prametro por el cual se actualizan
	 * @return nos dice si se ha hecho o no
	 * @throws SQLException lanza excepcion de BD
	 */
	public boolean ActualizarEntregasDePedidos(int NPedido) throws SQLException {

		/**
		 * Variable para confirmar el proceso.
		 */
		boolean Hecho = false;
		/**
		 * variable a actualizar
		 */
		boolean Entregado = false;
		int NumeroDePedido = 0;
		Date FechaDePedido = null;
		Date FechaDeEntrega = null;
		int NumeroDeCliente_Pedidos = 0;
		String NombreYApelliosDelCliente = null;

		/**
		 * Variables para buscar la posicion de objeto en el array
		 */
		int index = -1;
		int bound = MiListaDePedidos.size();
		/**
		 * miramos en que posicion de Array se encuentra nuestro objeto buscado
		 */
		for (int userInd = 0; userInd < bound; userInd++) {
			if (MiListaDePedidos.get(userInd).getIntegerProperty(clsConstantes.PROPIEDAD_PEDIDOS_NUMERO_DE_PEDIDO)
					.equals(NPedido)) {
				index = userInd;
				break;
			}

		}

		/**
		 * si encontramos posicion del objeto en el array borramos si no devolvemos
		 * false
		 */
		if (index == -1) {
			Hecho = false;

		} else {
			Hecho = true;
			/**
			 * Obtenemos el objeto
			 */
			ClsPedidos PedidoActualizar = MiListaDePedidos.get(index);

			/**
			 * obtenemos los datos del objeto
			 */

			NumeroDePedido = PedidoActualizar.getNumeroDePedido();
			FechaDePedido = PedidoActualizar.getFechaDePedido();
			FechaDeEntrega = PedidoActualizar.getFechaDeEntrega();
			Entregado = PedidoActualizar.isEntregado();
			NumeroDeCliente_Pedidos = PedidoActualizar.getNumeroDeCliente_Pedidos();
			NombreYApelliosDelCliente = PedidoActualizar.getNombreYApelliosDelCliente();

			/**
			 * actualizamos el estado
			 */
			if (Entregado == false) {
				Entregado = true;
			} else {
				Entregado = false;
			}

			/**
			 * quitamos el objeto anterior del array
			 */
			MiListaDePedidos.remove(index);
			/**
			 * creamos objeto actualizado y lo a�adimos al array
			 */
			ClsPedidos PedidoActualizado = new ClsPedidos(NumeroDePedido, FechaDePedido, FechaDeEntrega, Entregado,
					NombreYApelliosDelCliente, NumeroDeCliente_Pedidos);
			MiListaDePedidos.add(PedidoActualizado);
			/**
			 * Mantamos a actualizar en BD
			 */
			objDatos.conectarBD();
			objDatos.ActualizarPedidos(NumeroDePedido, FechaDePedido, FechaDeEntrega, Entregado,
					NombreYApelliosDelCliente, NumeroDeCliente_Pedidos);
			objDatos.desconectarBD();

		}

		return Hecho;
	}

}
