package ln;

import static comun.clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_DESGLOSE;
import static comun.clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_PEDIDO;
import static comun.clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_REFERENCIA_DEL_ARTICULO;
import static comun.clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_SERIE;
import static comun.clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_TAMA�O;
import static comun.clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_TIPO;

import java.util.Date;

import comun.ItfProperty;
import excepciones.clsRunTimeExcepcion;

/**
 * @author Aitor Ubierna Esta clase recoge y crea los objetos para la entidad
 *         Desglose De Pedido.
 */
public class ClsDesgloseDePedido implements ItfProperty {

	/**
	 * Atributo para recoger el numero de desglose
	 */
	private int NumeroDePedido;
	/**
	 * Atributo para recoger la referencia de articulo (Herencia de la tabla
	 * articulos)
	 */
	private int ReferenciaDelArticulo;
	/**
	 * Atributo para recoger el numero de la serie
	 */
	private int Serie;

	/**
	 * Atributo para recoger el tipo de libro
	 */
	private String Tipo;

	/**
	 * Atributo para recoger el tama�o del libro
	 */
	private String Tama�o;

	/**
	 * Atributo para recoger el numero de pedido (Herencia de la tabla pedidos)
	 */
	private int Pedidos_NPedido_Desglose;

	/**
	 * Este es el constructor de la clase Desglose De Pedidos.
	 * 
	 * @param numeroDePedido           parametro numero de pedido
	 * @param referenciaDelArticulo    parametro numero de referencia del articulo
	 * @param serie                    parametro numero de serie
	 * @param tipo                     parametro del tipo de libro.
	 * @param tama�o                   parametro del tama�o de libro.
	 * @param pedidos_NPedido_Desglose parametro del numero del cliente.
	 */
	public ClsDesgloseDePedido(int numeroDePedido, int referenciaDelArticulo, int serie, String tipo, String tama�o,
			int pedidos_NPedido_Desglose) {
		NumeroDePedido = numeroDePedido;
		ReferenciaDelArticulo = referenciaDelArticulo;
		Serie = serie;
		Tipo = tipo;
		Tama�o = tama�o;
		Pedidos_NPedido_Desglose = pedidos_NPedido_Desglose;
	}

	/**
	 * Estos son los metodos getters y setters.
	 * 
	 * @return nos genera unos returns
	 */

	public int getNumeroDePedido() {
		return NumeroDePedido;
	}

	public void setNumeroDePedido(int numeroDePedido) {
		NumeroDePedido = numeroDePedido;
	}

	public int getReferenciaDelArticulo() {
		return ReferenciaDelArticulo;
	}

	public void setReferenciaDelArticulo(int referenciaDelArticulo) {
		ReferenciaDelArticulo = referenciaDelArticulo;
	}

	public int getSerie() {
		return Serie;
	}

	public void setSerie(int serie) {
		Serie = serie;
	}

	public String getTipo() {
		return Tipo;
	}

	public void setTipo(String tipo) {
		Tipo = tipo;
	}

	public String getTama�o() {
		return Tama�o;
	}

	public void setTama�o(String tama�o) {
		Tama�o = tama�o;
	}

	public int getPedidos_NPedido_Desglose() {
		return Pedidos_NPedido_Desglose;
	}

	public void setPedidos_NPedido_Desglose(int pedidos_NPedido_Desglose) {
		Pedidos_NPedido_Desglose = pedidos_NPedido_Desglose;
	}

	/**
	 * Implementaci�n itfProperty
	 */
	@Override
	public String getStringProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_DESGLOSE_DE_PEDIDO_TIPO:
			return this.getTipo();
		case PROPIEDAD_DESGLOSE_DE_PEDIDO_TAMA�O:
			return this.getTama�o();
		default:
			throw new clsRunTimeExcepcion(propiedad);

		}

	}

	@Override
	public Integer getIntegerProperty(String propiedad) {
		switch (propiedad) {
		case PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_PEDIDO:
			return this.getNumeroDePedido();
		case PROPIEDAD_DESGLOSE_DE_PEDIDO_REFERENCIA_DEL_ARTICULO:
			return this.getReferenciaDelArticulo();
		case PROPIEDAD_DESGLOSE_DE_PEDIDO_SERIE:
			return this.getSerie();
		case PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_DESGLOSE:
			return this.getPedidos_NPedido_Desglose();
		default:
			throw new clsRunTimeExcepcion(propiedad);
		}

	}

	@Override
	public Float getFloatProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDoubleProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public char getCharProperty(String propiedad) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Date getDateProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean getBooleanProperty(String propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

}
