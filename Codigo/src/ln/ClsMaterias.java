package ln;

import java.util.Date;



/**
 * @author Aitor Ubierna Esta clase nos da los atributos padre para las
 *         herencias de la clase "TiposMateriales".
 * 
 */
public class ClsMaterias {

	/**
	 * Atributo comun para TAPAS, MATERIALES Y TIPOHOJA para recoger su Referencia
	 */
	private int Referencia;
	/**
	 * Atributo comun para TAPAS, MATERIALES Y TIPOHOJA para recoger la Descripcion
	 */
	private String Descripcion;
	/**
	 * Atributo comun para TAPAS, MATERIALES Y TIPOHOJA para recoger el Preccio
	 */
	private double Precio;

	/**
	 * 
	 * Este seria el constructor de la clase.
	 * 
	 * @param referencia  prametro referencia
	 * @param descripcion parametro descripcion
	 * @param precio      parametro precio
	 * 
	 */
	public ClsMaterias(int referencia, String descripcion, double precio) {
		super();
		Referencia = referencia;
		Descripcion = descripcion;
		Precio = precio;
	}

	/**
	 * 
	 * A partir de aqui tendriamos los metodos getters y setters.
	 * 
	 * @return nos genera un return
	 * 
	 */

	public int getReferencia() {
		return Referencia;
	}

	public void setReferencia(int referencia) {
		Referencia = referencia;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	public double getPrecio() {
		return Precio;
	}

	public void setPrecio(double precio) {
		Precio = precio;
	}

}
