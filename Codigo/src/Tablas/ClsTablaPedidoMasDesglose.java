package Tablas;

import static comun.clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_PEDIDO;
import static comun.clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_REFERENCIA_DEL_ARTICULO;
import static comun.clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_SERIE;
import static comun.clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_TIPO;
import static comun.clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_TAMA�O;
import static comun.clsConstantes.PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_DESGLOSE;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import comun.ItfProperty;
import ln.ClsGestorLN;

public class ClsTablaPedidoMasDesglose extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	/**
	 * Array Desgloses
	 */
	ArrayList<ItfProperty> DesglosesRecuperados;

	private String[] NombreColumnas = { "N� de Pedido", "Referencia", "Serie", "Tipo", "Tama�o", "N� de Desglose" };
	Object[][] data;

	/**
	 * Metodo para crear la tabla
	 * 
	 * @param Desgloses recibe parametro
	 */
	public ClsTablaPedidoMasDesglose(ArrayList<ItfProperty> Desgloses) {
		super();

		int filas = Desgloses.size();
		int cont;
		data = new Object[filas][];
		cont = 0;

		for (ItfProperty b : Desgloses) {
			Object[] a = { b.getIntegerProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_PEDIDO),
					b.getIntegerProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_REFERENCIA_DEL_ARTICULO),
					b.getIntegerProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_SERIE),
					b.getIntegerProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_SERIE),
					b.getStringProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_TIPO),
					b.getStringProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_TAMA�O),
					b.getIntegerProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_PEDIDO) };
			data[cont] = a;
			cont++;
		}
	}

	/**
	 * Metodo para actualizar la tabla
	 * 
	 * @param ObjGestor recibe parametro
	 */
	public void setData(ClsGestorLN ObjGestor) {
		DesglosesRecuperados = ObjGestor.ObtenerDesgloses();
		int filas = DesglosesRecuperados.size();
		int cont;
		data = new Object[filas][];
		cont = 0;

		for (ItfProperty b : DesglosesRecuperados) {
			Object[] a = { b.getIntegerProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_PEDIDO),
					b.getIntegerProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_REFERENCIA_DEL_ARTICULO),
					b.getIntegerProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_SERIE),
					b.getIntegerProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_SERIE),
					b.getStringProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_TIPO),
					b.getStringProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_TAMA�O),
					b.getIntegerProperty(PROPIEDAD_DESGLOSE_DE_PEDIDO_NUMERO_DE_PEDIDO), };
			data[cont] = a;
			cont++;
		}

	}

	/**
	 * Metodo para obtener la cantidad de las columnas
	 */
	public int getColumnCount() {
		return NombreColumnas.length;
	}

	/**
	 * Metodo para obetener el numero de filas
	 */
	public int getRowCount() {
		return data.length;
	}

	/**
	 * Metodo para obtener nombre de las columnas
	 */
	public String getColumnName(int col) {
		return NombreColumnas[col];
	}

	/**
	 * metodo para obtener los valores
	 */
	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	/*
	 * JTable uses this method to determine the default renderer/ editor for each
	 * cell. If we didn't implement this method, then the last column would contain
	 * text ("true"/"false"), rather than a check box.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	/*
	 * Don't need to implement this method unless your table's editable.
	 */
	public boolean isCellEditable(int row, int col) {

		return false;

	}

	/*
	 * Don't need to implement this method unless your table's data can change.
	 */
	public void setValueAt(Object value, int row, int col) {

		data[row][col] = value;
		fireTableCellUpdated(row, col);

	}

}
