package Tablas;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import static comun.clsConstantes.PROPIEDAD_TIPOHOJA_REFERENCIA;
import static comun.clsConstantes.PROPIEDAD_TIPOHOJA_DESCRIPCION;
import static comun.clsConstantes.PROPIEDAD_TIPOHOJA_PRECIO;
import comun.ItfProperty;
import ln.ClsGestorLN;

/**
 * Clase para formar la tabla tipos de Hoja
 * 
 * @author Aitor Ubierna
 *
 */
public class ClsTablaTiposHoja extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	/**
	 * Array tipos de hoja
	 */
	ArrayList<ItfProperty> TiposHojaRecuperados;

	private String[] NombreColumnas = { "Referencia", "Descripción", "Precio" };
	Object[][] data;

	/**
	 * Metodo para crear la tabla
	 * 
	 * @param TiposdeHoja recibe parametro
	 */
	public ClsTablaTiposHoja(ArrayList<ItfProperty> TiposdeHoja) {
		super();

		int filas = TiposdeHoja.size();
		int cont;
		data = new Object[filas][];
		cont = 0;

		for (ItfProperty b : TiposdeHoja) {
			Object[] a = { b.getIntegerProperty(PROPIEDAD_TIPOHOJA_REFERENCIA),
					b.getStringProperty(PROPIEDAD_TIPOHOJA_DESCRIPCION),
					b.getDoubleProperty(PROPIEDAD_TIPOHOJA_PRECIO), };
			data[cont] = a;
			cont++;
		}
	}

	/**
	 * Metodo para actualizar la tabla
	 * 
	 * @param ObjGestor recibe parametro
	 * @throws SQLException fallo bbdd
	 */
	public void setData(ClsGestorLN ObjGestor) throws SQLException {
		TiposHojaRecuperados = ObjGestor.ObtenerTiposHoja();
		int filas = TiposHojaRecuperados.size();
		int cont;
		data = new Object[filas][];
		cont = 0;

		for (ItfProperty b : TiposHojaRecuperados) {
			Object[] a = { b.getIntegerProperty(PROPIEDAD_TIPOHOJA_REFERENCIA),
					b.getStringProperty(PROPIEDAD_TIPOHOJA_DESCRIPCION),
					b.getDoubleProperty(PROPIEDAD_TIPOHOJA_PRECIO), };
			data[cont] = a;
			cont++;
		}

	}

	/**
	 * Metodo para obtener la cantidad de las columnas
	 */
	public int getColumnCount() {
		return NombreColumnas.length;
	}

	/**
	 * Metodo para obetener el numero de filas
	 */
	public int getRowCount() {
		return data.length;
	}

	/**
	 * Metodo para obtener nombre de las columnas
	 */
	public String getColumnName(int col) {
		return NombreColumnas[col];
	}

	/**
	 * metodo para obtener los valores
	 */
	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	/*
	 * JTable uses this method to determine the default renderer/ editor for each
	 * cell. If we didn't implement this method, then the last column would contain
	 * text ("true"/"false"), rather than a check box.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	/*
	 * Don't need to implement this method unless your table's editable.
	 */
	public boolean isCellEditable(int row, int col) {

		return false;

	}

	/*
	 * Don't need to implement this method unless your table's data can change.
	 */
	public void setValueAt(Object value, int row, int col) {

		data[row][col] = value;
		fireTableCellUpdated(row, col);

	}

}
