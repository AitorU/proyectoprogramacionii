package Tablas;

import static comun.clsConstantes.PROPIEDAD_ARTICULO_CANTIDAD_DE_MATERIAL;
import static comun.clsConstantes.PROPIEDAD_ARTICULO_DESCRIPCION;
import static comun.clsConstantes.PROPIEDAD_ARTICULO_PRECIO;
import static comun.clsConstantes.PROPIEDAD_ARTICULO_REFERENCIA;
import static comun.clsConstantes.PROPIEDAD_ARTICULO_SERIE;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import comun.ItfProperty;

/**
 * Clase para crear tablas de articulos
 * 
 * @author Aitor Ubierna
 *
 */
public class ClsTablaArticulos extends AbstractTableModel {
	
	private static final long serialVersionUID = 1L;

	private String[] NombreColumnas = { "Referencia", "N� Serie", "Descripcion", "Cantidad Material", "Precio"};
	Object[][] data;

	/**
	 * Metodo para crear la tabla
	 * 
	 * @param Articulos recibe parametro
	 */
	public ClsTablaArticulos(ArrayList<ItfProperty> Articulos) {
		super();

		int filas = Articulos.size();
		int cont;
		data = new Object[filas][];
		cont = 0;

		for (ItfProperty b : Articulos) {
			Object[] a = { b.getIntegerProperty(PROPIEDAD_ARTICULO_REFERENCIA),
					b.getIntegerProperty(PROPIEDAD_ARTICULO_SERIE), b.getStringProperty(PROPIEDAD_ARTICULO_DESCRIPCION),
					b.getIntegerProperty(PROPIEDAD_ARTICULO_CANTIDAD_DE_MATERIAL),
					b.getDoubleProperty(PROPIEDAD_ARTICULO_PRECIO)};
			data[cont] = a;
			cont++;
		}
	}

	/**
	 * Metodo para actualizar la tabla
	 * 
	 * @param Articulos recibe parametro
	 */
	public void setData(ArrayList<ItfProperty> Articulos) {
		int filas = Articulos.size();
		int cont;
		data = new Object[filas][];
		cont = 0;

		for (ItfProperty b : Articulos) {
			Object[] a = { b.getIntegerProperty(PROPIEDAD_ARTICULO_REFERENCIA),
					b.getIntegerProperty(PROPIEDAD_ARTICULO_SERIE), b.getStringProperty(PROPIEDAD_ARTICULO_DESCRIPCION),
					b.getIntegerProperty(PROPIEDAD_ARTICULO_CANTIDAD_DE_MATERIAL),
					b.getDoubleProperty(PROPIEDAD_ARTICULO_PRECIO)};
			data[cont] = a;
			cont++;
		}
	}

	/**
	 * Metodo para obtener la cantidad de las columnas
	 */
	public int getColumnCount() {
		return NombreColumnas.length;
	}

	/**
	 * Metodo para obetener el numero de filas
	 */
	public int getRowCount() {
		return data.length;
	}

	/**
	 * Metodo para obtener nombre de las columnas
	 */
	public String getColumnName(int col) {
		return NombreColumnas[col];
	}

	/**
	 * metodo para obtener los valores
	 */
	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	/*
	 * JTable uses this method to determine the default renderer/ editor for each
	 * cell. If we didn't implement this method, then the last column would contain
	 * text ("true"/"false"), rather than a check box.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	/*
	 * Don't need to implement this method unless your table's editable.
	 */
	public boolean isCellEditable(int row, int col) {

		return false;

	}

	/*
	 * Don't need to implement this method unless your table's data can change.
	 */
	public void setValueAt(Object value, int row, int col) {

		data[row][col] = value;
		fireTableCellUpdated(row, col);

	}

}
