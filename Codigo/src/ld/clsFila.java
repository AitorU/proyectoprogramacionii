package ld;

import java.util.HashMap;

import comun.itfData;

/**
 * Objeto que representa a una fila de una tabla de una base de datos.
 * @author javier.cerro
 *
 */
public class clsFila implements itfData
{
	
	private HashMap<String,Object> fila;
	
	public clsFila()
	{
		fila = new HashMap<String,Object>();
	}

	public void ponerColumna(String columna, Object  valor)
	{
		fila.put(columna,valor);
	}
	
	public Object getData(String columna) 
	{
		
		return this.fila.get(columna);
	}
	

}
