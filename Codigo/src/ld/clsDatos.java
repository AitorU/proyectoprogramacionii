package ld;

import static ld.clsConstantesBD.QUERY_PARA_UPDATE_ENTREGA_DE_PEDIDO;
import static ld.clsConstantesBD.QUERY_PARA_SELECT_SERIES;
import static ld.clsConstantesBD.CONTRASE�A_DE_LA_BD;
import static ld.clsConstantesBD.NOMBRE_DEL_USUARIO;
import static ld.clsConstantesBD.QUERY_PARA_DELETE_SERIES_POR_N�DESERIE;
import static ld.clsConstantesBD.QUERY_PARA_DELETE_TAPAS_POR_REFERENCIA;
import static ld.clsConstantesBD.QUERY_PARA_INSERTAR_SERIES;
import static ld.clsConstantesBD.QUERY_PARA_INSERTAR_TAPAS;
import static ld.clsConstantesBD.QUERY_PARA_SELECT_SERIES;
import static ld.clsConstantesBD.QUERY_PARA_SELECT_TAPAS;
import static ld.clsConstantesBD.QUERY_PARA_SELECT_MATERIALES;
import static ld.clsConstantesBD.QUERY_PARA_SELECT_ARTICULOS;
import static ld.clsConstantesBD.QUERY_PARA_SELECT_CLIENTES;
import static ld.clsConstantesBD.QUERY_PARA_SELECT_DESGLOSE_DE_PEDIDOS;
import static ld.clsConstantesBD.QUERY_PARA_SELECT_ENVIOS;
import static ld.clsConstantesBD.QUERY_PARA_SELECT_PEDIDOS;
import static ld.clsConstantesBD.QUERY_PARA_SELECT_TIPOS_DE_HOJA;
import static ld.clsConstantesBD.RUTA_DE_LA_BD;

/**
 * Clase para hacer la conexi�n con la Base de Datos.
 * @author Aitor Ubierna
 *
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import comun.clsConstantes;
import comun.itfData;

public class clsDatos {

	/**
	 * Objeto para crear conexion a BD
	 */
	Connection objConn = null;

	/**
	 * Objeto para crear la consulta a base de datos.
	 */
	PreparedStatement objSt = null;
	/**
	 * Objeto para devolver el resultado de la consulta.
	 */
	ResultSet rs = null;

	/**
	 * Objeto para preparar consultas
	 */
	Statement st = null;

	/**
	 * Constructor vacio de ClsDatos.
	 */
	public clsDatos() {
	}

	/**
	 * Para generar la conexion con BD
	 * 
	 * @throws SQLException lanzamos excepciones a la capa LP
	 */
	public void conectarBD() throws SQLException {

		/**
		 * Realizamos la conexion
		 */
		objConn = DriverManager.getConnection(RUTA_DE_LA_BD, NOMBRE_DEL_USUARIO, CONTRASE�A_DE_LA_BD);

	}

	/**
	 * Metodo para cerrar la conexion con BD
	 * 
	 * @throws SQLException Lanza posible excepcion
	 */
	public void desconectarBD() throws SQLException {
		objConn.close();
	}

	/**
	 * Para hacer insert de Series
	 * 
	 * @param NDeSerie    parametro para insert
	 * @param Descripcion parametro para insert
	 * @throws SQLException lanzamos excepciones a la capa LP
	 */
	public void InsertarSerie(int NDeSerie, String Descripcion) throws SQLException {

		/**
		 * Insertamos Series
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 */
			objSt = objConn.prepareStatement(QUERY_PARA_INSERTAR_SERIES);
			/**
			 * Datos a insertar
			 */
			objSt.setInt(1, NDeSerie);
			objSt.setString(2, Descripcion);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}
	}

	/**
	 * Para consultar Series.
	 * 
	 * @throws SQLException lanzamos excepciones hacia la capa LP
	 * @return nos devuelve la lista con los datos.
	 */
	public ResultSet consultarSeries() throws SQLException {
		/**
		 * Consultamos Series
		 */
		if (objConn != null) {
			/**
			 * Preparamos la consulta
			 */
			st = objConn.createStatement();
			/**
			 * Proceso de recuperacion de datos
			 */
			rs = st.executeQuery(QUERY_PARA_SELECT_SERIES);
			if (rs.isBeforeFirst()) {
				return rs;
			}
			while (rs.next()) {

				return rs;

			}

			/**
			 * Cerramos el resulset
			 * 
			 */
			rs.close();
		}

		return null;

	}

	/**
	 * Para hacer delete en Series
	 * 
	 * @param NDeSerie parametro de condicion.
	 * @throws SQLException lanzamos excepciones hacia la capa LP
	 */
	public void eliminarSeries(int NDeSerie) throws SQLException {

		/**
		 * Eliminamos Series
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 */
			objSt = objConn.prepareStatement(QUERY_PARA_DELETE_SERIES_POR_N�DESERIE);
			/**
			 * Dato por el cual borramos
			 */
			objSt.setInt(1, NDeSerie);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Metodo para insertar tapas
	 * 
	 * @param Referencia  parametro de tapas
	 * @param Descripcion parametro de tapas
	 * @param Precio      parametro de tapas
	 * @throws SQLException lanzamos excepciones
	 */
	public void InsertarTapas(int Referencia, String Descripcion, Double Precio) throws SQLException {

		/**
		 * Insertamos Tapas
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 */
			objSt = objConn.prepareStatement(QUERY_PARA_INSERTAR_TAPAS);
			/**
			 * Datos a guardar
			 */
			objSt.setInt(1, Referencia);
			objSt.setString(2, Descripcion);
			objSt.setDouble(3, Precio);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Para consultar Tapas.
	 * 
	 * @throws SQLException lanzamos excepciones hacia la capa LP
	 * @return nos devuelve la lista con los datos.
	 */
	public ResultSet consultarTapas() throws SQLException {

		/**
		 * Consultamos Tapas
		 */

		if (objConn != null) {
			/**
			 * Preparamos la consulta
			 */
			st = objConn.createStatement();
			/**
			 * Proceso de recuperacion de datos
			 */
			rs = st.executeQuery(QUERY_PARA_SELECT_TAPAS);
			if (rs.isBeforeFirst()) {
				return rs;
			}
			while (rs.next()) {

				return rs;

			}

			/**
			 * Cerramos el resulset
			 * 
			 */
			rs.close();
		}

		return null;
	}

	/**
	 * Para eliminar tapas por numero de serie
	 * 
	 * @param NDeSerie parametro por el cual eliminar
	 * @throws SQLException lanzamos excepcion
	 */
	public void eliminarTapas(int NDeSerie) throws SQLException {

		/**
		 * Eliminamos Tapas
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 */
			objSt = objConn.prepareStatement(QUERY_PARA_DELETE_TAPAS_POR_REFERENCIA);
			/**
			 * Dato por el cual borramos
			 */
			objSt.setInt(1, NDeSerie);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * para insertar materiales
	 * 
	 * @param Referencia  parametro a insertar
	 * @param Descripcion parametro a insertar
	 * @param Precio      parametro a insertar
	 * @throws SQLException lanzamos excepciones
	 */
	public void InsertarMateriales(int Referencia, String Descripcion, Double Precio) throws SQLException {

		/**
		 * Insertamos Materiales
		 */
		if (objConn != null) {

			// Creamos las preparedstaments
			objSt = objConn.prepareStatement(clsConstantesBD.QUERY_PARA_INSERTAR_MATERIALES);
			/**
			 * Datos a insertar
			 */
			objSt.setInt(1, Referencia);
			objSt.setString(2, Descripcion);
			objSt.setDouble(3, Precio);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Para consultar materiales.
	 * 
	 * @throws SQLException lanzamos excepciones hacia la capa LP
	 * @return nos devuelve la lista con los datos.
	 */
	public ResultSet consultarMateriales() throws SQLException {

		/**
		 * Consultamos materiales
		 */
		if (objConn != null) {
			/**
			 * Preparamos la consulta
			 */
			st = objConn.createStatement();
			/**
			 * Proceso de recuperacion de datos
			 */
			rs = st.executeQuery(QUERY_PARA_SELECT_MATERIALES);
			if (rs.isBeforeFirst()) {
				return rs;
			}
			while (rs.next()) {

				return rs;

			}

			/**
			 * Cerramos el resulset
			 * 
			 */
			rs.close();
		}

		return null;
	}

	public void eliminarMateriales(int Referencia) throws SQLException {

		/**
		 * Eliminamos Materiales
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 */
			objSt = objConn.prepareStatement(clsConstantesBD.QUERY_PARA_DELETE_MATERIALES_POR_REFERENCIA);
			/**
			 * Dato por el cual borramos
			 */
			objSt.setInt(1, Referencia);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Metodo para insertar tipos de hoja
	 * 
	 * @param Referencia  parametro de tipos de hoja
	 * @param Descripcion parametro de tipo de hoja
	 * @param Precio      parametro de tipo de hoja
	 * @throws SQLException lanzamos excepciones
	 */
	public void InsertarTiposHoja(int Referencia, String Descripcion, Double Precio) throws SQLException {

		/**
		 * Insertamos el tipo de hoja
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 */
			objSt = objConn.prepareStatement(clsConstantesBD.QUERY_PARA_INSERTAR_TIPOS_DE_HOJA);
			/**
			 * Datos a insertar
			 */
			objSt.setInt(1, Referencia);
			objSt.setString(2, Descripcion);
			objSt.setDouble(3, Precio);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Para consultar tipos de hojas
	 * 
	 * @throws SQLException lanzamos excepciones hacia la capa LP
	 * @return nos devuelve la lista con los datos.
	 */
	public ResultSet consultarTiposHoja() throws SQLException {

		/**
		 * Consultamos los tipos de hoja
		 */
		if (objConn != null) {
			/**
			 * Preparamos la consulta
			 */
			st = objConn.createStatement();
			/**
			 * Proceso de recuperacion de datos
			 */
			rs = st.executeQuery(QUERY_PARA_SELECT_TIPOS_DE_HOJA);
			if (rs.isBeforeFirst()) {
				return rs;
			}
			while (rs.next()) {

				return rs;

			}

			/**
			 * Cerramos el resulset
			 * 
			 */
			rs.close();
		}

		return null;
	}

	/**
	 * Para eliminar tipos de hoja por numero de serie
	 * 
	 * @param NDeSerie parametro por el cual eliminar
	 * @throws SQLException lanzamos excepcion
	 */
	public void eliminarTiposHoja(int NDeSerie) throws SQLException {

		/**
		 * Eliminamos el tipo de hoja
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 */
			objSt = objConn.prepareStatement(clsConstantesBD.QUERY_PARA_DELETE_TIPOS_DE_HOJA_POR_REFERENCIA);
			/**
			 * Parametro por el cual borramos
			 */
			objSt.setInt(1, NDeSerie);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Metodo para insertar clientes en BD
	 * 
	 * @param NCliente           parametro recibido.
	 * @param NombreYApellidos   parametro recibido
	 * @param DNI_NIF            parametro recibido
	 * @param DireccionDeCliente parametro recibido
	 * @param Provincia          parametro recibido
	 * @param Telefono           parametro recibido
	 * @param Email              parametro recibido
	 * @throws SQLException lanzamos excepcion
	 */

	public void InsertarClientes(int NCliente, String NombreYApellidos, String DNI_NIF, String DireccionDeCliente,
			String Provincia, int Telefono, String Email) throws SQLException {

		/**
		 * Insertamos Clientes
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 * 
			 */
			objSt = objConn.prepareStatement(clsConstantesBD.QUERY_PARA_INSERTAR_CLIENTES);
			/**
			 * Datos a insertar
			 */
			objSt.setInt(1, NCliente);
			objSt.setString(2, NombreYApellidos);
			objSt.setString(3, DNI_NIF);
			objSt.setString(4, DireccionDeCliente);
			objSt.setString(5, Provincia);
			objSt.setInt(6, Telefono);
			objSt.setString(7, Email);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Para consultar clientes
	 * 
	 * @throws SQLException lanzamos excepciones hacia la capa LP
	 * @return nos devuelve la lista con los datos.
	 */
	public ResultSet consultarClientes() throws SQLException {

		/**
		 * Consultamos los clientes
		 */
		if (objConn != null) {
			/**
			 * Preparamos la consulta
			 */
			st = objConn.createStatement();
			/**
			 * Proceso de recuperacion de datos
			 */
			rs = st.executeQuery(QUERY_PARA_SELECT_CLIENTES);
			if (rs.isBeforeFirst()) {
				return rs;
			}
			while (rs.next()) {

				return rs;

			}

			/**
			 * Cerramos el resulset
			 * 
			 */
			rs.close();
		}
		return null;
	}

	/**
	 * Metodo para eliminar clientes de BD
	 * 
	 * @param NCliente parametro de eliminacion
	 * @throws SQLException lanzamos excepcion
	 */
	public void eliminarClientes(int NCliente) throws SQLException {

		/**
		 * Eliminamos Clientes.
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 */
			objSt = objConn.prepareStatement(clsConstantesBD.QUERY_PARA_DELETE_CLIENTES_POR_NUMERO_DE_CLIENTE);
			/**
			 * Dato por el cual borramos
			 */
			objSt.setInt(1, NCliente);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Metodo para insertar Envios en BD
	 * 
	 * @param NEnvio            parametro recibido
	 * @param NombreCliente     parametro recibido
	 * @param DireccionDeEnvio  parametro recibido
	 * @param PoblacionDeEnvio  parametro recibido
	 * @param CPDeEnvio         parametro recibido
	 * @param ProvinciaDeEnvio  parametro recibido
	 * @param TelefonoDeEnvio   parametro recibido
	 * @param Clientes_NCliente parametro recibido
	 * @throws SQLException lanzamos la excepcion
	 */
	public void InsertarEnvios(int NEnvio, String NombreCliente, String DireccionDeEnvio, String PoblacionDeEnvio,
			int CPDeEnvio, String ProvinciaDeEnvio, int TelefonoDeEnvio, int Clientes_NCliente) throws SQLException {

		/**
		 * Insertamos Envios
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 */
			objSt = objConn.prepareStatement(clsConstantesBD.QUERY_PARA_INSERTAR_ENVIOS);
			/**
			 * Datos a insertar
			 */
			objSt.setInt(1, NEnvio);
			objSt.setString(2, NombreCliente);
			objSt.setString(3, DireccionDeEnvio);
			objSt.setString(4, PoblacionDeEnvio);
			objSt.setInt(5, CPDeEnvio);
			objSt.setString(6, ProvinciaDeEnvio);
			objSt.setInt(7, TelefonoDeEnvio);
			objSt.setInt(8, Clientes_NCliente);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Para consultar envios
	 * 
	 * @throws SQLException lanzamos excepciones hacia la capa LP
	 * @return nos devuelve la lista con los datos.
	 */
	public ResultSet consultarEnvios() throws SQLException {

		/**
		 * Consultamos los envios
		 */
		if (objConn != null) {
			/**
			 * Preparamos la consulta
			 */
			st = objConn.createStatement();
			/**
			 * Proceso de recuperacion de datos
			 */
			rs = st.executeQuery(QUERY_PARA_SELECT_CLIENTES);
			if (rs.isBeforeFirst()) {
				return rs;
			}
			while (rs.next()) {

				return rs;

			}

			/**
			 * Cerramos el resulset
			 * 
			 */
			rs.close();
		}
		return null;
	}

	/**
	 * Metodo para eliminar Envios de BD
	 * 
	 * @param NEnvio parametro recibido
	 * @throws SQLException lazamos la excepcion
	 */
	public void eliminarEnvios(int NEnvio) throws SQLException {

		/**
		 * Eliminamos envio
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 */
			objSt = objConn.prepareStatement(clsConstantesBD.QUERY_PARA_DELETE_ENVIOS_POR_NUMERO_DE_ENVIO);
			/**
			 * Parametro por el cual borramos
			 */
			objSt.setInt(1, NEnvio);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Metodo para insertar pedidos en BD
	 * 
	 * @param NPedido           parametro recibido
	 * @param FechaDePedido     parametro recibido
	 * @param FechaDeEntrega    parametro recibido
	 * @param Entregado         parametro recibido
	 * @param Clientes_NCliente parametro recibido
	 * @param NombreYApellidos  parametro recibido
	 * @throws SQLException lanzamos excepcion.
	 */
	public void InsertarPedidos(int NPedido, Date FechaDePedido, Date FechaDeEntrega, Boolean Entregado,
			int Clientes_NCliente, String NombreYApellidos) throws SQLException {

		/**
		 * Traspaso de fechas de util.Date a long y despues a sql.Date
		 */
		java.sql.Date Fecha_de_pedido = new java.sql.Date(FechaDePedido.getTime());
		java.sql.Date Fecha_de_entrega = new java.sql.Date(FechaDeEntrega.getTime());

		/**
		 * Insertamos Pedidos.
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 */
			objSt = objConn.prepareStatement(clsConstantesBD.QUERY_PARA_INSERTAR_PEDIDOS);
			/**
			 * Datos a insertar
			 */
			objSt.setInt(1, NPedido);
			objSt.setDate(2, (java.sql.Date) Fecha_de_pedido);
			objSt.setDate(3, (java.sql.Date) Fecha_de_entrega);
			objSt.setBoolean(4, Entregado);
			objSt.setInt(5, Clientes_NCliente);
			objSt.setString(6, NombreYApellidos);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Para consultar pedidos
	 * 
	 * @throws SQLException lanzamos excepciones hacia la capa LP
	 * @return nos devuelve la lista con los datos.
	 */
	public ResultSet consultarPedidos() throws SQLException {

		/**
		 * Consultamos los pedidos
		 */
		if (objConn != null) {
			/**
			 * Preparamos la consulta
			 */
			st = objConn.createStatement();
			/**
			 * Proceso de recuperacion de datos
			 */
			rs = st.executeQuery(QUERY_PARA_SELECT_PEDIDOS);
			if (rs.isBeforeFirst()) {
				return rs;
			}
			while (rs.next()) {

				return rs;

			}

			/**
			 * Cerramos el resulset
			 * 
			 */
			rs.close();
		}
		return null;
	}

	/**
	 * Metodo para eliminar pedidos en la BD
	 * 
	 * @param NPedido parametro recibido
	 * @throws SQLException lanzamos excepcion
	 */
	public void eliminarPedidos(int NPedido) throws SQLException {

		/**
		 * Eliminamos Pedidos
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 * 
			 */
			objSt = objConn.prepareStatement(clsConstantesBD.QUERY_PARA_DELETE_PEDIDOS_POR_NUMERO_DE_PEDIDO);

			/**
			 * Parametro por el cual borramos
			 */
			objSt.setInt(1, NPedido);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Metodo para introducir Articulos en la BD
	 * 
	 * @param Referencia       parametro recibido
	 * @param Serie            parametro recibido
	 * @param Descripcion      parametro recibido
	 * @param CantidadMaterial parametro recibido
	 * @param Precio           parametro recibido
	 * @throws SQLException lanzamos excpcion.
	 */
	public void InsertarArticulos(int Referencia, int Serie, String Descripcion, int CantidadMaterial, double Precio)
			throws SQLException {

		/**
		 * Insertamos Articulos
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 * 
			 */
			objSt = objConn.prepareStatement(clsConstantesBD.QUERY_PARA_INSERTAR_ARTICULOS);
			/**
			 * Datos a insertar
			 */
			objSt.setInt(1, Referencia);
			objSt.setInt(2, Serie);
			objSt.setString(3, Descripcion);
			objSt.setInt(4, CantidadMaterial);
			objSt.setDouble(5, Precio);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Para consultar art�culos
	 * 
	 * @throws SQLException lanzamos excepciones hacia la capa LP
	 * @return nos devuelve la lista con los datos.
	 */
	public ResultSet consultarArticulos() throws SQLException {

		/**
		 * Consultamos los art�culos
		 */
		if (objConn != null) {
			/**
			 * Preparamos la consulta
			 */
			st = objConn.createStatement();
			/**
			 * Proceso de recuperacion de datos
			 */
			rs = st.executeQuery(QUERY_PARA_SELECT_CLIENTES);
			if (rs.isBeforeFirst()) {
				return rs;
			}
			while (rs.next()) {

				return rs;

			}

			/**
			 * Cerramos el resulset
			 * 
			 */
			rs.close();
		}
		return null;
	}

	/**
	 * Metodo para eliminar Articulos de la BD
	 * 
	 * @param Referencia parametro recibido
	 * @throws SQLException lanzamos expcion
	 */
	public void eliminarArticulos(int Referencia) throws SQLException {

		/**
		 * Eliminamos Articulo
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 */
			objSt = objConn.prepareStatement(clsConstantesBD.QUERY_PARA_DELETE_ARTICULOS_POR_REFERENCIA);
			/**
			 * Parametro por el cual borramos
			 */
			objSt.setInt(1, Referencia);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Metodo para insertar desgloses a la BD
	 * 
	 * @param Numero numero
	 * @param Referencia referencia
	 * @param Serie serie
	 * @param Tipo tipo
	 * @param Tamano tama�o
	 * @param NPedido numero pedido
	 * @throws SQLException fallo bbdd
	 */
	public void InsertarDesglose(int Numero, int Referencia, int Serie, String Tipo, String Tamano, int NPedido)
			throws SQLException {

		/**
		 * Insertamos el desglose
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 * 
			 */
			objSt = objConn.prepareStatement(clsConstantesBD.QUERY_PARA_INSERTAR_DE_DESGLOSE_DE_PEDIDO);
			/**
			 * Datos a insertar
			 */
			objSt.setInt(1, Numero);
			objSt.setInt(2, Referencia);
			objSt.setInt(3, Serie);
			objSt.setString(4, Tipo);
			objSt.setString(5, Tamano);
			objSt.setInt(5, NPedido);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Para consultar desgloses
	 * 
	 * @throws SQLException lanzamos excepciones hacia la capa LP
	 * @return nos devuelve la lista con los datos.
	 */
	public ResultSet consultarDesglose() throws SQLException {

		/**
		 * Consultamos los desgloses
		 */
		if (objConn != null) {
			/**
			 * Preparamos la consulta
			 */
			st = objConn.createStatement();
			/**
			 * Proceso de recuperacion de datos
			 */
			rs = st.executeQuery(QUERY_PARA_SELECT_DESGLOSE_DE_PEDIDOS);
			if (rs.isBeforeFirst()) {
				return rs;
			}
			while (rs.next()) {

				return rs;

			}

			/**
			 * Cerramos el resulset
			 * 
			 */
			rs.close();
		}
		return null;
	}

	/**
	 * Metodo para eliminar Desgloses de la BD
	 * 
	 * @param Referencia parametro recibido
	 * @throws SQLException lanzamos expcion
	 */
	public void eliminarDesglose(int Referencia) throws SQLException {

		/**
		 * Eliminamos Articulo
		 */
		if (objConn != null) {

			/**
			 * Creamos las preparedstaments
			 */
			objSt = objConn.prepareStatement(
					clsConstantesBD.QUERY_PARA_DELETE_DESGLOSE_DE_PEDIDO_POR_NUMERO_DE_PEDIDO_DESGLOSE);
			/**
			 * Parametro por el cual borramos
			 */
			objSt.setInt(1, Referencia);

			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}

	}

	/**
	 * Metodo para actualizar pedidos
	 * 
	 * @param numeroDePedido            parametro recibido
	 * @param fechaDePedido             parametro recibido
	 * @param fechaDeEntrega            parametro recibido
	 * @param entregado                 parametro recibido
	 * @param nombreYApelliosDelCliente parametro recibido
	 * @param numeroDeCliente_Pedidos   parametro recibido
	 * @throws SQLException lanza excepcion
	 */
	public void ActualizarPedidos(int numeroDePedido, Date fechaDePedido, Date fechaDeEntrega, boolean entregado,
			String nombreYApelliosDelCliente, int numeroDeCliente_Pedidos) throws SQLException {

		/**
		 * Traspaso de fechas de util.Date a long y despues a sql.Date
		 */
		java.sql.Date Fecha_de_pedido = new java.sql.Date(fechaDePedido.getTime());
		java.sql.Date Fecha_de_entrega = new java.sql.Date(fechaDeEntrega.getTime());

		/**
		 * Actualizamos entregas
		 */
		if (objConn != null) {

			/**
			 * Creamos la preparedStaments
			 */
			objSt = objConn.prepareStatement(QUERY_PARA_UPDATE_ENTREGA_DE_PEDIDO);
			/**
			 * Parametros para la query
			 */
			objSt.setInt(1, numeroDePedido);
			objSt.setDate(2, Fecha_de_pedido);
			objSt.setDate(3, Fecha_de_entrega);
			objSt.setBoolean(4, entregado);
			objSt.setString(5, nombreYApelliosDelCliente);
			objSt.setInt(6, numeroDeCliente_Pedidos);
			objSt.setInt(7, numeroDePedido);
			objSt.setInt(8, numeroDeCliente_Pedidos);
			/**
			 * Ejecutamos la query que hemos preparado
			 */
			objSt.execute();
		}
	}

}
