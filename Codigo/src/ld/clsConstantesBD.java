package ld;

/**
 * Clase para almacenar todas las constantes que utilicemos a nivel de LP con
 * base de datos.
 * 
 * @author Aitor Ubierna
 *
 */

public class clsConstantesBD {

	/**
	 * Constantes para la conexion.
	 */
	public static final String NOMBRE_DE_LA_BD = "mydb";
	public static final String NOMBRE_DEL_HOSTNAME = "localhost";
	public static final String PUERTO_BD = "3306";
	public static final String NOMBRE_DEL_USUARIO = "root";
	public static final String CONTRASE�A_DE_LA_BD = "rotia1995";
	public static final String RUTA_DE_LA_BD = "jdbc:mysql://" + NOMBRE_DEL_HOSTNAME + ":" + PUERTO_BD + "/"
			+ NOMBRE_DE_LA_BD + "?useTimezone=true&serverTimezone=GMT&useSSL=false";

	/**
	 * Constante para insert de Series.
	 */
	public static final String QUERY_PARA_INSERTAR_SERIES = "INSERT INTO `bdproyecto`.`series`"
			+ "(`NDeSerie`,`Descripcion`)" + "VALUES(?,?);";

	/**
	 * Constante para Select de series.
	 */
	public static final String QUERY_PARA_SELECT_SERIES = "SELECT `series`.`NDeSerie`,\r\n"
			+ "    `series`.`Descripcion`\r\n" + "FROM `bdproyecto`.`series`;";
	/**
	 * Constante para Delete de series condicionado por el N�DeSerie.
	 */
	public static final String QUERY_PARA_DELETE_SERIES_POR_N�DESERIE = "DELETE FROM `bdproyecto`.`series`\r\n"
			+ "WHERE NDeSerie = ?;";
	/**
	 * Constante para la INSERT de tapas.
	 */
	public static final String QUERY_PARA_INSERTAR_TAPAS = "INSERT INTO `bdproyecto`.`tapas`\r\n" + "(`Referenica`,\r\n"
			+ "`Descripcion`,\r\n" + "`Precio`)\r\n" + "VALUES\r\n" + "(?,?,?);";

	/**
	 * Consatnte para la SELECT de tapas.
	 */
	public static final String QUERY_PARA_SELECT_TAPAS = "SELECT `tapas`.`Referenica`,\r\n"
			+ "    `tapas`.`Descripcion`,\r\n" + "    `tapas`.`Precio`\r\n" + "FROM `bdproyecto`.`tapas`;";

	/**
	 * Constante de DELETE para tapas por referencia.
	 */
	public static final String QUERY_PARA_DELETE_TAPAS_POR_REFERENCIA = "DELETE FROM `bdproyecto`.`tapas`\r\n"
			+ "WHERE Referencia = ?;";

	/**
	 * Constante para INSERT materiales.
	 */
	public static final String QUERY_PARA_INSERTAR_MATERIALES = "INSERT INTO `bdproyecto`.`materiales`\r\n"
			+ "(`Referencia`,\r\n" + "`Descripcion`,\r\n" + "`Precio`)\r\n" + "VALUES\r\n" + "(?,?,?);";
	/**
	 * Constantes para SELECT materiales.
	 */
	public static final String QUERY_PARA_SELECT_MATERIALES = "SELECT `materiales`.`Referencia`,\r\n"
			+ "    `materiales`.`Descripcion`,\r\n" + "    `materiales`.`Precio`\r\n"
			+ "FROM `bdproyecto`.`materiales`;";
	/**
	 * Constante para DELETE materiales por referencia.
	 */
	public static final String QUERY_PARA_DELETE_MATERIALES_POR_REFERENCIA = "DELETE FROM `bdproyecto`.`materiales`\r\n"
			+ "WHERE Referencia = ?;";
	/**
	 * Constantes para INSERT tipo de hoja.
	 */
	public static final String QUERY_PARA_INSERTAR_TIPOS_DE_HOJA = "INSERT INTO `bdproyecto`.`tipohoja`\r\n"
			+ "(`Referencia`,\r\n" + "`Descripcion`,\r\n" + "`Precio`)\r\n" + "VALUES\r\n" + "(?,?,?);";
	/**
	 * Constantes para la SELECT de tipo de hoja.
	 */
	public static final String QUERY_PARA_SELECT_TIPOS_DE_HOJA = "SELECT `tipohoja`.`Referencia`,\r\n"
			+ "    `tipohoja`.`Descripcion`,\r\n" + "    `tipohoja`.`Precio`\r\n" + "FROM `bdproyecto`.`tipohoja`;";
	/**
	 * Constante de DELETE para tipos de hoja por referencia.
	 */
	public static final String QUERY_PARA_DELETE_TIPOS_DE_HOJA_POR_REFERENCIA = "DELETE FROM `bdproyecto`.`tipohoja`\r\n"
			+ "WHERE Referencia = ?;";
	/**
	 * Constantes para INSERT clientes.
	 */
	public static final String QUERY_PARA_INSERTAR_CLIENTES = "INSERT INTO `bdproyecto`.`clientes`\r\n"
			+ "(`NCliente`,\r\n" + "`NombreYApellidos`,\r\n" + "`DNI`,\r\n" + "`DireccionCliente`,\r\n"
			+ "`Provincia`,\r\n" + "`Telefono`,\r\n" + "`Email`)\r\n" + "VALUES\r\n" + "(?,?,?,?,?,?,?);";
	/**
	 * Constantes para la SELECT de clientes.
	 */
	public static final String QUERY_PARA_SELECT_CLIENTES = "SELECT `clientes`.`NCliente`,\r\n"
			+ "    `clientes`.`NombreYApellidos`,\r\n" + "    `clientes`.`DNI`,\r\n"
			+ "    `clientes`.`DireccionCliente`,\r\n" + "    `clientes`.`Provincia`,\r\n"
			+ "    `clientes`.`Telefono`,\r\n" + "    `clientes`.`Email`\r\n" + "FROM `bdproyecto`.`clientes`;";
	/**
	 * Constante de DELETE para clientes por n�mero de cliente.
	 */
	public static final String QUERY_PARA_DELETE_CLIENTES_POR_NUMERO_DE_CLIENTE = "DELETE FROM `bdproyecto`.`clientes`\r\n"
			+ "WHERE NCliente = ?;";
	/**
	 * Constantes para INSERT Envios.
	 */
	public static final String QUERY_PARA_INSERTAR_ENVIOS = "INSERT INTO `bdproyecto`.`envios`\r\n" + "(`NEnvio`,\r\n"
			+ "`NombreCliente`,\r\n" + "`DireccionEnvio`,\r\n" + "`PoblacionEnvio`,\r\n" + "`CPEnvio`,\r\n"
			+ "`ProvinciaEnvio`,\r\n" + "`TelefonoEnvio`,\r\n" + "`Clientes_NCliente1`)\r\n" + "VALUES\r\n"
			+ "(?,?,?,?,?,?,?,?);";
	/**
	 * Constantes para la SELECT de Envios.
	 */
	public static final String QUERY_PARA_SELECT_ENVIOS = "SELECT `envios`.`NEnvio`,\r\n"
			+ "    `envios`.`NombreCliente`,\r\n" + "    `envios`.`DireccionEnvio`,\r\n"
			+ "    `envios`.`PoblacionEnvio`,\r\n" + "    `envios`.`CPEnvio`,\r\n"
			+ "    `envios`.`ProvinciaEnvio`,\r\n" + "    `envios`.`TelefonoEnvio`,\r\n"
			+ "    `envios`.`Clientes_NCliente1`\r\n" + "FROM `bdproyecto`.`envios`;";
	/**
	 * Constante de DELETE para Envios por N�Envio.
	 */
	public static final String QUERY_PARA_DELETE_ENVIOS_POR_NUMERO_DE_ENVIO = "DELETE FROM `bdproyecto`.`envios`\r\n"
			+ "WHERE NEnvio = ?;";
	/**
	 * Constante para la INSERT de Pedido.
	 */
	public static final String QUERY_PARA_INSERTAR_PEDIDOS = "INSERT INTO `bdproyecto`.`pedidos`\r\n"
			+ "(`NPedido`,\r\n" + "`FechaPedido`,\r\n" + "`FechaEntrega`,\r\n" + "`Entregado`,\r\n"
			+ "`NombreYApellidos`,\r\n" + "`Clientes_NCliente`,\r\n" + "`Clientes_NCliente1`)\r\n" + "VALUES\r\n"
			+ "(?,?,?,?,?,?,?);";
	/**
	 * Constante para SELECT pedidos.
	 */
	public static final String QUERY_PARA_SELECT_PEDIDOS = "SELECT `pedidos`.`NPedido`,\r\n"
			+ "    `pedidos`.`FechaPedido`,\r\n" + "    `pedidos`.`FechaEntrega`,\r\n"
			+ "    `pedidos`.`Entregado`,\r\n" + "    `pedidos`.`NombreYApellidos`,\r\n"
			+ "    `pedidos`.`Clientes_NCliente`,\r\n" + "    `pedidos`.`Clientes_NCliente1`\r\n"
			+ "FROM `bdproyecto`.`pedidos`;";
	/**
	 * Constante para DELETE Pedidos.
	 */
	public static final String QUERY_PARA_DELETE_PEDIDOS_POR_NUMERO_DE_PEDIDO = "DELETE FROM `bdproyecto`.`pedidos`\r\n"
			+ "WHERE NPedido = ?;";
	/**
	 * Constante para la INSERT de Articulos.
	 */
	public static final String QUERY_PARA_INSERTAR_ARTICULOS = "INSERT INTO `bdproyecto`.`articulos`\r\n"
			+ "(`Referencia`,\r\n" + "`Serie`,\r\n" + "`Descripcion`,\r\n" + "`CantidadMaterial`,\r\n" + "`Precio`)\r\n"
			+ "VALUES\r\n" + "(?,?,?,?,?);";
	/**
	 * Constante para SELECT Articulos.
	 */
	public static final String QUERY_PARA_SELECT_ARTICULOS = "SELECT `articulos`.`Referencia`,\r\n"
			+ "    `articulos`.`Serie`,\r\n" + "    `articulos`.`Descripcion`,\r\n"
			+ "    `articulos`.`CantidadMaterial`,\r\n" + "    `articulos`.`Precio`\r\n"
			+ "FROM `bdproyecto`.`articulos`;";
	/**
	 * Constante para DELETE Articulos.
	 */
	public static final String QUERY_PARA_DELETE_ARTICULOS_POR_REFERENCIA = "DELETE FROM `bdproyecto`.`articulos`\r\n"
			+ "WHERE Referencia = ?;";
	/**
	 * Constante para INSERT de desglose de pedidos.
	 */
	public static final String QUERY_PARA_INSERTAR_DE_DESGLOSE_DE_PEDIDO = "INSERT INTO `bdproyecto`.`desglosedepedidos`\r\n"
			+ "(`NPedidoD`,\r\n" + "`Serie`,\r\n" + "`Tipo`,\r\n" + "`Articulos_Referencia`,\r\n"
			+ "`Pedidos_NPedido`,\r\n" + "`Pedidos_Clientes_NCliente1`,\r\n" + "`Tama�o`)\r\n" + "VALUES\r\n"
			+ "(?,?,?,?,?,?,?);";
	/**
	 * Constante para SELECT de desglose de pedidos.
	 */
	public static final String QUERY_PARA_SELECT_DESGLOSE_DE_PEDIDOS = "SELECT `desglosedepedidos`.`NPedidoD`,\r\n"
			+ "    `desglosedepedidos`.`Serie`,\r\n" + "    `desglosedepedidos`.`Tipo`,\r\n"
			+ "    `desglosedepedidos`.`Articulos_Referencia`,\r\n" + "    `desglosedepedidos`.`Pedidos_NPedido`,\r\n"
			+ "    `desglosedepedidos`.`Pedidos_Clientes_NCliente1`,\r\n" + "    `desglosedepedidos`.`Tama�o`\r\n"
			+ "FROM `bdproyecto`.`desglosedepedidos`;";
	/**
	 * Constante para DELETE desglose de pedido por numero de Pedido Desglose.
	 */
	public static final String QUERY_PARA_DELETE_DESGLOSE_DE_PEDIDO_POR_NUMERO_DE_PEDIDO_DESGLOSE = "DELETE FROM `bdproyecto`.`desglosedepedidos`\r\n"
			+ "WHERE NPedidoD = ?;";
	/**
	 * Constante para UPDATE entrega de pedidos
	 */
	public static final String QUERY_PARA_UPDATE_ENTREGA_DE_PEDIDO = "UPDATE `bdproyecto`.`desglosedepedidos`\r\n"
			+ "SET\r\n" + "`NPedidoD` = ?,\r\n" + "`Serie` = ?,\r\n" + "`Tipo` = ?,\r\n"
			+ "`Articulos_Referencia` = ?,\r\n" + "`Pedidos_NPedido` = ?,\r\n" + "`Pedidos_Clientes_NCliente1` = ?,\r\n"
			+ "`Tama�o` = ?\r\n" + "WHERE `NPedidoD` = ?;\r\n";

}
