INSERT INTO `bdproyecto`.`series`
(`NDeSerie`,
`Descripcion`)
VALUES
(1,
"p");


INSERT INTO `bdproyecto`.`tapas`
(`Referencia`,
`Descripcion`,
`Precio`)
VALUES
(1,
"d",
23.3);


INSERT INTO `bdproyecto`.`tipohoja`
(`Referencia`,
`Descripcion`,
`Precio`)
VALUES
(1,
"d",
3.3);

INSERT INTO `bdproyecto`.`materiales`
(`Referencia`,
`Descripcion`,
`Precio`)
VALUES
(1,
"d",
2.2);


INSERT INTO `bdproyecto`.`articulos`
(`Referencia`,
`Serie`,
`Descripcion`,
`CantidadMaterial`,
`Precio`)
VALUES
(1,
1,
"d",
3,
32.3);

INSERT INTO `bdproyecto`.`clientes`
(`NCliente`,
`NombreYApellidos`,
`DNI_NIF`,
`DireccionDeCliente`,
`Provincia`,
`Telefono`,
`Email`)
VALUES
(1,
"p",
"2",
"2",
"2",
2,
"2");


INSERT INTO `bdproyecto`.`pedidos`
(`NPedido`,
`Fecha_de_pedido`,
`Fecha_de_entrega`,
`Entregado`,
`Clientes_NCliente`,
`NombreYApellidos`)
VALUES
(1,
"2019-04-02",
"2019-07-02",
0,
1,
"j");


INSERT INTO `bdproyecto`.`envios`
(`NEnvio`,
`NombreCliente`,
`DireccionDeEnvio`,
`PoblacionDeEnvio`,
`CPDeEnvio`,
`ProvinciaDeEnvio`,
`TelefonoDeEnvio`,
`Clientes_NCliente`)
VALUES
(1,
"j",
"2",
"2",
2,
"2",
2,
1);


INSERT INTO `bdproyecto`.`desglosedepedidos`
(`NPedidoD`,
`Serie`,
`Tipo`,
`Articulos_Referencia`,
`Pedidos_NPedido`,
`Pedidos_Clientes_NCliente1`,
`Tamaño`)
VALUES
(1,
1,
"1",
1,
1,
1,
"1");